import React, { Component } from "react";
import Index from "./App/index";
import { Platform, SafeAreaView, Dimensions } from "react-native";
import firebase from "react-native-firebase";
import SplashScreen from "react-native-splash-screen";
import AsyncStorage from '@react-native-community/async-storage';
import * as Constants from './App/Resource/Constants';
import FlashMessage from "react-native-flash-message";
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import { doSetNotificationData } from './App/Store/Actions/AuthAction';
import { connect } from "react-redux";

import * as Sentry from '@sentry/react-native';

Sentry.init({ 
  dsn: 'https://2f38f8f4e7e843f99e2ce367ca4bc155@o355099.ingest.sentry.io/5235889', 
});

class App extends Component {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    SplashScreen.hide();
    this.checkPermission();
    this.createNotificationListeners();
    Dimensions.addEventListener('change', (dimensions) => {
      console.log('dimention', dimensions)
    });
    if (Platform.OS === "ios") {
      PushNotificationIOS.addEventListener("register", token => {});
      PushNotificationIOS.requestPermissions();
    }
    this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
       AsyncStorage.setItem(Constants.FIRE_BASE_TOKEN, fcmToken);
      });
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }
  async getToken() {
     let fcmToken = await AsyncStorage.getItem(Constants.FIRE_BASE_TOKEN);
     console.log('fcmToken', fcmToken)
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      console.log('fcmToken', fcmToken)
      if (fcmToken) {
        console.log('fcmToken', fcmToken)
        AsyncStorage.setItem(Constants.FIRE_BASE_TOKEN, fcmToken);
      }
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();

      this.getToken();
    } catch (error) {
    }
  }
  componentWillUnmount() {
    this.unsubscribeFromNotificationListener;
    this.notificationOpenedListener;
  }
  async createNotificationListeners() {
    const channel = new firebase.notifications.Android.Channel(
      "com.mysafetyadult",
      "notification",
      firebase.notifications.Android.Importance.Max
    ).setDescription("A natural description of the channel");
    firebase.notifications().android.createChannel(channel);

    const channelGroup = new firebase.notifications.Android.ChannelGroup(
      "com.mysafetyadult",
      "notification"
    );
    firebase.notifications().android.createChannelGroup(channelGroup);

    this.unsubscribeFromNotificationListener = firebase
      .notifications()
      .onNotification(notification => {
        if (Platform.OS === "android") {
          const localNotification = new firebase.notifications.Notification({
            sound: "default",
            show_in_foreground: true
          })
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification.body)
            .setData(notification.data)
            .android.setChannelId("com.mysafetyadult")
            .android.setSmallIcon("ic_notification")
            .android.setAutoCancel(true)
            .android.setGroupSummary(true)
            .android.setGroup("com.mysafetyadult")
            .android.setPriority(firebase.notifications.Android.Priority.High);

          firebase
            .notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));
        } else if (Platform.OS === "ios") {
          const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification.body)
            .setData(notification.data)
            .ios.setBadge(notification.ios.badge);
          firebase
            .notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));
        }
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
    .notifications()
    .onNotificationOpened(notificationOpen => {
      const { title, body } = notificationOpen.notification;

      if (notificationOpen) {
        const action = notificationOpen.action;
        const notification = notificationOpen.notification;
        this.props.dispatch(doSetNotificationData(notification.data));

        firebase
          .notifications()
          .removeDeliveredNotification(notification.notificationId);
      }
    });

  /*
   * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
   * */

  firebase
    .notifications()
    .getInitialNotification()
    .then(notificationOpen => {
      if (notificationOpen) {
        const action = notificationOpen.action;
        const notification = notificationOpen.notification;
         this.props.dispatch(doSetNotificationData(notification.data));
        firebase
          .notifications()
          .removeDeliveredNotification(notification.notificationId);
      }
    });
  }

  render() {
    return  <SafeAreaView style={{flex:1}}><Index />
            <FlashMessage position="top" />

    </SafeAreaView>;
  }
}
export default connect(null,null)(App);