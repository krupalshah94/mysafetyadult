const string = {
  tabHome: "Home",
  tabConnections: "Connections",
  tabEvents: "Events",
  tabBadge: "Badge",
  tabSetting: "Setting",

  //error massage
  emptyEmail: "Please enter Email",
  invalidEmail: "Please enter valid Email",
  emptyPassword: "Please enter Password",
  invalidPassword: "Please use at least 5 character",

  //login screen name
  login: "LOGIN",
  signUp: "SIGN UP",
  forgotPassword: "FORGOT PASSWORD?",

  //signUp massage

  errImage: "Please select Profile Picture",
  errFName: "Please enter First Name",
  errLName: "Please enter Last Name",
  errPhone: "Please enter Phone Number",
  errEmail: "Please enter Email",
  errEmail: "Please enter valid Email",
  errCity: "Please enter City",
  errStateName: "Please enter State",
  errPassword: "Please enter Password",
  errPasswordLen: "Please use at least 5 character",
  errCPassword: "Please enter Confirm Password",
  errCPassword: "Please use at least 5 character",
  errCPasswordMatch: "Password does not matched",
  errTerms: "Please select Terms & conditions"

  //signUp namae
  
};
export default string;
