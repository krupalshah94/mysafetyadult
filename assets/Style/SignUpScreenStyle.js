import { StyleSheet} from 'react-native';
import Colors from "../Color/index";
const SignUpScreenStyle = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#DFE2F1',
    },
    card:{
      backgroundColor: Colors.white,
      borderRadius: 4,
      marginTop:'5%',
      marginStart: "5%",
      marginEnd: "5%"            
    },
    text_reg:{ color: Colors.black, fontSize: 16, marginStart: 10,fontFamily:'AktivGrotesk-Bold' }
  });

  export default SignUpScreenStyle;