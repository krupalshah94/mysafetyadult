import { StyleSheet } from "react-native";
import Colors from "../Color/index";
const LoginScreenStyle = StyleSheet.create({
  container: {
    flex: 1
  },
  card: {
    backgroundColor: Colors.white,
    borderRadius: 20,
    marginTop: "5%",
    marginStart: "10%",
    marginEnd: "10%"
  },
  image_badge: {
    marginTop: "10%",
    marginBottom: "5%",
    width: 120,
    height: 120,
    alignSelf: "center"
  },
  text_login: {
    color: "#0330BF",
    marginTop: "10%",
    marginBottom: "5%",
    textAlign: "center",
    fontSize: 16,
    fontFamily: "AktivGrotesk-Regular"
  },
  text_forgot: {
    marginTop: 20,
    textDecorationLine: "underline",
    color: "#3B50E0",
    marginTop: 20,
    marginBottom: 20,
    fontSize: 16,
    textAlign: "center",
    fontFamily: "AktivGrotesk-Regular"
  },
  button_signup: {
    marginTop: 20,
    marginBottom: 20,
    marginStart: "10%",
    marginEnd: "10%"
  }
});

export default LoginScreenStyle;
