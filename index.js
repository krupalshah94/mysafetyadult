import { AppRegistry,YellowBox } from "react-native";
import App from "./App";
import React from "react";
import { Provider } from "react-redux";
import {store} from "./App/Store/configureStore";
import { name as appName } from "./app.json";

YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);
YellowBox.ignoredYellowBox = ["Warning:"];
console.disableYellowBox = true;

const RNRedux = () => (
  <Provider store={store}>
    <App/>
  </Provider>
);

AppRegistry.registerComponent(appName, () => RNRedux);
