import * as actionType from '../ActionType';

export const sendConnectionRequest = () => ({
type: actionType.SEND_CONNECTION_REQUEST,
payload: '',
});

export const sendConnectionSuccess = (data) => ({
type: actionType.SEND_CONNECTION_SUCCESS,
payload: data,
});

export const sendConnectionFailed = (data) => ({
type: actionType.SEND_CONNECTION_FAILED,
payload: data,
});

export const getConnectionListRequest = () => ({
type: actionType.GET_CONNECTION_LIST_REQUEST,
payload: '',
});

export const getConnectionListSuccess = (data) => ({
type: actionType.GET_CONNECTION_LIST_SUCCESS,
payload: data,
});

export const getConnectionListFailed = (data) => ({
type: actionType.GET_CONNECTION_LIST_FAILED,
payload: data,
});

export const actionConnectionRequest = () => ({
type: actionType.ACTION_CONNECTION_REQUEST,
payload: '',
});

export const actionConnectionSuccess = (data) => ({
type: actionType.ACTION_CONNECTION_SUCCESS,
payload: data,
});

export const actionConnectionFailed = (data) => ({
type: actionType.ACTION_CONNECTION_FAILED,
payload: data,
});

export const connectedUserListRequest = () => ({
type: actionType.CONNECTED_USER_LIST_REQUEST,
payload: '',
});

export const connectedUserListSuccess = (data) => ({
type: actionType.CONNECTED_USER_LIST_SUCCESS,
payload: data,
});

export const connectedUserListFailed = (data) => ({
type: actionType.CONNECTED_USER_LIST_FAILED,
payload: data,
});

export const getConnectedUserProfileRequest = () => ({
type: actionType.GET_CONNECTED_USER_PROFILE_REQUEST,
payload: '',
});

export const getConnectedUserProfileSuccess = (data) => ({
type: actionType.GET_CONNECTED_USER_PROFILE_SUCCESS,
payload: data,
});

export const getConnectedUserProfileFailed = (data) => ({
type: actionType.GET_CONNECTED_USER_PROFILE_FAILED,
payload: data,
});
    
export const connectedUserSendRequest = () => ({
    type: actionType.CONNECTED_USER_SEND_REQUEST,
    payload: '',
    });
    
    export const connectedUserSendSuccess = (data) => ({
    type: actionType.CONNECTED_USER_SEND_SUCCESS,
    payload: data,
    });
    
    export const connectedUserSendFailed = (data) => ({
    type: actionType.CONNECTED_USER_SEND_FAILED,
    payload: data,
    });