import * as actionType from "../ActionType";


export const loginRequest = () => ({
  type: actionType.GET_LOGIN_REQUEST,
  payload: '',
});

export const loginSuccess = (userData) => ({
  type: actionType.GET_LOGIN_SUCCESS,
  payload: userData,
});

export const loginFail = (error) => ({
  type: actionType.GET_LOGIN_FAILED,
  payload: error,
});


export const forgetPasswordRequest = () => ({
  type: actionType.FORGET_PASSWORD_REQUEST,
  payload: '',
});

export const forgetPasswordSuccess = (userData) => ({
  type: actionType.FORGET_PASSWORD_SUCCESS,
  payload: userData,
});

export const forgetPasswordFail = (error) => ({
  type: actionType.FORGET_PASSWORD_FAILED,
  payload: error,
});

export const confirmOtpRequest = () => ({
  type: actionType.CONFIRM_OTP_REQUEST,
  payload: '',
});

export const confirmOtpSuccess = (userData) => ({
  type: actionType.CONFIRM_OTP_SUCCESS,
  payload: userData,
});

export const confirmOtpFail = (error) => ({
  type: actionType.CONFIRM_OTP_FAILED,
  payload: error,
});

export const setNewPasswordRequest = () => ({
  type: actionType.SET_NEW_PASSWORD_REQUEST,
  payload: '',
});

export const setNewPasswordSuccess = (userData) => ({
  type: actionType.SET_NEW_PASSWORD_SUCCESS,
  payload: userData,
});

export const setNewPasswordFail = (error) => ({
  type: actionType.SET_NEW_PASSWORD_FAILED,
  payload: error,
});

export const changePasswordRequest = () => ({
  type: actionType.CHANGE_PASSWORD_REQUEST,
  payload: '',
});

export const changePasswordSuccess = (userData) => ({
  type: actionType.CHANGE_PASSWORD_SUCCESS,
  payload: userData,
});

export const changePasswordFail = (error) => ({
  type: actionType.CHANGE_PASSWORD_FAILED,
  payload: error,
});

export const getNotificationRequest = () => ({
  type: actionType.GET_NOTIFICATION_REQUEST,
  payload: '',
});

export const getNotificationSuccess = (userData) => ({
  type: actionType.GET_NOTIFICATION_SUCCESS,
  payload: userData,
});

export const getNotificationFail = (error) => ({
  type: actionType.GET_NOTIFICATION_FAILED,
  payload: error,
});

export const doSetNotificationData = (data) => ({
  type: actionType.SET_PUSH_NOTIFICATION,
  payload: data,
});