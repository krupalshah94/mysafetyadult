import * as actionType from "../ActionType";

export const getEventListRequest = () => ({
    type: actionType.GET_EVENT_LIST_REQUEST,
    payload: '',
  });

export const getEventListSuccess = (data) => ({
type: actionType.GET_EVENT_LIST_SUCCESS,
payload: data,
});

export const getEventListFailed = (data) => ({
type: actionType.GET_EVENT_LIST_FAILED,
payload: data,
});


export const addEventListRequest = () => ({
    type: actionType.ADD_EVENT_LIST_REQUEST,
    payload: '',
  });

export const addEventListSuccess = (data) => ({
type: actionType.ADD_EVENT_LIST_SUCCESS,
payload: data,
});

export const addEventListFailed = (data) => ({
type: actionType.ADD_EVENT_LIST_FAILED,
payload: data,
});

export const getEventDetailsRequest = () => ({
    type: actionType.GET_EVENT_DETAILS_REQUEST,
    payload: '',
  });

export const getEventDetailsSuccess = (data) => ({
type: actionType.GET_EVENT_DETAILS_SUCCESS,
payload: data,
});

export const getEventDetailsFailed = (data) => ({
type: actionType.GET_EVENT_DETAILS_FAILED,
payload: data,
});

export const getSafetyImagesRequest = () => ({
  type: actionType.GET_SAFETY_IMAGES_REQUEST,
  payload: '',
});

export const getSafetyImagesSuccess = (data) => ({
type: actionType.GET_SAFETY_IMAGES_SUCCESS,
payload: data,
});

export const getSafetyImagesFailed = (data) => ({
type: actionType.GET_SAFETY_IMAGES_FAILED,
payload: data,
});

export const getMyIncomingEventRequest = () => ({
  type: actionType.GET_MY_INCOMING_REQUEST,
  payload: '',
});

export const getMyIncomingEventSuccess = (data) => ({
type: actionType.GET_MY_INCOMING_SUCCESS,
payload: data,
});

export const getMyIncomingEventFailed = (data) => ({
type: actionType.GET_MY_INCOMING_FAILED,
payload: data,
});

export const getAllEventRequest = () => ({
  type: actionType.GET_ALL_EVENTS_REQUEST,
  payload: '',
});

export const getAllEventSuccess = (data) => ({
type: actionType.GET_ALL_EVENTS_SUCCESS,
payload: data,
});

export const getAllEventFailed = (data) => ({
type: actionType.GET_ALL_EVENTS_FAILED,
payload: data,
});

export const completeEventRequest = () => ({
  type: actionType.COMPLETE_YOUR_EVENTS_REQUEST,
  payload: '',
});

export const completeEventSuccess = (data) => ({
type: actionType.COMPLETE_YOUR_EVENTS_SUCCESS,
payload: data,
});

export const completeEventFailed = (data) => ({
type: actionType.COMPLETE_YOUR_EVENTS_FAILED,
payload: data,
});

export const eventCompleted = (isCompleted) => ({
  type: actionType.EVENT_COMPLETED,
  payload: isCompleted,
});

export const actionEventRequest = () => ({
  type: actionType.ACTION_EVENT_REQUEST,
  payload: '',
});

export const actionEventSuccess = (data) => ({
type: actionType.ACTION_EVENT_SUCCESS,
payload: data,
});

export const actionEventFailed = (data) => ({
type: actionType.ACTION_EVENT_FAILED,
payload: data,
});

export const calenderDatesRequest = () => ({
  type: actionType.CALENDER_DATES_REQUEST,
  payload: '',
});

export const calenderDatesSuccess = (data) => ({
type: actionType.CALENDER_DATES_SUCCESS,
payload: data,
});

export const calenderDatesFailed = (data) => ({
type: actionType.CALENDER_DATES_FAILED,
payload: data,
});

export const addNewEventRequest = () => ({
  type: actionType.ADD_NEW_EVENT_REQUEST,
  payload: '',
});

export const addNewEventSuccess = (data) => ({
type: actionType.ADD_NEW_EVENT_SUCCESS,
payload: data,
});

export const addNewEventFailed = (data) => ({
type: actionType.ADD_NEW_EVENT_FAILED,
payload: data,
});

export const userEventRequest = () => ({
  type: actionType.GET_EVENTS_BY_USER_REQUEST,
  payload: '',
});

export const userEventSuccess = (data) => ({
type: actionType.GET_EVENTS_BY_USER_SUCCESS,
payload: data,
});

export const userEventFailed = (data) => ({
type: actionType.GET_EVENTS_BY_USER_FAILED,
payload: data,
});


export const refreshList = (data) => ({
  type: actionType.REFRESH_LIST,
  payload: data,
  });
  