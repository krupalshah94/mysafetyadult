import * as actionType from "../ActionType";

export const UserProfileRequest = () => ({
    type: actionType.GET_USER_PROFILE_REQUEST,
    payload: '',
  });

export const UserProfile = (userData) => ({
  type: actionType.GET_USER_PROFILE,
  payload: userData,
});

export const UserProfileFailed = (error) => ({
  type: actionType.GET_USER_PROFILE_FAILED,
  payload: error,
});

export const UpdateUserProfileRequest = () => ({
  type: actionType.UPDATE_USER_PROFILE_REQUEST,
  payload: '',
});

export const UpdateUserProfile = (userData) => ({
type: actionType.UPDATE_USER_PROFILE,
payload: userData,
});

export const UpdateUserProfileFailed = (error) => ({
type: actionType.UPDATE_USER_PROFILE_FAILED,
payload: error,
});