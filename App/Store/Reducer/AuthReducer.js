import * as actionType from '../ActionType'

const initialState={
    user:[],
    isLoginLoading: false,
    loginError: undefined,
    isForgetPasswordLoading: false,
    forgetPasswordDetails: {},
    forgetPasswordError: undefined,
    isConfirmOtpLoading: false,
    confirmOtpDetails: {},
    confirmOtpError: undefined,
    isNewPasswordLoading: false,
    setPasswordDetails: {},
    setPasswordError: undefined,
    isChangePasswordLoading: false,
    changePasswordDetails: {},
    changePasswordError: undefined,
    isNotificationListLoading: false,
    notificationListDetails: {},
    notificationListError: undefined,
    notification: undefined,
}

const Auth=(state = initialState, action)=>{
    switch (action.type) {
        case actionType.GET_LOGIN_REQUEST:
        return {
            ...state, 
            isLoginLoading: true,
        };
        case actionType.GET_LOGIN_SUCCESS:
        return {
            ...state, 
            user: action.payload,
            isLoginLoading: false,
        };
        case actionType.GET_LOGIN_FAILED:
        return {
            ...state, 
            loginError: action.payload,
            isLoginLoading: false,
        };
        case actionType.FORGET_PASSWORD_REQUEST:
        return {
            ...state, 
            isForgetPasswordLoading: true,
        };
        case actionType.FORGET_PASSWORD_SUCCESS:
        return {
            ...state, 
            forgetPasswordDetails: action.payload,
            isForgetPasswordLoading: false,
        };
        case actionType.FORGET_PASSWORD_FAILED:
        return {
            ...state, 
            forgetPasswordError: action.payload,
            isForgetPasswordLoading: false,
        };
        case actionType.CONFIRM_OTP_REQUEST:
        return {
            ...state, 
            isConfirmOtpLoading: true,
        };
        case actionType.CONFIRM_OTP_SUCCESS:
        return {
            ...state, 
            confirmOtpDetails: action.payload,
            isConfirmOtpLoading: false,
        };
        case actionType.CONFIRM_OTP_FAILED:
        return {
            ...state, 
            confirmOtpError: action.payload,
            isConfirmOtpLoading: false,
        };
        case actionType.SET_NEW_PASSWORD_REQUEST:
        return {
            ...state, 
            isNewPasswordLoading: true,
        };
        case actionType.SET_NEW_PASSWORD_SUCCESS:
        return {
            ...state, 
            setPasswordDetails: action.payload,
            isNewPasswordLoading: false,
        };
        case actionType.SET_NEW_PASSWORD_FAILED:
        return {
            ...state, 
            setPasswordError: action.payload,
            isNewPasswordLoading: false,
        };
        case actionType.CHANGE_PASSWORD_REQUEST:
        return {
            ...state, 
            isChangePasswordLoading: true,
        };
        case actionType.CHANGE_PASSWORD_SUCCESS:
        return {
            ...state, 
            changePasswordDetails: action.payload,
            isChangePasswordLoading: false,
        };
        case actionType.CHANGE_PASSWORD_FAILED:
        return {
            ...state, 
            changePasswordError: action.payload,
            isChangePasswordLoading: false,
        };
        case actionType.GET_NOTIFICATION_REQUEST:
        return {
            ...state, 
            isNotificationListLoading: true,
        };
        case actionType.GET_NOTIFICATION_SUCCESS:
        return {
            ...state, 
            notificationListDetails: action.payload,
            isNotificationListLoading: false,
        };
        case actionType.GET_NOTIFICATION_FAILED:
        return {
            ...state, 
            notificationListError: action.payload,
            isNotificationListLoading: false,
        };
        case actionType.SET_PUSH_NOTIFICATION:
        return {
            ...state, 
            notification: action.payload,
        };
        default:
            return state; 
    }
}
export default Auth;