import * as actionType from '../ActionType'

const initialState={
    isLoadingGetList: false,
    getList: {},
    getListError: undefined,
    isLoadingAddEvent: false,
    addEvent: {},
    getAddEventError: undefined,
    isLoadingEventDetails: false,
    eventDetails: {},
    errorEventDetails: undefined,
    isLoadingSafetyImages: false,
    getImagesList: {},
    errorImages: undefined,
    isLoadingMYIncomingEvent: false,
    myIncomingEventList: {},
    errorMyIncomingEvents: undefined,
    isLoadingAllEvent: false,
    allEventList: {},
    errorAllEvents: undefined,
    isLoadingActionEvent: false,
    actionEvent: {},
    errorActionEvents: undefined,
    isLoadingCompleteEvent: false,
    completeEvent: {},
    errorCompleteEvents: undefined,
    isCompletedEvent: false,
    isLoadingCalenderDates: false,
    calenderDates: {},
    errorCalenderDates: undefined,
    isLoadingAddNewEvent: false,
    addNewEvent: {},
    errorAddNewEvent: undefined,
    isLoadingUserEvent: false,
    userEvent: {},
    errorUserEvent: undefined,
    isListRefresh: false,
}

const Event=(state = initialState, action)=>{
    switch (action.type) {
        case actionType.GET_EVENT_LIST_REQUEST:
        return {
            ...state, 
            isLoadingGetList: true,
        };
        case actionType.GET_EVENT_LIST_SUCCESS:
        return {
            ...state, 
            isLoadingGetList: false,
            getList: action.payload,
        };
        case actionType.GET_EVENT_LIST_FAILED:
        return {
            ...state, 
            isLoadingGetList: false,
            getListError: action.payload,
        };
        case actionType.ADD_EVENT_LIST_REQUEST:
        return {
            isLoadingAddEvent: true,
            ...state, 
        };
        case actionType.ADD_EVENT_LIST_SUCCESS:
        return {
            ...state, 
            isLoadingAddEvent: false,
            addEvent: action.payload,
        };
        case actionType.ADD_EVENT_LIST_FAILED:
        return {
            ...state, 
            isLoadingAddEvent: false,
            getAddEventError: action.payload,
        };
        case actionType.GET_EVENT_DETAILS_REQUEST:
        return {
            ...state, 
            isLoadingEventDetails: true,
        };
        case actionType.GET_EVENT_DETAILS_SUCCESS:
        return {
            ...state, 
            isLoadingEventDetails: false,
            eventDetails: action.payload,
        };
        case actionType.GET_EVENT_DETAILS_FAILED:
        return {
            ...state, 
            isLoadingEventDetails: false,
            errorEventDetails: action.payload,
        };
        case actionType.GET_SAFETY_IMAGES_REQUEST:
        return {
            ...state, 
            isLoadingSafetyImages: true,
        };
        case actionType.GET_SAFETY_IMAGES_SUCCESS:
        return {
            ...state, 
            isLoadingSafetyImages: false,
            getImagesList: action.payload,
        };
        case actionType.GET_SAFETY_IMAGES_FAILED:
        return {
            ...state, 
            isLoadingSafetyImages: false,
            errorImages: action.payload,
        };
        case actionType.GET_MY_INCOMING_REQUEST:
        return {
            ...state, 
            isLoadingMYIncomingEvent: true,
        };
        case actionType.GET_MY_INCOMING_SUCCESS:
        return {
            ...state, 
            isLoadingMYIncomingEvent: false,
            myIncomingEventList: action.payload,
        };
        case actionType.GET_MY_INCOMING_FAILED:
        return {
            ...state, 
            isLoadingMYIncomingEvent: false,
            errorMyIncomingEvents: action.payload,
        };
        case actionType.GET_ALL_EVENTS_REQUEST:
        return {
            ...state, 
            isLoadingAllEvent: true,
        };
        case actionType.GET_ALL_EVENTS_SUCCESS:
        return {
            ...state, 
            isLoadingAllEvent: false,
            allEventList: action.payload,
        };
        case actionType.GET_ALL_EVENTS_FAILED:
        return {
            ...state, 
            isLoadingAllEvent: false,
            errorAllEvents: action.payload,
        };
        case actionType.ACTION_EVENT_REQUEST:
        return {
            ...state, 
            isLoadingActionEvent: true,
        };
        case actionType.ACTION_EVENT_SUCCESS:
        return {
            ...state, 
            isLoadingActionEvent: false,
            actionEvent: action.payload,
        };
        case actionType.ACTION_EVENT_FAILED:
        return {
            ...state, 
            isLoadingActionEvent: false,
            errorActionEvents: action.payload,
        };
        case actionType.COMPLETE_YOUR_EVENTS_REQUEST:
        return {
            ...state, 
            isLoadingCompleteEvent: true,
        };
        case actionType.COMPLETE_YOUR_EVENTS_SUCCESS:
        return {
            ...state, 
            isLoadingCompleteEvent: false,
            completeEvent: action.payload,
        };
        case actionType.COMPLETE_YOUR_EVENTS_FAILED:
        return {
            ...state, 
            isLoadingCompleteEvent: false,
            errorCompleteEvents: action.payload,
        };
        case actionType.EVENT_COMPLETED:
        return {
            ...state, 
          isCompletedEvent: action.payload,
        };
        case actionType.CALENDER_DATES_REQUEST:
        return {
            ...state, 
            isLoadingCalenderDates: true,
        };
        case actionType.CALENDER_DATES_SUCCESS:
        return {
            ...state, 
            isLoadingCalenderDates: false,
            calenderDates: action.payload,
        };
        case actionType.CALENDER_DATES_FAILED:
        return {
            ...state, 
            isLoadingCalenderDates: false,
            errorCalenderDates: action.payload,
        };
        case actionType.ADD_NEW_EVENT_REQUEST:
        return {
            ...state, 
            isLoadingAddNewEvent: true,
        };
        case actionType.ADD_NEW_EVENT_SUCCESS:
        return {
            ...state, 
            isLoadingAddNewEvent: false,
            addNewEvent: action.payload,
        };
        case actionType.ADD_NEW_EVENT_FAILED:
        return {
            ...state, 
            isLoadingAddNewEvent: false,
            errorAddNewEvent: action.payload,
        };
        case actionType.GET_EVENTS_BY_USER_REQUEST:
        return {
            ...state, 
            isLoadingUserEvent: true,
        };
        case actionType.GET_EVENTS_BY_USER_SUCCESS:
        return {
            ...state, 
            isLoadingUserEvent: false,
            userEvent: action.payload,
        };
        case actionType.GET_EVENTS_BY_USER_FAILED:
        return {
            ...state, 
            isLoadingUserEvent: false,
            errorUserEvent: action.payload,
        };
        case actionType.REFRESH_LIST:
            return {
                ...state, 
                isListRefresh: action.payload,
            };
        default:
            return state; 
    }
}
export default Event;