import * as actionType from '../ActionType'

const initialState={ 
    user: {},
    isUserDataLoading: false,
    userError: undefined,
    updatedUser: {},
    isUpdateUserLoading: false,
    updateUserError: undefined,
}

const User=(state = initialState, action)=>{
    switch (action.type) {
        case actionType.GET_USER_PROFILE_REQUEST:
        return {
            ...state, 
            isUserDataLoading: true
        };
        case actionType.GET_USER_PROFILE:
        return {
            ...state, 
            user: action.payload,
            isUserDataLoading: false,
        };
        case actionType.GET_USER_PROFILE_FAILED:
        return {
            ...state, 
            userError: action.payload,
            isUserDataLoading: false
        };
        case actionType.UPDATE_USER_PROFILE_REQUEST:
        return {
            ...state, 
            isUpdateUserLoading: true
        };
        case actionType.UPDATE_USER_PROFILE:
        return {
            ...state, 
            updatedUser: action.payload,
            isUpdateUserLoading: false,
        };
        case actionType.UPDATE_USER_PROFILE_FAILED:
        return {
            ...state, 
            updateUserError: action.payload,
            isUpdateUserLoading: false
        };
        default:
        return state; 
    }
};

export default User;