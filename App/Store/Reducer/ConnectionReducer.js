import * as actionType from '../ActionType'

const initialState={
    isLoadingSendConnection: false,
    sendConnectionData: {},
    errorSendConnection: undefined,
    isLoadingGetConnectionList: false,
    connectionList: {},
    errorConnectionList: undefined,
    isLoadingActionConnection: false,
    actionConnection: {},
    errorActionConnection: undefined,
    isLoadingConnectedUserList: false,
    connectedUserList: {},
    errorConnectedUserList: undefined,
    isLoadingConnectedUserProfile: false,
    connectedUserProfile: {},
    errorConnectedUserProfile: undefined,
    isLoadingConnectedUserSend: false,
    connectedUserSend: {},
    errorConnectedUserSend: undefined,
}

const Connection=(state = initialState, action)=>{
    switch (action.type) {
        case actionType.SEND_CONNECTION_REQUEST:
        return {
            ...state, 
            isLoadingSendConnection: true,
        };
        case actionType.SEND_CONNECTION_SUCCESS:
        return {
            ...state, 
            sendConnectionData: action.payload,
            isLoadingSendConnection: false,
        };
        case actionType.SEND_CONNECTION_FAILED:
        return {
            ...state, 
            errorSendConnection: action.payload,
            isLoadingSendConnection: false,
        };
        case actionType.GET_CONNECTION_LIST_REQUEST:
        return {
            ...state, 
            isLoadingGetConnectionList: true,
        };
        case actionType.GET_CONNECTION_LIST_SUCCESS:
        return {
            ...state, 
            connectionList: action.payload,
            isLoadingGetConnectionList: false,
        };
        case actionType.GET_CONNECTION_LIST_FAILED:
        return {
            ...state, 
            errorConnectionList: action.payload,
            isLoadingGetConnectionList: false,
        };
        case actionType.ACTION_CONNECTION_REQUEST:
        return {
            ...state, 
            isLoadingActionConnection: true,
        };
        case actionType.ACTION_CONNECTION_SUCCESS:
        return {
            ...state, 
            actionConnection: action.payload,
            isLoadingActionConnection: false,
        };
        case actionType.ACTION_CONNECTION_FAILED:
        return {
            ...state, 
            errorActionConnection: action.payload,
            isLoadingActionConnection: false,
        };
        case actionType.CONNECTED_USER_LIST_REQUEST:
        return {
            ...state, 
            isLoadingConnectedUserList: true,
        };
        case actionType.CONNECTED_USER_LIST_SUCCESS:
        return {
            ...state, 
            connectedUserList: action.payload,
            isLoadingConnectedUserList: false,
        };
        case actionType.CONNECTED_USER_LIST_FAILED:
        return {
            ...state, 
            errorConnectedUserList: action.payload,
            isLoadingConnectedUserList: false,
        };
        case actionType.GET_CONNECTED_USER_PROFILE_REQUEST:
        return {
            ...state, 
            isLoadingConnectedUserProfile: true,
        };
        case actionType.GET_CONNECTED_USER_PROFILE_SUCCESS:
        return {
            ...state, 
            connectedUserProfile: action.payload,
            isLoadingConnectedUserProfile: false,
        };
        case actionType.GET_CONNECTED_USER_PROFILE_FAILED:
        return {
            ...state, 
            errorConnectedUserProfile: action.payload,
            isLoadingConnectedUserProfile: false,
        };
        case actionType.CONNECTED_USER_SEND_REQUEST:
            return {
                ...state, 
                isLoadingConnectedUserSend: true,
            };
            case actionType.CONNECTED_USER_SEND_SUCCESS:
            return {
                ...state, 
                connectedUserSend: action.payload,
                isLoadingConnectedUserSend: false,
            };
            case actionType.CONNECTED_USER_SEND_FAILED:
            return {
                ...state, 
                errorConnectedUserSend: action.payload,
                isLoadingConnectedUserSend: false,
            };
        default:
            return state; 
    }
}
export default Connection;