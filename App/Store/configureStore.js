import { createStore, combineReducers, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import Auth from './Reducer/AuthReducer';
import User from './Reducer/UserReducer';
import Event from './Reducer/EventReducer';
import Connection from './Reducer/ConnectionReducer';

const rootReducer = combineReducers({
    Auth : Auth,
    User: User,
    Event: Event,
    Connection: Connection,
  });

 let store;
if (process.env.NODE_ENV === 'development') {
  store = createStore(rootReducer, applyMiddleware(logger, thunk));
} else {
  store = createStore(rootReducer);
}

const dispatch = (action) => {
  store.dispatch(action);
};


export { store, dispatch };





