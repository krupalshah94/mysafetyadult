import React from "react";
import { View } from "react-native";

import { BarIndicator } from "react-native-indicators";
import colors from "../Resource/Color/index";
const BarIndicatorLoader = props => (
  <View
    style={{
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "rgba(0,0,0,0.2)",
    }}
  >
    <BarIndicator color={colors.white} count={5} />
  </View>
);

export default BarIndicatorLoader;
