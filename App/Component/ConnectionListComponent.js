import React, { PureComponent } from "react";
import { View, FlatList, StyleSheet, TouchableOpacity, ActivityIndicator } from "react-native";
import { Text, Image } from "react-native-elements";
import Colors from "../Resource/Color/index";
import Icons from "../Resource/index";
import PropTypes from "prop-types";
import font from "../Resource/fontFamily";
import { RFPercentage } from "react-native-responsive-fontsize";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-community/async-storage';
import * as Constants from '../Resource/Constants';
import BarIndicatorLoader from './BarIndicatorLoader';
import CustomToast from './CustomToast';
import { connect } from 'react-redux';
import {getConnectionList} from '../Network/Service';
import {connectedUserListRequest, connectedUserListSuccess, connectedUserListFailed } from '../Store/Actions/ConnectionAction';
import { widthPercentageToDP, heightPercentageToDP } from "react-native-responsive-screen";
import colors from "../Resource/Color/index";
class ConnectionListComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
        isConnected: false,
        loading: false,
        viewEmpty: true,
        refreshing: false,
        page: 1,
        per_page: 10,
        last_page: 0,
        connectionData: [],
        connections: [],
        selectedUser: {},
        invitedUserId: undefined,
        loadingImg: true
    }
}

componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentWillUnmount() {
    this._subscription && this._subscription();
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.isLoadingConnectedUserList !== this.props.isLoadingConnectedUserList) {
        this.setState({ loading: this.props.isLoadingConnectedUserList });
    }
    if (prevProps.connectedUserList !== this.props.connectedUserList) {
      if (this.props.connectedUserList.data.data.length === 0) {
        this.setState({ viewEmpty: true, refreshing: false, }) 
      } else {
        this.setState({ viewEmpty: false,
          connections: this.state.page === 1? this.props.connectedUserList.data.data : [...this.state.connections, ...this.props.connectedUserList.data.data],
        refreshing: false,
        last_page: this.props.connectedUserList.data.last_page
        });
      }
    }
}

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    if (this.Token !== '') {
        this.getConnections();
    }
  }

  handleLoadMore = () => {
    if (this.state.page != this.state.last_page) {
      this.setState(
        {
          page: this.state.page + 1
        },
        () => {
          this.Token=== "" ? null : this.getConnections();
        }
      );
    }
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.getConnections();
      }
    );
  };

  renderEmpty = () => {
    return <View style={styles.container}>
        <View style={{
             backgroundColor: Colors.white,
            flexDirection: "row",
            borderRadius: 4,
            marginEnd: 10,
            alignItems: "center",
            height: 80,
            width: widthPercentageToDP(95),
            marginStart: 10
            }}>
        <Text style={{
            padding: 30,
            textAlign: 'center',
            fontSize: RFPercentage(2.5),
            fontFamily: "AktivGrotesk-Regular", 
        }}>No Connection Found</Text>
        </View>
    </View>;
  }

  getConnections = async() => {
    if (!this.state.isConnected) {
        this.refs.internetConnection.ShowToastFunction('No Internet Connected');
       return;
     }
     try{
        const status = 'connected';
        const { page, per_page } = this.state;
        this.props.dispatch(connectedUserListRequest());
        const res = await getConnectionList(this.Token, page, per_page, status);
        const code = res.code;
        switch (code) {
          case 200:
            this.props.dispatch(connectedUserListSuccess(res));  
            break;
          case 400: 
          this.props.dispatch(connectedUserListFailed(res));
          case 401:
            this.handleLogout();
          default:
            break;
        }
      } catch (error) {
        this.props.dispatch(connectedUserListFailed(error));
        this.setState({ refreshing: false})
      }
  }
  
  handleLogout = () => {
    AsyncStorage.clear();
    this.doFinish('Login');
  }

  doFinish(screen) {
    this.props.navigation.navigate(screen);
    // this.props.navigation.dispatch(StackActions.popToTop());
  }

  doClick(Data) {
    const { navigate } = this.props.navigation;
    navigate("ConnectionUser", {userData: Data.user.id});
  }

  handleLoading = () => {
    this.setState({
      loadingImg: false,
    });
  }
  renderConnectionItem(item, index) {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => this.doClick(item)}
      >
        <View style={styles.imageView}>
        <View>
          <Image source={{ uri: item.user.profile_pic }} style={styles.userImage}
          onLoadEnd={this.handleLoading}
          />
          <ActivityIndicator
                style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                  backgroundColor: colors.transparent,
                }}
                animating={this.state.loadingImg}
              />
          </View>
          <View style={{position: 'absolute', end:-10, top:3}}>
          <Image source={Icons.tick} style={styles.tickImage} />
          </View>
        </View>
        <View style={styles.detailView}>
          <Text style={styles.nameView}>{item.user.name}</Text>
          <Text style={styles.badgeView}>
            Badge No : {item.user.batch_id}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.state.connections}
          renderItem={({ item, index }) =>
            this.renderConnectionItem(item, index)
          }
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.props.refreshing}
          style={styles.list}
          ListEmptyComponent={this.state.viewEmpty ? this.renderEmpty : null}
          onEndReached={this.handleLoadMore}
          onEndThreshold={50}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
        />
      </View>
    );
  }
}
ConnectionListComponent.propTypes = {
  navigation: PropTypes.object,
  Connections: PropTypes.array,
  refreshing: PropTypes.bool
};

const styles = StyleSheet.create({
  itemContainer: {
    backgroundColor: Colors.white,
    flexDirection: "row",
    borderRadius: 4,
    marginEnd: 10,
    alignItems: "center",
    height: 80
  },
  imageView: {margin: 5, marginStart: 10 },
  userImage: { width: 60, height: 60, borderRadius: 30 },
  tickImage: {
    height: 15,
    width: 15,
  },
  detailView: { flex: 1, marginStart: 10, marginEnd: 10 },
  nameView: {
    // fontFamily: font.Regular,
    //fontSize: 14,
    fontSize: RFPercentage(2.2),
    color: Colors.black
  },
  badgeView: {
    // fontFamily: font.Regular,
    // fontSize: 12,
    fontSize: RFPercentage(2),
    color: Colors.textColorDark
  },
  container: { backgroundColor: Colors.bg_color },
  list: { marginStart: "5%", marginEnd: "5%" }
});

function mapStateToProps(state) {
  return {
    isLoadingConnectedUserList: state.Connection.isLoadingConnectedUserList,
    connectedUserList: state.Connection.connectedUserList,
    errorConnectedUserList: state.Connection.errorConnectedUserList,
    actionConnection: state.Connection.actionConnection,
  };
}
export default connect(mapStateToProps,null)(ConnectionListComponent);
