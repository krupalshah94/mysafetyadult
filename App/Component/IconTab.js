import React, { Component } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import Resource from "../Resource/index";
import colors from "../Resource/Color/index";
import string from "../Resource/string/index";
import { getLangValue } from "../Resource/string/language";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { RFPercentage } from "react-native-responsive-fontsize";

class IconTab extends Component {
  render() {
    let icon = Resource.tab_home;
    let Name = getLangValue(string.tabHome, 'en');
    const { press, focused, index } = this.props;

    if (index === 0) {
      icon = Resource.tab_home;
      Name = getLangValue(string.tabHome, 'en');
    } else if (index === 1) {
      icon = Resource.tab_connection;
      Name =  getLangValue(string.tabConnections, 'en');
    } else if (index === 2) {
      icon = Resource.tab_event;
      Name =  getLangValue(string.tabEvents, 'en');
    } else if (index === 3) {
      icon = Resource.tab_badge;
      Name = getLangValue(string.tabBadge, 'en');
    } else if (index === 4) {
      icon = Resource.tab_setting;
      Name = getLangValue(string.tabSetting, 'en');
    } else {
      icon = Resource.tab_setting;
      Name = getLangValue(string.tabHome, 'en');
    }
    return (
      <View style={{backgroundColor: 'transparent'}}>
      {index === 2?
        <View>

        </View>
      :
      <TouchableOpacity onPress={press}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            height: 70,
            width: 70,
            borderRadius:0,
            bottom: 0,
          }}
        >
          <View
            style={{
              height: 60,
              width: 60,
              borderRadius:0,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: index === 2? colors.blue : "transparent"
            }}
          >
            <Image
              style={{
                height: focused ? wp(12) : wp(10),
                width: focused ? wp(12) : wp(10),
                tintColor: focused ? colors.white : colors.image_tint
              }}
              source={icon}
            />
            {/* <Text
              style={{
                fontSize: RFPercentage(1.2),
                display: "flex",
                color: colors.image_tint
              }}
            >
              {Name}
            </Text> */}
          </View>
        </View>
      </TouchableOpacity>
      }
      </View>
    );
  }
}
export default IconTab;
