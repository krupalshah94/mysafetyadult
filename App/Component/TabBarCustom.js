import React, {Component} from 'react';
import {View, StyleSheet, ImageBackground, SafeAreaView, TouchableOpacity, Image} from 'react-native';
import IconTab from './IconTab';
import Icons from '../Resource/index';
import colors from '../Resource/Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Resource from '../Resource/index';
var eventRoute = '';
class TabBarCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focusedEvent: false,
    }
  }
 render() {
    const {navigation, state} = this.props;
    const {routes} = state;
    return (
      <SafeAreaView style={styles.tabContainer}>
      <View style={{bottom: 0, position: 'absolute'}}>
        <ImageBackground
          source={Icons.tab_blue_back}
          style={{width: wp(100), height: hp(10), flex: 1}}>
          <View style={styles.subContainer}>
            {routes &&
              routes.map((route, index) => {
                focused = index === state.index;
                if (index === 2) {
                  eventRoute = route.name;
                }
                return (
                  <View key={route.key} style={styles.tabView}>
                    <IconTab
                      press={() => navigation.jumpTo(route.name)}
                      key={route.key}
                      index={index}
                      focused={focused}
                      eventFocused={this.handleEventFocused}
                    />
                  </View>
                );
              })}
          </View>
        </ImageBackground>
        <View style={{alignSelf: 'center', position: 'absolute', marginBottom: hp(5), bottom: 0}}>
        <TouchableOpacity onPress={() => navigation.jumpTo(eventRoute)}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            height: 90,
            width: 90,
            borderRadius: 35,
            bottom: 0,
            
          }}
        >
          <View
            style={{
              height: 65,
              width: 65,
              borderRadius: 30,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: colors.blue,
            }}
          >
            <Image
              style={{
                height: state.index === 2 ? wp(12) : wp(10),
                width: state.index === 2 ? wp(12) : wp(10),
                tintColor: state.index  === 2? colors.white : colors.image_tint
              }}
              source={Resource.tab_event}
            />
          </View>
        </View>
      </TouchableOpacity>
        </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  tabContainer: {
  flexDirection: 'row',
  position: 'absolute',
  left: 0,
  right: 0,
  bottom: 0,
  },
  subContainer: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabView: {
    height: hp(20),
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
});

export default TabBarCustom;
