import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  View,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";
import colors from "../Resource/Color";
import font from "../Resource/fontFamily";
import { heightPercentageToDP } from "react-native-responsive-screen";
import { RFPercentage } from "react-native-responsive-fontsize";

class NotificationListComponent extends Component {
  goNotification(screen, Data) {
    const { navigate } = this.props.navigation;
    navigate(screen, { NotificationDetail: Data });
  }

  _renderItem(item, index) {
    return (
      <View>
        <TouchableOpacity
          style={style.notificationView}
          onPress={() => this.goNotification("NotificationDetail", item)}
        >
          <View
            style={[
              style.dotView,
              {
                backgroundColor: this.props.sectionDotColor[
                  Math.floor(Math.random() * this.props.sectionDotColor.length)
                ]
              }
            ]}
          />
          <View style={{ flex: 1 }}>
            <Text style={style.notificationText}>{item.event_title}  <Text style={style.name}>{item.name}</Text></Text>
            <Text style={style.notificationTimeText}>{item.time}</Text>
            <Text style={style.notificationLocationText}>{item.location}</Text>
          </View>
        </TouchableOpacity>
        <View style={style.underline} />
      </View>
    );
  }
  renderOnRefresh() {
  }

  handleLoadMore() {
    if (this.props.page < this.props.last_page) {
      this.props.page = this.props.page + 1;
    }
  }

  render() {
    return (
      <View elevation={5} style={style.container}>
        <ActivityIndicator
          size="large"
          style={{
            display: this.props.refreshing ? "flex" : "none",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
        />
        <FlatList
          onRefresh={() => this.renderOnRefresh()}
          data={this.props.Notification}
          contentContainerStyle={{ paddingBottom: heightPercentageToDP(15) }}
          renderItem={({ item, index }) => this._renderItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.props.refreshing}
          style={{
            display: this.props.refreshing ? "none" : "flex"
          }}
          onEndReached={() => this.handleLoadMore()}
          onEndReachedThreshold={15}
          ListEmptyComponent={
            this.props.viewEmpty ? this.renderEmpty : null
          }
        />
      </View>
    );
  }
}
NotificationListComponent.propTypes = {
  navigation: PropTypes.object,
  Notification: PropTypes.array,
  refreshing: PropTypes.bool,
  sectionDotColor: PropTypes.array,
  last_page: PropTypes.number,
  page: PropTypes.number,
  viewEmpty: PropTypes.bool
};

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
    borderRadius: 5,
    margin:"5%"
  },
  underline: {
    width: "90%",
    height: 0.8,
    backgroundColor: colors.bg_color,
    alignSelf: "center",
    marginStart: 15,
    marginTop: 10,
    marginBottom: 10
  },
  notificationView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    marginStart: 5,
    marginEnd: 10
  },
  dotView: { width: 15, height: 15, borderRadius: 7.5, marginLeft: 10, marginRight: 10},
  notificationText: {
    fontSize: RFPercentage(2.2),
    fontFamily: "AktivGrotesk-Regular", 
    color: colors.black,
    paddingBottom: 10,
  },
  notificationLocationText: {
    fontFamily: "AktivGrotesk-Regular", 
    fontSize: RFPercentage(2.2),
    color: colors.textColorDark,
  },
  notificationTimeText: {
    fontFamily: "AktivGrotesk-Regular", 
    fontSize: RFPercentage(2.2),
    color: colors.textColorDark,
    paddingBottom: 10,
  },
  name: {
    fontSize: RFPercentage(2.2),
    fontFamily: "AktivGrotesk-Regular", 
    color: colors.blue,
    paddingBottom: 10,
  }
});

export default NotificationListComponent;
