import {StyleSheet} from 'react-native';
import colors from '../../Resource/Color/index';
import {RFPercentage} from 'react-native-responsive-fontsize';

const PhoneNumberInputStyle = StyleSheet.create({
  viewBorderNormal: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#DFDFDF',
    borderBottomWidth: 2,
  },
  viewBorderError: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#DFDFDF',
    borderBottomWidth: 2,
  },
  viewInput: {flex: 1, marginStart: 20},
  textError: {
    fontSize: RFPercentage(2),
    fontFamily: "AktivGrotesk-Regular",
    color:  colors.red,
    marginTop: 5,
  },
  inputNumber: {
    color: colors.inputTextColor,
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2),
    marginBottom: 0,
    paddingBottom: 0,
  },
});

export default PhoneNumberInputStyle;
