import React, {PureComponent} from 'react';
import {View, TextInput, Text, StyleSheet} from 'react-native';
import colors from '../../Resource/Color/index';
import strings from '../../Resource/string/index';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {getLangValue} from '../../Resource/string/language';
import {phoneNumberFormat} from './phoneNumberFormat';
import * as data from './countries.json';
import RNPickerSelect from 'react-native-picker-select';
import PhoneNumberInputStyle from './PhoneNumberInputStyle';
class PhoneNumberInput extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isPhoneNumberFocus: false,
      countryCode: undefined,
      phone_number: '',
      err_Phone_Number: undefined,
      countries: [],
      errCountryCode: '',
    };
  }
  componentDidMount() {
    this.setState({countries: data.countries, phone_number: this.props.value});
  }
  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      this.setState({ phone_number: this.props.value, countryCode: this.props.code})
    }
    if (prevProps.errCountryCode !== this.props.errCountryCode) {
      this.setState({ errCountryCode: this.props.errCountryCode });
    }
  }
  updateState(country_code, phoneNumber, errPhoneNumber) {
    if (
      country_code === undefined &&
      phoneNumber === '' &&
      errPhoneNumber === undefined
    ) {
      this.setState({
        err_Phone_Number: this.state.isPhoneNumberFocus
          ? getLangValue(strings.enterPhoneNumber, 'en')
          : undefined,
      });
    } else {
      this.setState({
        countryCode: country_code,
        phone_number: phoneNumber,
        err_Phone_Number: errPhoneNumber,
      });
    }
  }
  onChangeText = text => {
    if (this.props.onNumberChange !== undefined) {
      this.props.onNumberChange(text);
    }

    this.setState(
      {
        phone_number: phoneNumberFormat(text.replace(/[^0-9]/g, '')),
      },
      () => {
        this.state.phone_number === ''
          ? this.setState({
              err_Phone_Number: getLangValue(
                strings.enterPhoneNumber,
                'en',
              ),
            })
          : this.setState({err_Phone_Number: undefined});
      },
    );
  };
  onValueChange = (value, index) => {
    this.setState({countryCode: value, errCountryCode: ''});
  };

  render() {
    const {countryCode, phone_number, err_Phone_Number} = this.state;
    const placeholder = {
      label:
        countryCode !== undefined
          ? countryCode
          : getLangValue(strings.countryCode, 'en'),
      value: undefined,
      color: colors.inputTextColor,
    };

    const {onComplete, label, labelColor, onNumberChange} = this.props; //
    onComplete(countryCode, phone_number, err_Phone_Number, this.phoneInput);

    return (
      <View style={{flex:1}}>
        <View
          style={
            this.state.err_Phone_Number !== undefined
              ? PhoneNumberInputStyle.viewBorderError
              : PhoneNumberInputStyle.viewBorderNormal
          }>
          <View>
            <RNPickerSelect
              placeholder={placeholder}
              items={this.state.countries}
              onValueChange={(value, index) => this.onValueChange(value, index)}
              style={
                this.state.isPhoneNumberFocus
                  ? pickerSelectStyles
                  : pickerStyles
              }
              value={countryCode}
              useNativeAndroidPickerStyle={false}
              ref={el => {
                this.inputRefs = el;
              }}>
              <TextInput
                ref={input => (this.phoneInput = input)}
                fontFamily="AktivGrotesk-Regular"
                placeholder={placeholder.label}
                placeholderTextColor={placeholder.color}
                value={
                  this.state.countryCode !== null &&
                  this.state.countryCode !== undefined
                    ? this.state.countryCode
                    : placeholder.label
                }
                useNativeAndroidPickerStyle={false}
                underlineColorAndroid={colors.colorTransparent}
                keyboardType="numeric"
                returnKeyType="done"
                editable={false}
                style={[
                  PhoneNumberInputStyle.inputNumber,
                  {
                    color:
                      this.state.countryCode !== null &&
                      this.state.countryCode !== undefined
                        ? colors.inputTextColor
                        : colors.textColorLight,
                    backgroundColor: 'white'
                  },
                  
                ]}
              />
            </RNPickerSelect>
          </View>
          <View style={PhoneNumberInputStyle.viewInput}>
            <TextInput
              ref={input => (this.phoneInput = input)}
              placeholder={label}
              placeholderTextColor={labelColor}
              fontFamily="AktivGrotesk-Regular"
              value={this.state.phone_number}
              onChangeText={text => {
                this.onChangeText(text);
              }}
              underlineColorAndroid={colors.colorTransparent}
              keyboardType="numeric"
              returnKeyType="done"
              onSubmitEditing={() => {
                this.state.phoneNumber === ''
                  ? this.setState({
                      err_Phone_Number: getLangValue(
                        strings.enterPhoneNumber,
                        'en',
                      ),
                    })
                  : this.setState({err_Phone_Number: undefined});
              }}
              onBlur={e => {
                this.setState({isPhoneNumberFocus: false});
              }}
              onFocus={e => {
                this.setState({isPhoneNumberFocus: true});
              }}
              style={PhoneNumberInputStyle.inputNumber}
              maxLength={12}
            />
          </View>
        </View>
        <Text style={PhoneNumberInputStyle.textError}>
          {this.state.err_Phone_Number}
        </Text>
        <Text style={PhoneNumberInputStyle.textError}>{this.state.errCountryCode}</Text>
      </View>
    );
  }
}

const pickerStyles = StyleSheet.create({
  inputIOS: {
    fontSize: RFPercentage(2.5),
    fontFamily: "AktivGrotesk-Regular",
    paddingHorizontal: 0,
    color: '#DFDFDF',
    paddingRight: 0,
  },
  inputAndroid: {
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2.5),
    paddingHorizontal: 0,
    color: '#DFDFDF',
    paddingRight: 0,
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: RFPercentage(2.5),
    fontFamily: "AktivGrotesk-Regular",
    paddingHorizontal: 0,
    color: '#DFDFDF',
    paddingRight: 0,
  },
  inputAndroid: {
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2.5),
    paddingVertical: 5,
    paddingHorizontal: 0,
    color: '#DFDFDF',
    paddingRight: 0, // to ensure the text is never behind the icon
    borderBottomColor: '#DFDFDF',
    borderBottomWidth: 2,
  },
});
export default PhoneNumberInput;
