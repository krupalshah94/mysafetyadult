export function phoneNumberFormat(text) {
    var cleaned = ('' + text).replace(/\D/g, '');
    var match = cleaned.match(/(\d{3})(\d{3})(\d{4})$/);
    if (match) {
    var number = [match[1], '-', match[2], '-', match[3]].join('');
    return number;
    }
    return text;
}