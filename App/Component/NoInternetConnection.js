import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import colors from '../Resource/Color'
import images from '../Resource/index';
import { RFPercentage } from 'react-native-responsive-fontsize';
import strings from '../Resource/string/index';
import { getLangValue } from '../Resource/string/language';
export default class NoInternetConnection extends Component {
    render() {
        return (
            <View style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                backgroundColor: colors.white,
                justifyContent: "center",
                alignItems: "center",
            }}>
            <Image source={images.noInternet} style={{ width: 90, height: 90 }} />
                <Text
                style={{
                    textAlign: "center",
                    fontSize: RFPercentage(3),
                    color: colors.inputTextColor,
                    padding: 10
                }}
                >
                {getLangValue(strings.noInternet, 'en')}
                </Text>
            </View>
        )
    }
}
