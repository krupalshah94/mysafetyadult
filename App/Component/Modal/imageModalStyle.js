import { StyleSheet } from 'react-native';
import colors from '../../Resource/Color';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { RFPercentage } from 'react-native-responsive-fontsize';

export const styles = StyleSheet.create({
    flex_1: {flex:1},
    flexRow: {flexDirection: 'row'},
    container: {
        flex: 1,
        backgroundColor: "rgba(66,66,66,0.65)",
        position: "relative",
    },
    modalMainView: {
        marginTop: hp(5),
        marginBottom: hp(15),
        marginStart: wp(5),
        marginEnd: wp(5),
        backgroundColor: colors.white,
        borderRadius: 4,
    },
    modalHeight: {
        height: hp(83)
    },
    headingText: {
        textAlign: 'center', 
        fontSize: RFPercentage(3)
    },
    subHeadingGreen: {
        textAlign: 'center',
        fontSize: RFPercentage(2),
        color: colors.greenText,
    },
    subHeadingBlue: {
        textAlign: 'center',
        fontSize: RFPercentage(2),
        color: colors.blue,
    },
    subHeadingRed: {
        textAlign: 'center',
        fontSize: RFPercentage(2),
        color: colors.red,
    },
    flatListMainView: {
        margin: 10, 
        flex: 1, 
        justifyContent: 'center'
    },
    imageMainView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 1,        
    },
    image: {
        width: wp(16),
        height: wp(16),
    },
    imageSelected: {
        width: wp(16),
        height: wp(16),
        position: 'absolute',
    },
    nextBtnMainView: {
        margin: 15,
        position: 'absolute',
        bottom: hp(10),
        width: wp(60),
        alignSelf: 'center'
    },
    nextBtn: {
        backgroundColor: "#3444D6",
        borderRadius: 20,
        alignItems: "center"
    },
    nextBtmTitle: {
        fontFamily: "AktivGrotesk-Bold",
        fontSize: 16
    },
})