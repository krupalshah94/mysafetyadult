import { StyleSheet } from 'react-native';
import colors from '../../Resource/Color';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { RFPercentage } from 'react-native-responsive-fontsize';
export const styles = StyleSheet.create({
flex_1: {flex:1},
flexRow: {flexDirection: 'row'},
container: {
    flex: 1,
    backgroundColor: "rgba(66,66,66,0.65)",
    position: "relative",
},
modalMainView: {
    marginTop: hp(5),
    marginBottom: hp(15),
    marginStart: wp(5),
    marginEnd: wp(5),
    backgroundColor: colors.white,
    borderRadius: 4,
},
modalHeight: {
    height: hp(83)
},
inviteConnectionView: {
    marginTop: hp(2),
},
inviteConnectionText: {
    textAlign: 'center', 
    fontSize: RFPercentage(3)
},
closeModalView: {
    width: wp(30), 
    margin: 10,
    borderRadius: 12,
    backgroundColor: colors.white,
    alignSelf: 'center',
    borderColor: colors.bg_btn_blue,
    borderWidth: StyleSheet.hairlineWidth
},
closeBtnText: {
    textAlign: 'center', 
    color: colors.black, 
    padding: 5
},
inviteBtnView: {
    width: wp(30), 
    margin: 10,
    borderRadius: 12,
    backgroundColor: colors.bg_btn_blue,
    alignSelf: 'center'
},
inviteBtnText: {
    textAlign: 'center', 
    color: '#FFFFFF', 
    padding: 5
}


});
