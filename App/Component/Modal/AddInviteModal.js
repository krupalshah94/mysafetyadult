import React, { Component } from 'react'
import { Text, View, Modal, FlatList, TouchableOpacity, StyleSheet } from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { RFPercentage } from 'react-native-responsive-fontsize';
import Icons from '../../Resource/index';
import InviteConnectionItems from '../../Screen/home/Subcomponents/InviteConnectionItems';
import colors from '../../Resource/Color/index';
import string from '../../Resource/string';
import {getLangValue} from '../../Resource/string/language';
import { styles } from './inviteModalStyle';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-community/async-storage';
import * as Constants from '../../Resource/Constants';
import BarIndicatorLoader from '../../Component/BarIndicatorLoader';
import CustomToast from '../../Component/CustomToast';
import { connect } from 'react-redux';
import {getConnectionList} from '../../Network/Service';
import {connectedUserListRequest, connectedUserListSuccess, connectedUserListFailed } from '../../Store/Actions/ConnectionAction';

class AddInviteModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: false,
            loading: false,
            viewEmpty: true,
            refreshing: false,
            page: 1,
            per_page: 10,
            last_page: 0,
            connectionData: [],
            connections: [],
            selectedUser: {},
            invitedUserId: undefined,
        }
    }

    componentDidMount() {
        this._subscription = NetInfo.addEventListener(
          this._handleConnectivityChange
        );
        this.getToken();
      }
    
      componentWillUnmount() {
        this._subscription && this._subscription();
      }
    
      _handleConnectivityChange = state => {
        this.setState({
          isConnected: state.isConnected
        });
      };

      componentDidUpdate(prevProps) {
        if (prevProps.isLoadingConnectedUserList !== this.props.isLoadingConnectedUserList) {
            this.setState({ loading: this.props.isLoadingConnectedUserList });
        }
        if (prevProps.connectedUserList !== this.props.connectedUserList) {
          if (this.props.connectedUserList.data.data.length === 0) {
            this.setState({ viewEmpty: true, refreshing: false, }) 
          } else {
            this.setState({ viewEmpty: false,
              connections: this.state.page === 1? this.props.connectedUserList.data.data : [...this.state.connections, ...this.props.connectedUserList.data.data],
            refreshing: false,
            last_page: this.props.connectedUserList.data.last_page
            }, () => {
                let connectionDataUpdate = [...this.state.connections];
                let connectionData = [];
                connectionDataUpdate.length > 0 && connectionDataUpdate.map((data, index) => {
                  if(data.user.id) {
                        connectionData.push({
                        id: data.user.id,
                        name: data.user.name,
                        image: data.user.profile_pic,
                        badge: data.user.batch_id,
                        time: data.created_humans,
                        selected: false,
                    }); 
                  }
                });
                this.setState({ connectionData: connectionData});
            })
          }
        }
    }

      async getToken() {
        this.Token = await AsyncStorage.getItem(Constants.TOKEN);
        if (this.Token !== '') {
            this.getConnections();
        }
      }

      handleLoadMore = () => {
        if (this.state.page != this.state.last_page) {
          this.setState(
            {
              page: this.state.page + 1
            },
            () => {
              this.Token=== "" ? null : this.getConnections();
            }
          );
        }
      };
    
      handleRefresh = () => {
        this.setState(
          {
            page: 1,
            refreshing: true
          },
          () => {
            this.getConnections();
          }
        );
      };

      renderEmpty = () => {
        return <View style={styles.container}>
            <View style={{
                width: wp(90), 
                height: hp(100),
                margin: 20,
                }}>
            <Text style={{
                textAlign: 'center',
                fontSize: RFPercentage(3),
                fontFamily: "AktivGrotesk-Regular", 
            }}>No Connection Found</Text>
            </View>
        </View>;
      }

      getConnections = async() => {
        if (!this.state.isConnected) {
            this.refs.internetConnection.ShowToastFunction('No Internet Connected');
           return;
         }
         try{
            const status = 'connected';
            const { page, per_page } = this.state;
            this.props.dispatch(connectedUserListRequest());
            const res = await getConnectionList(this.Token, page, per_page, status);
            const code = res.code;
            switch (code) {
              case 200:
                this.props.dispatch(connectedUserListSuccess(res));  
                break;
              case 400: 
              this.props.dispatch(connectedUserListFailed(res));
              default:
                break;
            }
          } catch (error) {
            this.props.dispatch(connectedUserListFailed(error));
            this.setState({ refreshing: false})
          }
      }

      

    handleSelectedValue = (id,index,selection) => {
        let updateList = [...this.state.connectionData];
        updateList.map((data) => {
           data.selected = false;
        });
        updateList[index].selected = true;
        this.setState({ connectionData: updateList, selectedUser: updateList[index] });
    }
    render() {
        const { showModal, closeModal, handleInvite } = this.props;
        const { connectionData } = this.state;
        return (
            <Modal
                visible={showModal}
                onRequestClose={closeModal}
                animationType="slide"
                transparent={true}
            >
                <View style={styles.container}>
                    <View style={styles.modalMainView}>
                        <View style={styles.modalHeight}>
                            <View style={styles.inviteConnectionView}>
                                <Text style={styles.inviteConnectionText}>
                                {getLangValue(string.inviteYourConnections, 'en')}</Text>
                            </View>
                            <View style={styles.flex_1}>
                                <FlatList 
                                    data={connectionData}
                                    keyExtractor={(item, index) =>index.toString()}
                                    renderItem={({ item, index }) => {
                                        return  <InviteConnectionItems item={item} index={index} navigation={this.props.navigation} 
                                        handleConnection={this.handleSelectedValue} />
                                    }}
                                    ListEmptyComponent={this.state.viewEmpty ? this.renderEmpty : null}
                                    onEndReached={this.handleLoadMore}
                                    onEndThreshold={50}
                                    onRefresh={this.handleRefresh}
                                    refreshing={this.state.refreshing}
                                />
                             <View style={styles.flexRow}>
                             <View style={{flex:1}}>
                                    <TouchableOpacity onPress={closeModal}>
                                       <View style={styles.closeModalView}>
                                        <Text style={styles.closeBtnText}>
                                        {getLangValue(string.close, 'en')}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex:1}}>
                                    <TouchableOpacity onPress={() => {
                                                        handleInvite(this.state.selectedUser);
                                                        closeModal();                
                                                        }}>
                                       <View style={styles.inviteBtnView}>
                                        <Text style={styles.inviteBtnText}>{getLangValue(string.invite, 'en')}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
            <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
            </Modal>
        )
    }
}
function mapStateToProps(state) {
    return {
      isLoadingConnectedUserList: state.Connection.isLoadingConnectedUserList,
      connectedUserList: state.Connection.connectedUserList,
      errorConnectedUserList: state.Connection.errorConnectedUserList,
      actionConnection: state.Connection.actionConnection,
    };
  }
  export default connect(mapStateToProps,null)(AddInviteModal);
