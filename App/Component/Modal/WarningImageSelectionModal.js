import React, {Component} from 'react';
import {
  Text,
  View,
  Modal,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Icons from '../../Resource/index';
import colors from '../../Resource/Color/index';
import { Button } from "react-native-elements";
import string from '../../Resource/string';
import { getLangValue } from '../../Resource/string/language';
import { styles } from '../../Component/Modal/imageModalStyle';
import {getSafetyImagesRequest, getSafetyImagesSuccess, getSafetyImagesFailed} from '../../Store/Actions/EventAction';
import NetInfo from "@react-native-community/netinfo";
import { StackActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import * as Constants from '../../Resource/Constants';
import BarIndicatorLoader from '../../Component/BarIndicatorLoader';
import { connect } from 'react-redux';
import CustomToast from '../CustomToast';
import {getImageList} from '../../Network/Service';

class WarningImageSelectionModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: false,
      loading: false,
      images: [],
      selectedSafetyImage: '',
      errSelectedImage: '',
      selectedImageId: '',
    };
    this.Token = '';
  }

  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoadingSafetyImages !== this.props.isLoadingSafetyImages) {
      this.setState({ loading: this.props.isLoadingSafetyImages})
    }
    if (prevProps.selectedImageId !== this.props.selectedImageId) {
      this.setState({selectedSafetyImage: this.props.selectedImageId});
      let images = [...this.state.images];
     images.map((data, index) => {
      if (data.id === this.props.selectedImageId) {
        images.splice(index,1);
       }
     });
      this.setState({ images });
    }
  }

  componentWillUnmount() {
    this._subscription && this._subscription();
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    if (this.Token !== '') {
      // this.getIncomingEvents();
      this.getImages();
    }
  }

  getImages = async () => {
    const {selectedSafetyImage} = this.state;
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    try{
      this.props.dispatch(getSafetyImagesRequest());
      const res = await getImageList(this.Token);
      const code = res.code;
      switch (code) {
        case 200:
        let images = [...this.state.images];
        if (res.data.safety_images.length > 0) {
          if (selectedSafetyImage !== '' || selectedSafetyImage !== undefined) {
            res.data.safety_images.map((data) => {
              if (parseInt(selectedSafetyImage) !== parseInt(data.id)) {
              images.push({
                id: data.id,
                src: data.image_url,
                selected: false,
              });
            }
            });
            this.setState({ images });
          }
        }
          this.props.dispatch(getSafetyImagesSuccess(res));
          break;
        case 400:
          this.props.dispatch(getSafetyImagesFailed(res));
          break;
        
        default:
          break;
      }
    } catch(error) {
      this.props.dispatch(getSafetyImagesFailed(error));
    }
  }

  handleSafetyImage = id => {
    let updatedList = [...this.state.images];
    updatedList.map(data => {
      if (data.id === id) {
        data.selected = !data.selected;
        if (data.selected === true) {
          this.setState({selectedImageId: id, errSelectedImage: ''});
        } else {
          this.setState({selectedImageId: ''});
        }
      } else {
        data.selected = false;
      }
    });
    this.setState({images: updatedList});
  };

  handleLogout = () => {
    AsyncStorage.clear();
    this.doFinish('Login');
  }

  doFinish(screen) {
    // this.props.navigation.dispatch(StackActions.popToTop());
  }

  handleSubmit = () => {
    if (this.state.selectedImageId === '') {
      this.setState({ errSelectedImage: getLangValue(string.enterSelectedImage, 'en')});
      return;
    }
    this.props.handleSubmit(this.state.selectedImageId);
    this.props.closeModal();
  }
  render() {
    const {showModal, closeModal} = this.props;
    return (
      <Modal
        visible={showModal}
        onRequestClose={closeModal}
        animationType="slide"
        transparent={true}>
        <View
          style={styles.container}>
          <View>
          <View
            style={styles.modalMainView}>
            <View style={styles.modalHeight}>
              <View style={{marginTop: hp(4)}}>
                <Text style={styles.headingText}>
                {getLangValue(string.chooseWarning, 'en')}
                </Text>
              </View>
              <View style={{marginTop: hp(3)}}>
                <Text
                  style={styles.subHeadingRed}>
                  {getLangValue(string.selectWarning, 'en')}
                </Text>
              </View>
              <View style={styles.flatListMainView}>
                <FlatList
                  data={this.state.images}
                  keyExtractor={(item, index) => index.toString()}
                  numColumns={4}
                  style={{ flex: 1,marginVertical: 20}}
                  renderItem={({item, index}) => (
                    <TouchableOpacity
                      onPress={() => this.handleSafetyImage(item.id)}>
                          <View  style={[styles.imageMainView, {height: Dimensions.get('window').width / 4,}]}>
                       <View style={{marginBottom: 5,
                                      alignItems: 'center',
                                      justifyContent: 'center',
                                      marginEnd: wp(5),}}>
                        <Image
                         style={[styles.image,{opacity: item.selected ? 1 : 0.5}]}
                         source={{uri:item.src}}
                        />
                        {item.selected && (
                          <Image
                            source={Icons.selectedRed}
                            style={styles.imageSelected}
                          />
                        )}
                      </View>
                      </View>
                    </TouchableOpacity>
                  )}
                />
                   <Text style={{ color: colors.red, fontSize: RFPercentage(3), fontFamily: "AktivGrotesk-Regular", marginBottom: 20, marginStart: 5 }}>
            {this.state.errSelectedImage}</Text>
              </View>
             
            </View>
          </View>
          <View style={styles.nextBtnMainView}>
              <Button
                onPress={this.handleSubmit}
                title={getLangValue(string.submit, 'en')}
                buttonStyle={styles.nextBtn}
                // titleStyle={styles.nextBtmTitle}
                textStyle={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Bold",
                }}
              />
              </View>
          </View>
        </View>
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoadingSafetyImages: state.Event.isLoadingSafetyImages,
    getImagesList: state.Event.getImagesList,
    errorImages: state.Event.errorImages
  };
}
export default connect(mapStateToProps, null)(WarningImageSelectionModal);