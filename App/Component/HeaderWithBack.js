import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, Platform } from "react-native";
import PropTypes from "prop-types";
import colors from "../Resource/Color";
import Resource from "../Resource/index";
import { RFPercentage } from "react-native-responsive-fontsize";
import { heightPercentageToDP } from "react-native-responsive-screen";

class HeaderWithBack extends Component {
  goNotification = (screen) => {
    const { navigate } = this.props.navigation;
    navigate(screen);
  }
  render() {
    return (
      <View style={style.container} elevation={5}>
          {this.props.isBackNeeded &&
          <View style={style.backBtnView}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
              <Image style={{ width: 24, height: 24 }} source={Resource.back_icon} />
            </TouchableOpacity>
          </View>
        }  
          <View style={style.headerTitleTax}>
            <Text style={style.text}>{this.props.name}</Text>
          </View>
        <View style={style.containerSubView}>
          <TouchableOpacity
            onPress={() => {
              this.goNotification("Notification")
            }}
          >
            <Image source={Resource.alarm} style={style.image} />
            <View
              style={[
                style.dot,
                {
                  backgroundColor: this.props.notify
                    ? colors.dotColor
                    : colors.transparent
                }
              ]}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
HeaderWithBack.propTypes = {
  navigation: PropTypes.object,
  name: PropTypes.any,
  notify: PropTypes.bool,
  isBackNeeded: PropTypes.bool,
};

HeaderWithBack.defaultProps =
{
  notify: false,
  isBackNeeded: true,
}

const style = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: colors.white,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerSubView: {position: 'absolute', end:10},
  backBtnView: {flex:1},
  headerTitleTax: {flex:6},
  text: { textAlign: 'left',color: colors.black, fontSize: RFPercentage(2.5),  fontFamily: "AktivGrotesk-Regular", paddingTop:Platform.OS === "ios"? 5 : 2.5 },
  image: { width: 24, height: 24, overflow: "hidden" },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    position: "absolute",
    top: 0
  }
});

export default HeaderWithBack;
