import React, { Component } from 'react'
import {
    View, BackHandler,
  } from "react-native";
import AllEventListComponents from './AllEventListComponents';

export default class CompletedEventListComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
          sectionDotColor: ["#4181DF"],
        }
    }


  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }
  
    render() {
        return (
            <View style={{flex:1}}>
                <View style={{ marginTop: 10, flex:1 }}>
                <AllEventListComponents
                    navigation={this.props.navigation}
                    upcomingEvents={[]}
                    sectionDotColor={this.state.sectionDotColor}
                />
                </View>
            </View>
        )
    }
}
