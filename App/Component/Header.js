import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import colors from "../Resource/Color";
import Resource from "../Resource/index";
import { RFPercentage } from "react-native-responsive-fontsize";
import { heightPercentageToDP } from "react-native-responsive-screen";

class Header extends Component {
  goNotification = (screen) => {
    const { navigate } = this.props.navigation;
    navigate(screen);
  }
  render() {
    return (
      <View style={style.container} elevation={5}>
        <View style={style.headerTitleTax}>
          <Text style={style.text}>{this.props.name}</Text>
        </View>
        <View style={style.containerSubView}>
          <TouchableOpacity
            onPress={() => {
              this.goNotification("Notification")
            }}
          >
            <Image source={Resource.alarm} style={style.image} />
            <View
              style={[
                style.dot,
                {
                  backgroundColor: this.props.notify
                    ? colors.dotColor
                    : colors.transparent
                }
              ]}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
Header.propTypes = {
  navigation: PropTypes.object,
  name: PropTypes.any,
  notify: PropTypes.bool
};

const style = StyleSheet.create({
  container: {
    shadowColor: colors.bg_color,
    shadowOpacity: 1,
    shadowOffset: { height: 1, width: 0 },
    flexDirection: "row",
    backgroundColor: colors.white,
    height: heightPercentageToDP(7),
    padding: 20,
    alignItems: 'center'
  },
  containerSubView: {flex: 1},
  backBtnView: {flex: 1},
  headerTitleTax: {flex: 6},
  text: { color: colors.black, fontSize: RFPercentage(2.5) },
  image: { width: 24, height: 24, overflow: "hidden" },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    position: "absolute",
    top: 0
  }
});

export default Header;
