import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  View,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";
import colors from "../Resource/Color/index";
import { heightPercentageToDP } from "react-native-responsive-screen";
import { RFPercentage } from "react-native-responsive-fontsize";

class ConnectionUserComponent extends Component {
  doClick(screen, Data) {
    // const { navigate } = this.props.navigation;
    // navigate(screen, { NotificationDetail: Data });
  }

  _renderItem(item, index) {
    return (
      <View>
        <TouchableOpacity
          style={style.notificationView}
          onPress={() => this.doClick("NotificationDetail", item)}
        >
          <View>
            <Text style={style.notificationText}>{item.event_title} <Text style={style.name}>{item.name}</Text></Text>
            <Text style={style.notificationTimeText}>{item.time}</Text>
            <Text style={style.notificationLocationText}>{item.location}</Text>
          </View>
        </TouchableOpacity>
        <View style={style.underline} />
      </View>
    );
  }

  render() {
    return (
      <View >
        <ActivityIndicator
          size="large"
          style={{
            display: this.props.refreshing ? "flex" : "none",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
        />
        <FlatList
          data={this.props.Notification}
          contentContainerStyle={{ paddingBottom: heightPercentageToDP(15) }}
          renderItem={({ item, index }) => this._renderItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
ConnectionUserComponent.propTypes = {
  navigation: PropTypes.object,
  Notification: PropTypes.array
};

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
    borderRadius: 5,
    marginTop: 20,
   
  },
  underline: {
    width: "90%",
    height: 1,
    backgroundColor: colors.bg_color,
    alignSelf: "center",
    marginStart: 0,
    marginTop: 0,
    marginBottom: 0
  },
  notificationView: {
    marginTop: 15,
    marginBottom: 15,
    marginStart: 15,
    marginEnd: 15
  },

  notificationText: {
    fontSize: RFPercentage(2.2),
    fontFamily: "AktivGrotesk-Regular", 
    color: colors.black,
    paddingBottom: 10,
  },
  notificationLocationText: {
    fontFamily: "AktivGrotesk-Regular", 
    fontSize: RFPercentage(2.2),
    color: colors.textColorLight,
  },
  notificationTimeText: {
    fontFamily: "AktivGrotesk-Regular", 
    fontSize: RFPercentage(2.2),
    color: colors.textColorDark,
    paddingBottom: 10,
  },
  name: {
    fontSize: RFPercentage(2.2),
    fontFamily: "AktivGrotesk-Regular", 
    color: colors.blue,
    paddingBottom: 10,
  }
});

export default ConnectionUserComponent;
