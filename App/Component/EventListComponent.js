import React, { PureComponent } from "react";
import { View, TouchableOpacity, StyleSheet, SectionList, Image } from "react-native";
import { Text } from "react-native-elements";
import Colors from "../Resource/Color/index";
import PropTypes from "prop-types";
import { heightPercentageToDP, widthPercentageToDP } from "react-native-responsive-screen";
import { RFPercentage } from "react-native-responsive-fontsize";
import AsyncStorage from "@react-native-community/async-storage";
import * as Constants from '../Resource/Constants';
import { getIncomingEvents, actionRequestEvents, addImagesToEvents } from '../Network/Service';
import { getEventListFailed, getEventListSuccess, getEventListRequest, actionEventRequest, actionEventSuccess, actionEventFailed, eventCompleted, addNewEventFailed, addNewEventSuccess, addNewEventRequest  } from '../Store/Actions/EventAction';
import { connect } from "react-redux";
import NetInfo from "@react-native-community/netinfo";
import CustomToast from "./CustomToast";
import colors from "../Resource/Color/index";
import images from '../Resource/index';
import { StackActions } from "react-navigation";
import BarIndicatorLoader from "./BarIndicatorLoader";
import SafetyImageSelectionModal from "./Modal/SafetyImageSelectionModal";
import WarningImageSelectionModal from "./Modal/WarningImageSelectionModal";
const eventStatus = {
  awaiting: colors.awaitingEvent,
  scheduled: colors.scheduledEvent,
  completed: colors.completedEvent,
  cancelled: colors.cancelledEvent
};
class EventListComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      perPage: 10,
      refreshing: false,
      loading: false,
      events: [],
      errorEvents: undefined,
      upcomingEvents: this.props.upcomingEvents,
      isConnected: false,
      last_page: 0,
      viewEmpty: true,
      showSafetyImageModal: false,
      showWarningImageModal: false,
      selectedImageId: '',
      selectedWarningImage: '',
      event_id: '',
    };
    this.Token = '';
    this.UserData = '';
  }

  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentWillUnmount() {
    this._subscription && this._subscription();
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.getList !== this.props.getList) { 
        if (this.props.getList.data.incoming_events.length === 0) {
          this.setState({ viewEmpty: true, loading: false, events: [], refreshing: false}) 
       } else {
          this.setState({ viewEmpty: false, loading: false, 
            events: this.state.page === 1?  this.props.getList.data.incoming_events : [...this.state.events, ...this.props.getList.data.incoming_events], 
            refreshing: false, last_page: this.props.getList.data.last_page }) 
       }
    }
    if (prevProps.getListError !== this.props.getListError) {
      this.setState({ errorEvents: this.props.getListError })
    }

    if (prevProps.isLoadingActionEvent !== this.props.isLoadingActionEvent) {
      this.setState({ loading: this.props.isLoadingActionEvent});
    }
    if(prevProps.isCompletedEvent !== this.props.isCompletedEvent) {
      this.getIncomingEvents();
      this.props.dispatch(eventCompleted(false));
    }
    if (prevProps.isLoadingAddNewEvent !== this.props.isLoadingAddNewEvent) {
      this.setState({ loading: this.props.isLoadingAddNewEvent})
    }
    if (prevProps.addNewEvent !== this.props.addNewEvent) {
      if (this.props.addNewEvent.code === 200 && this.props.addNewEvent.status === "true") {
        this.getIncomingEvents();
      }
    }
  }

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    this.UserData = await AsyncStorage.getItem(Constants.USER_DATA);
    this.UserData = JSON.parse(this.UserData);
    if (this.Token !== '' && this.UserData !== '') {
      this.getIncomingEvents();
    }
  }

  handleLogout = () => {
    AsyncStorage.clear();
    this.doFinish('Login');
  }

  doFinish(screen) {
    this.props.navigation.navigate(screen);
    // this.props.navigation.dispatch(StackActions.popToTop());
  }


  getIncomingEvents = async () => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    const { perPage, page } = this.state;
    try {
      this.props.dispatch(getEventListRequest());
      const res = await getIncomingEvents(this.Token, page, perPage);
      const code = res.code;
      switch (code) {
        case 200:
          this.props.dispatch(getEventListSuccess(res));
          break;
        case 400:
          this.props.dispatch(getEventListFailed(res));
          this.refs.errorEvents.ShowToastFunction(res.message);
        case 401:
          this.handleLogout();
          this.refs.errorEvents.ShowToastFunction(res.message);
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(getEventListFailed(error));
    }
  }

  doClick(Data) {
    const { navigate } = this.props.navigation;
    navigate("EventDetail", { EventDetail: Data });
  }

  renderItemSeparator() {
    return <View style={styles.ItemSeparator} />;
  }
  renderSectionHeader(section) {
    return (
      <View style={styles.sectionContainer}>
        <View style={styles.sectionLeft} />
        <Text style={styles.sectionText}>{section.title}</Text>
        <View style={styles.sectionRight} />
      </View>
    );
  }

  acceptEventRequest = async (item) => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    const event_id = item.event_id;
    const approval_status = 'accept';
    this.setState({ event_id: item.event_id});
    try {
      this.props.dispatch(actionEventRequest());
      const res = await actionRequestEvents(this.Token, event_id, approval_status);
      const code = res.code;
      switch (code) {
        case 200:
          this.setState({events: [], page:1})
          this.getIncomingEvents();
          this.handleSafetyImageModal(true)
          this.props.dispatch(actionEventSuccess(res));
          // this.refs.accept.ShowToastFunction(res.message);
        break;
        case 400:
          this.props.dispatch(actionEventFailed(res));        
          // this.refs.accept.ShowToastFunction(res.message);
          break;
        case 401:
          this.props.dispatch(actionEventFailed(res)); 
          // this.refs.accept.ShowToastFunction(res.message);
          break;
        
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(actionEventFailed(error)); 
    }
  }

  declineEventRequest = async (item) => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    const event_id = item.event_id;
    const approval_status = 'decline';
    try {
      this.props.dispatch(actionEventRequest());
      const res = await actionRequestEvents(this.Token, event_id, approval_status);
      const code = res.code;
      switch (code) {
        case 200:
          this.setState({events: [], page:1})
          this.getIncomingEvents();
          this.props.dispatch(actionEventSuccess(res));
          //  this.refs.errorEvents.ShowToastFunction(res.message);
        break;
        case 400:
          this.props.dispatch(actionEventFailed(res));        
           this.refs.errorEvents.ShowToastFunction(res.message);
          break;
        case 401:
          this.props.dispatch(actionEventFailed(res)); 
           this.refs.errorEvents.ShowToastFunction(res.message);
          break;
        
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(actionEventFailed(error)); 
    }
  }

  handleConnectionName = (item) => {
    this.props.navigation.navigate("ConnectionUser", {userData: item.created_by !== this.UserData.id ? item.created_by : item.user_id});
  }

  renderSectionItem(item, index, section) {
    return (
      <View style={styles.SectionItemContainer} >
        <View
          style={[
            styles.dotView,
            {
              backgroundColor: this.props.sectionDotColor[
                Math.floor(Math.random() * this.props.sectionDotColor.length)
              ]
            }
          ]}
        />
        <View style={styles.flex}>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
              <View style={{flexDirection:'row'}}>
              <Text style={styles.eventTitle}>{item.event_title} with </Text>
              <TouchableOpacity onPress={() => this.handleConnectionName(item)}><Text style={styles.name}>{item.created_by !== this.UserData.id ? item.created_by_name: item.user_name}</Text></TouchableOpacity>
              </View>
               <Text style={styles.location}>{item.location}</Text>
              <Text style={styles.time}>{item.time}</Text>
              <Text style={styles.createdBy}>Created By {item.created_by_name}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              {item.created_by !== this.UserData.id ?
              item.event_status === 'request'?
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{flex:1, marginTop: 5, marginRight: 10}}>
                  <TouchableOpacity onPress={() => this.acceptEventRequest(item)}>
                    <View style={{
                      backgroundColor: colors.bg_btn_blue,
                      borderRadius: 20,
                    }}>
                      <Text style={{
                        textAlign: 'center',
                        color: colors.white,
                        fontSize: RFPercentage(2),
                        paddingTop: 6,
                        paddingBottom: 6,
                        paddingStart: 10,
                        paddingEnd: 10,
                      }}>Accept</Text>
                    </View>
                    </TouchableOpacity>
                  </View>
                  <View style={{ marginTop: 5, flex:1 }}>
                  <TouchableOpacity onPress={() => this.declineEventRequest(item)}>
                    <View style={{
                      backgroundColor: '#ffffff',
                      borderRadius: 20,
                      borderWidth: 1,
                      borderColor: colors.bg_btn_blue
                    }}>
                      <Text style={{
                        textAlign: 'center',
                        fontSize: RFPercentage(2),
                        paddingTop: 6,
                        paddingBottom: 6,
                        paddingStart: 10,
                        paddingEnd: 10,
                      }}>Decline</Text>
                    </View>
                    </TouchableOpacity>
                  </View>
                </View>
                : <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{backgroundColor: eventStatus[item.event_status], borderRadius: 20, padding: 5, marginTop: 5, flex:1, marginRight: 10}}>
                    <Text style={{
                      textAlign: 'center',
                      color: colors.white,
                      fontSize: RFPercentage(2),
                      textTransform:'capitalize'
                    }}>{item.event_status}</Text>
                  </View>
                  <View style={{ marginTop: 5, flex:1 }}>
                    <TouchableOpacity onPress={() => this.doClick(item)}>
                      <View style={{
                        backgroundColor: colors.bg_btn_blue,
                        borderRadius: 20,
                      }}>
                        <Text style={{
                          textAlign: 'center',
                          color: colors.white,
                          fontSize: RFPercentage(2),
                          paddingTop: 6,
                          paddingBottom: 6,
                          paddingStart: 10,
                          paddingEnd: 10,
                        }}>View</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View> 
                : <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{backgroundColor: eventStatus[item.event_status], borderRadius: 20, padding: 5, marginTop: 5, flex:1, marginRight: 10}}>
                    <Text style={{
                      textAlign: 'center',
                      color: colors.white,
                      fontSize: RFPercentage(2),
                      textTransform:'capitalize',
                    }}>{item.event_status}</Text>
                  </View>
                  <View style={{ marginTop: 5, flex:1 }}>
                    <TouchableOpacity onPress={() => this.doClick(item)}>
                      <View style={{
                        backgroundColor: colors.bg_btn_blue,
                        borderRadius: 20,
                      }}>
                        <Text style={{
                          textAlign: 'center',
                          color: colors.white,
                          fontSize: RFPercentage(2),
                          paddingTop: 6,
                          paddingBottom: 6,
                          paddingStart: 10,
                          paddingEnd: 10,
                        }}>View</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>}
            </View>
          </View>
        </View>
      </View>
    );
  }

  renderEmpty = () => {
    return <View>
    <View>
      <View style={{
        justifyContent: 'center', 
        alignItems: 'center', 
        // backgroundColor: colors.white,
        marginTop: 20,
        }}>
        <Image source={images.noEvents} style={{width: widthPercentageToDP(30), height: widthPercentageToDP(30)}} />
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
        <Text style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: RFPercentage(3),
              color: Colors.textColorLight,
              paddingBottom: 10,
        }}>No Events Found</Text>
      </View>
    </View>
  </View>;
  }

  handleLoadMore = () => {
    if (this.state.page != this.state.last_page) {
      this.setState(
        {
          page: this.state.page + 1
        },
        () => {
          this.Token === "" ? null : this.getIncomingEvents();
        }
      );
    }
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.getIncomingEvents();
      }
    );
  };

  handleNext(id) {
    this.setState({ selectedImageId: id});
    this.handleSafetyImageModal(false);
    this.handleWarningImageModal(true);
  }

  handleSafetyImageModal = (visible) => {
    this.setState({ showSafetyImageModal: visible });
  }

  handleSubmit = (id) => {
    this.setState({ selectedWarningImage: id}, async() => {
      const { selectedWarningImage, selectedImageId, event_id } = this.state;
      const userData = {
        user_id: this.UserData.id,
        safe_image_id: selectedImageId,
        unsafe_image_id: selectedWarningImage,
      }
      try {
        this.props.dispatch(addNewEventRequest());
        const res = await addImagesToEvents(this.Token, event_id, userData);
        const code = res.code;
        switch (code) {
          case 200:
            this.props.dispatch(addNewEventSuccess(res));
          break;
          case 400:
            this.props.dispatch(addNewEventFailed(res));
          this.refs.errorEvent.ShowToastFunction(res.message);
          break;
          case 401:
            this.props.dispatch(addNewEventFailed(res));
          this.refs.errorEvent.ShowToastFunction(res.message);
          break;
          default:
            break;
        }
      } catch (error) {
        this.props.dispatch(addNewEventFailed(error));
      }
    });
  }

  handleWarningImageModal = (visible) => {
    this.setState({ showWarningImageModal: visible });
  }

  render() {
    const { showSafetyImageModal, showWarningImageModal } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.listView}>
          <SectionList
            contentContainerStyle={{ paddingBottom: heightPercentageToDP(10) }}
            renderItem={({ item, index, section }) =>
              this.renderSectionItem(item, index, section)
            }
            renderSectionHeader={({ section }) =>
              this.renderSectionHeader(section)
            }
            ItemSeparatorComponent={() => this.renderItemSeparator()}
            sections={this.state.events}
            keyExtractor={(item, index) => item + index}
            stickySectionHeadersEnabled={false}
            // style={styles.listView}
            ListEmptyComponent={this.state.viewEmpty ? this.renderEmpty : null}
            onEndReached={this.handleLoadMore}
            onEndThreshold={50}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
          />
        </View>
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        <CustomToast ref="errorEvents" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {/* <CustomToast ref="accept" backgroundColor={colors.white} textColor={colors.black} position="top" /> */}
        <SafetyImageSelectionModal showModal={showSafetyImageModal} handleNext={(id) => this.handleNext(id)} 
        closeModal={() => this.handleSafetyImageModal(false)} />
        <WarningImageSelectionModal selectedImageId={this.state.selectedImageId} showModal={showWarningImageModal} handleSubmit={(id) => this.handleSubmit(id)} closeModal={() => this.handleWarningImageModal(false)} />
        
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
      </View>
    );
  }
}

EventListComponent.propTypes = {
  navigation: PropTypes.object,
  upcomingEvents: PropTypes.array,
  sectionDotColor: PropTypes.array
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: Colors.bg_color },
  listView: {
    marginStart: "5%",
    marginEnd: "5%",
    marginBottom: "5%",
    backgroundColor: Colors.white,
    flex: 1
  },
  ItemSeparator: {
    height: 1,
    backgroundColor: Colors.itemSeparator,
    marginStart: 10,
    marginEnd: 10,
    flex: 1,
  },
  sectionContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
  },
  sectionLeft: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.line,
    marginStart: 10,
    marginEnd: 5
  },
  sectionText: {
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2.2),
    color: Colors.blue
  },
  sectionRight: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.line,
    marginStart: 5,
    marginEnd: 10
  },
  SectionItemContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 15,
    marginBottom: 15,
    marginStart: 5,
    marginEnd: 5,
  },
  dotView: {
    width: 15,
    height: 15,
    borderRadius: 7.5,
    marginLeft: 10,
    marginRight: 10
  },
  flex: { flex: 1 },
  eventTitle: {
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2.2),
    color: Colors.black,
    paddingBottom: 5,
  },
  name: {
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2.2),
    color: Colors.blue,
    paddingBottom: 5,
  },
  location: {
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2.2),
    color: Colors.textColorLight,
    paddingBottom: 5,
  },
  createdBy: {
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2.2),
    color: Colors.textColorLight,
    paddingBottom: 5,
  },
  time: {
    fontFamily: "AktivGrotesk-Regular",
    // fontSize: 12,
    fontSize: RFPercentage(2.2),
    color: Colors.textColorDark,
    paddingBottom: 5,
  }
});

function mapStateToProps(state) {
  return {
    isLoadingGetList: state.Event.isLoadingGetList,
    getList: state.Event.getList,
    getListError: state.Event.getListError,
    isLoadingActionEvent: state.Event.isLoadingActionEvent,
    actionEvent: state.Event.actionEvent,
    errorActionEvents: state.Event.errorActionEvents,
    isCompletedEvent: state.Event.isCompletedEvent,
    isLoadingAllEvent: state.Event.isLoadingAllEvent,
    allEventList: state.Event.allEventList,
    errorAllEvents: state.Event.errorAllEvents,
    addNewEvent: state.Event.addNewEvent,
    isLoadingAddNewEvent: state.Event.isLoadingAddNewEvent,
  };
}
export default connect(mapStateToProps, null)(EventListComponent);