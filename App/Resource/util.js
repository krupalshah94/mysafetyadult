import {showMessage, hideMessage} from 'react-native-flash-message';
import colors from './Color';
import { RFPercentage } from 'react-native-responsive-fontsize';
export const emailValidate = (text) =>  {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  };

  export function showErrorMessage(message) {
    // Snackbar.show({
    //   title: message,
    //   duration: Snackbar.LENGTH_SHORT,
    //   color: colors.white,
    // });
    showMessage({
      message: message,
      animationDuration: 400,
      duration: 1500,
      type: 'default',
      autoHide: true,
      backgroundColor: colors.white,
      titleStyle: {
        color: colors.colorBackground,
        // fontFamily: fonts.Dosis_Regular,
        fontSize: RFPercentage(2),
      },
    });
  }
  