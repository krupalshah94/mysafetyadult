import english from "./english";
import strings from "./index";
export function getLangValue(value, language) {
  if (language === strings.LAG_ENG) {
    return english[value];
  }
}
// export function getErrorValue(value, language) {
//   if (language === strings.LAG_ENG) {
//     return englishErrors[value];
//   } else {
//     return japaneseErrors[value];
//   }
// }
