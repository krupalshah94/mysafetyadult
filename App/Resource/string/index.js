const string = {
  LAG_ENG: 'en',
  tabHome: "tabHome",
  tabConnections: "tabConnections",
  tabEvents: "tabEvents",
  tabBadge: "tabBadge",
  tabSetting: "tabSetting",

  //error massage
  emptyEmail: "emptyEmail",
  invalidEmail: "invalidEmail",
  emptyPassword: "emptyPassword",
  invalidPassword: "invalidPassword",

  //login screen name
  login: "login",
  signUp: "signUp",
  forgotPassword: "forgotPassword",
  update: 'update',
  submit: 'submit',
  email: 'email',
  changePassword: 'changePassword',
  enterEmail: 'enterEmail',
  password: 'password',  
  newPassword: 'newPassword',
  confirmPassword: 'confirmPassword',
  verifyOtp: 'verifyOtp',
  verify: 'verify',
  resendOtp: 'resendOtp',
  enterOtp: 'enterOtp',
  emptyOtp: 'emptyOtp',
  setNewPassword:'setNewPassword',
  //signUp massage

  errImage: "errImage",
  errFName: "errFName",
  errLName: "errLName",
  errPhone: "errPhone",
  errEmail: "errEmail",
  errEmail: "errEmail",
  errCity: "errCity",
  errStateName: "errStateName",
  errPassword: "errPassword",
  errPasswordLen: "errPasswordLen",
  emptyCPassword: "emptyCPassword",
  errCPassword: "errCPassword",
  errCPasswordMatch: "errCPasswordMatch",
  errTerms: "errTerms",

 // connection 
 badgeNumber: 'badgeNumber',
 sendYouRequest: 'sendYouRequest',
// add event
addEvent: 'addEvent',
eventTitle: 'eventTitle',
eventTime: 'eventTime',
eventStartDate: 'eventStartDate',
eventEndDate: 'eventEndDate',
eventStartTime: 'eventStartTime',
eventEndTime: 'eventEndTime',
location: 'location',
eventLocation: 'eventLocation',
invite: 'invite',
addInvite: 'addInvite',
notes: 'notes',
save: 'save',
inviteYourConnections: 'inviteYourConnections',
close: 'close',
chooseSafety: 'chooseSafety',
chooseWarning: 'chooseWarning',
selectSafety: 'selectSafety',
selectWarning: 'selectWarning',
next: 'next',
submit: 'submit',
chooseImage: 'chooseImage',

//tabs
myConnection: 'myConnection',
viewAll: 'viewAll',
upcomingEvents: 'upcomingEvents',
home: 'home',
events: 'events',
badge: 'badge',
settings: 'settings',
subscription: 'subscription',
manageCards: 'manageCards',
changePassword: 'changePassword',
partners: 'partners',
helpAndFeedback: 'helpAndFeedback',
aboutUs: 'aboutUs',
contactUs: 'contactUs',
FAQs: 'FAQs',
privacyPolicy: 'privacyPolicy',

 //events
 'completedEvent':'completedEvent',
 enterEventNotes: 'enterEventNotes',
 enterEventTitle: 'enterEventTitle',
 enterEventLocation: 'enterEventLocation',
 enterEventStartDate: 'enterEventStartDate',
 enterEventEndDate: 'enterEventEndDate',
 enterEventStartTime: 'enterEventStartTime',
 enterEventEndTime: 'enterEventEndTime',
 enterInviteUser: 'enterInviteUser',
 enterSelectedImage: 'enterSelectedImage',
 //notification
 'notification': 'notification',

  //connection
  'connection': 'connection',
  confirm: 'confirm',
  delete: 'delete',
  accept:'accept',

  // edit profile
  enterPhoneNumber: 'enterPhoneNumber',
  countryCode: 'countryCode',
  phoneNumber: 'phoneNumber',


  noInternet: "noInternet",

};
export default string;
