const english = {
    tabHome: "Home",
    tabConnections: "Connections",
    tabEvents: "Events",
    tabBadge: "Badge",
    tabSetting: "Setting",
  
    //error massage
    emptyEmail: "Please enter Email",
    invalidEmail: "Please enter valid Email",
    emptyPassword: "Please enter Password",
    invalidPassword: "Please use at least 5 character",
  
    //login screen name
    login: "LOGIN",
    signUp: "SIGN UP",
    forgotPassword: "FORGOT PASSWORD",
    update: 'UPDATE',
    submit: 'SUBMIT',
    email: 'Enter email id',
    changePassword: 'CHANGE PASSWORD',
    enterEmail: 'Enter email',
    password: 'Password',
    newPassword: 'New Password',
    confirmPassword: 'Confirm Password',
    verifyOtp: 'VERIFY OTP',
    verify: 'VERIFY',
    resendOtp: 'RESEND OTP',
    enterOtp: 'Enter otp',
    emptyOtp: 'Please enter otp',
    setNewPassword:'SET NEW PASSWORD',
 
    //signUp massage
  
    errImage: "Please select Profile Picture",
    errFName: "Please enter First Name",
    errLName: "Please enter Last Name",
    errPhone: "Please enter Phone Number",
    errEmail: "Please enter Email",
    errEmail: "Please enter valid Email",
    errCity: "Please enter City",
    errStateName: "Please enter State",
    errPassword: "Please enter Password",
    errPasswordLen: "Please use at least 5 character",
    emptyCPassword: "Please enter Confirm Password",
    errCPassword: "Please use at least 5 character",
    errCPasswordMatch: "Password does not matched",
    errTerms: "Please select Terms & conditions",
  
    //signUp namae

    //connection 
    badgeNumber: 'Badge Number',
    sendYouRequest: 'Send you a request',

    // add event
    addEvent: 'ADD EVENT',
    eventTitle: 'EVENT TITLE',
    eventTime: 'EVENT TIME',
    eventStartDate: 'Event Start Date',
    eventEndDate: 'Event End Date',
    eventStartTime: 'Event Start Time',
    eventEndTime: 'Event End Time',
    location: 'LOCATION',
    eventLocation: 'EVENT LOCATION',
    invite: 'INVITE',
    addInvite: 'Add Invite',
    notes: 'NOTES',
    save: 'SAVE',
    inviteYourConnections: 'Invite Your Connections',
    close: 'CLOSE',
    chooseSafety: 'Choose your safety image',
    chooseWarning: 'Choose your warning image',
    selectSafety: 'Select safety image',
    selectWarning: 'Select warning image',
    next: 'NEXT',
    chooseImage: 'Choose Image',

    //tabs
    myConnection: 'MY CONNECTIONS',
    viewAll: 'VIEW ALL',
    upcomingEvents: 'UPCOMING EVENTS',
    home: 'HOME',
    events: 'EVENTS',
    badge: 'BADGE',
    settings: 'SETTINGS',
    subscription: 'Subscription',
    manageCards: 'Manage Cards',
    changePassword: 'Change Password',
    partners: 'Partners',
    helpAndFeedback: 'Help and Feedback',
    aboutUs: 'About Us',
    contactUs: 'Contact Us',
    FAQs: 'FAQs',
    privacyPolicy: 'Terms & Conditions',

    //events
    'completedEvent':'ALL EVENTS',
    enterEventNotes: 'Please enter notes',
    enterEventTitle: 'Please enter event title',
    enterEventLocation: 'Please enter event location',
    enterEventStartDate: 'Please select event start date',
    enterEventEndDate: 'Please select event end date',
    enterEventStartTime: 'Please select event start time',
    enterEventEndTime: 'Please select event end time',
    enterInviteUser: 'Please invite user',
    enterSelectedImage: 'Please select image',
     //notification
    'notification': 'NOTIFICATION',

    //connection
    'connection': 'CONNECTION',
    confirm: 'CONFIRM',
    delete: 'DELETE',
    accept:'ACCEPT',

      // edit profile
  enterPhoneNumber: 'Please enter phone number',
  countryCode: 'Country Code',
  phoneNumber: 'Phone Number',
  //
  noInternet: "No Internet!",

};
  export default english;
  