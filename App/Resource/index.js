const index = {
  tab_home: require("../../assets/Icon/home.png"),
  tab_connection: require("../../assets/Icon/connections.png"),
  tab_event: require("../../assets/Icon/event.png"),
  tab_badge: require("../../assets/Icon/badge.png"),
  tab_setting: require("../../assets/Icon/setting.png"),
  user: require("../../assets/Image/user.png"),
  bg_login: require("../../assets/Image/bg_login.png"),
  bg_cart: require("../../assets/Image/bg_cart.png"),
  badge_white:require('../../assets/Icon/badge_white.png'),
  checked:require('../../assets/Icon/checked.png'),
  alarm:require('../../assets/Icon/alarm_blue.png'),
  tick:require('../../assets/Icon/Tick.png'),
  tab_blue_back:require('../../assets/Image/tab_transperent.png'),
  back_icon: require('../../assets/Icon/left_arrow.png'),
  user_1: require('../../assets/Image/user_1.png'),
  user_2: require('../../assets/Image/user_2.png'),
  plus_icon: require('../../assets/Icon/plus_icon.png'),
  inviteUser: require('../../assets/Image/inviteUser.png'),
  checkedBlack: require('../../assets/Icon/checked_black.png'),
  unCheckedBlack: require('../../assets/Icon/unchecked_black.png'),
  safetyImage: require('../../assets/Image/safetyImage.png'),
  selectedChecked: require('../../assets/Image/selected.png'),
  selectedRed: require('../../assets/Image/selectedRed.png'),
  selectedBlue: require('../../assets/Image/whiteTransparent.png'),
  circle: require('../../assets/Icon/circle.png'),
  circleChecked: require('../../assets/Icon/circleChecked.png'),
  calender: require('../../assets/Icon/calendar.png'),
  clock: require('../../assets/Icon/clock.png'),
  user1: require('../../assets/Image/1.png'),
  user2: require('../../assets/Image/2.png'),
  user3: require('../../assets/Image/3.png'),
  user4: require('../../assets/Image/4.png'),
  user5: require('../../assets/Image/5.png'),
  user6: require('../../assets/Image/6.png'),
  user7: require('../../assets/Image/7.png'),
  user8: require('../../assets/Image/8.png'),
  user9: require('../../assets/Image/9.png'),
  noEvents: require('../../assets/Image/noEvent.png'),
  noInternet: require('../../assets/Image/no_internet.png'),
  copy: require('../../assets/Image/copy.png'),

};
export default index;