import { StyleSheet } from "react-native";
const EventScreenStyle = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#DFE2F1" }
});
export default EventScreenStyle;
