import { StyleSheet } from "react-native";
import Colors from "../Color/index";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const BadgeScreenStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#DFE2F1"
  },
  card: {
    backgroundColor: Colors.white,
    borderRadius: 4,
    height: hp(35),
    margin: "5%",
    justifyContent: "center"
  },
  checked: {
    width: 100,
    height: 100,
    margin: "5%",
    marginTop: "10%"
  },
  text_success: {
    margin: "5%",
    marginBottom: "10%",
    fontFamily: "AktivGrotesk-Regular",
    fontSize: 15,
    color: Colors.black
  },
  view_button: {
    top: hp(35),
    position: 'absolute',
    alignSelf:'center',
    width: wp(80),
  }
});

export default BadgeScreenStyle;
