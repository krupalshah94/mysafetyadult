import { StyleSheet } from "react-native";
import Colors from "../Color";
import { heightPercentageToDP } from "react-native-responsive-screen";

const SettingScreenStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#DFE2F1",
    // marginBottom: heightPercentageToDP(5)
  },
  card: {
    backgroundColor: Colors.white,
    borderRadius: 4,
    margin: "5%",
    alignContent: "center"
  },
  checked: {
    width: 100,
    height: 100,
    margin: "5%",
    marginTop: "10%"
  },
  text_success: {
    margin: "5%",
    marginBottom: "10%",
    fontFamily: "AktivGrotesk-Regular",
    fontSize: 15,
    color: Colors.black
  },
  view_button: {
    marginStart: 20,
    marginEnd: 20,
    marginBottom: 20,
  },
  text_reg:{ color: Colors.black, fontSize: 16, marginStart: 10,fontFamily:'AktivGrotesk-Bold' }
});

export default SettingScreenStyle;
