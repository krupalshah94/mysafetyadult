import colors from "../Resource/Color";
import font from "../Resource/fontFamily";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { StyleSheet } from "react-native";
import { RFPercentage } from 'react-native-responsive-fontsize';
const styles = {
  //sign up style
  text_reg: {
    color: colors.black,
    fontSize: 16,
    marginStart: 10,
    fontFamily: "AktivGrotesk-Bold"
  },
  topView: { position: "relative", flex: 1, width: "100%", marginTop: "20%" },
  cartView: { position: "relative", width: "100%" },
  signDropDownView: {
    flexDirection: "row",
    // alignItems: "center",
    marginStart: 10,
    marginEnd: 10,
    flex: 1
  },
  checkBoxView: {
    backgroundColor: colors.transparent,
    borderColor: colors.transparent,
    borderWidth: 0,
    marginStart: "5%",
    marginEnd: "5%"
  },
  imageStyle: {
    height: 150,
    width: 150,
    alignSelf: "center",
    borderRadius: 75
  },
  signContainerStyle: { marginStart: 10, marginEnd: 10 },
  roundImage: { position: "absolute", top: -65, width: "100%" },
  upload: { alignSelf: "center", marginTop: 10 },
  uploadText: { color: "#6570DE", 
  // fontFamily: font.Regular, 
  fontSize: 16 },
  //login style
  container: {
    flex: 1,
  },

  conformPasswordView: { marginStart: 10, marginEnd: 10, marginBottom: 20 },
  ImageBackground: { height: "100%", width: "100%" },
  card: {
    backgroundColor: colors.white,
    borderRadius: 20,
    marginTop: hp(1),
    marginStart: wp(10),
    marginEnd: wp(10)
  },
  image_badge: {
    marginTop: "10%",
    // marginBottom: "5%",
    width: wp(25),
    height: wp(25),
    alignSelf: "center",
  },
  text_login: {
    color: colors.blue,
    marginTop: "10%",
    marginBottom: "5%",
    textAlign: "center",
    fontSize: RFPercentage(3),
    fontFamily: "AktivGrotesk-Regular"
  },
  resend_text: {
    color: colors.blue,
    marginTop: "10%",
    marginBottom: "5%",
    textAlign: "center",
    fontSize: RFPercentage(2),
    fontFamily: "AktivGrotesk-Regular"
  },
  text_forget_heading: {
    fontWeight: '600',
    color: colors.blue,
    marginTop: "10%",
    marginBottom: "5%",
    textAlign: "center",
    fontSize: RFPercentage(3),
    fontFamily: "AktivGrotesk-Regular"
 },
  text_forgot: {
    marginTop: 20,
    textDecorationLine: "underline",
    color: colors.forgotText,
    marginTop: 20,
    marginBottom: 20,
    fontSize: 16,
    textAlign: "center",
    fontFamily: "AktivGrotesk-Regular"
  },
  text_login_red: {
    marginTop: 20,
    color: colors.login_red,
    marginTop: 20,
    marginBottom: 20,
    fontSize: 16,
    textAlign: "center",
    fontFamily: "AktivGrotesk-Regular",
    borderWidth: 0,
    fontWeight: '600',
  },
  button_signup: {
    marginTop: 20,
    marginStart: "15%",
    marginEnd: "15%"
  },
  containerStyleTextField: { marginStart: wp(3), marginEnd: wp(3), marginBottom:0 },
  inputContainerStyleTextField: {
    borderBottomColor: colors.TextField_bg,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  loginButton: { backgroundColor: colors.loginButton, borderRadius: 20, },
  loginButtonContainer: {
    margin: 10
  },
  signUpButton: { backgroundColor: colors.signUpButton, borderRadius: 20 }
};
export default styles;
