const STAGING_API = 'http://13.235.225.118/dev/laravel/mysafety-adult/public/api/v1/';
const LOCAL_URL = 'http://192.168.1.111/mysafety-adult/public/api/v1/';
const SERVER_API = 'http://devsecure.mysafetynet.info/api/v1/';
const BASE_URL = SERVER_API;
const ApiUrl = {
    LOGIN: BASE_URL + 'login',
    FORGOT_PASSWORD: BASE_URL + 'forgot-password',
    CONFIRM_OTP: BASE_URL + 'confirmOTP',
    SET_NEW_PASSWORD: BASE_URL + 'reset-password',
    USER_PROFILE: BASE_URL + 'profile',
    UPDATE_PROFILE: BASE_URL + 'profile/update',
    CHANGE_PASSWORD: BASE_URL + 'change-password',
    EVENT_INCOMING: BASE_URL + 'events/incoming?',
    SEND_CONNECTION_REQUEST: BASE_URL + 'send-request',
    GET_CONNECTION_LIST: BASE_URL + 'get-connections?',
    CONNECTION_ACTION: BASE_URL + 'action-on-request',
    EVENT_DETAILS: BASE_URL + 'event',
    MY_INCOMING_EVENT: BASE_URL + 'events/invites?',
    GET_IMAGES: BASE_URL + 'getImageList',
    GET_ALL_EVENTS: BASE_URL + 'events?',
    ACTION_EVENTS: BASE_URL + 'event/',
    COMPLETE_EVENTS: BASE_URL + 'event/',
    CALENDER_EVENTS: BASE_URL + 'events/calendar',
    ADD_NEW_EVENT: BASE_URL + 'event/add',
    ADD_IMAGES_TO_EVENTS: BASE_URL + 'event/',
    GET_CONNECTED_USER_PROFILE: BASE_URL + 'profile/',
    NOTIFICATION: BASE_URL + 'notifications?',
    READ_NOTIFICATION: BASE_URL + 'notifications/read',
};

export default ApiUrl;