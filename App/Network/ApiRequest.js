export const postRequest = async (apiUrl, body) => {
  console.log(apiUrl, body)
  try {
    const response = await fetch(apiUrl, {
      credentials: "include",
      method: "POST",
      headers: {
        // Authorization: "Basic " + getBasic(),
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    });
    const result = await response.text();
    console.log(result)
    const responseJson = JSON.parse(result);
    return responseJson;
  } catch (error) {
    console.log(error)
    
    throw error;
  }
};

export const postRequestWithHeader = async (apiUrl,token, body) => {
  console.log(apiUrl, body)
  try {
    const response = await fetch(apiUrl, {
      credentials: "include",
      method: "POST",
      headers: {
        Authorization: "Bearer" + token,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    });
    const result = await response.text();
    console.log(result)
    const responseJson = JSON.parse(result);
    return responseJson;
  } catch (error) {
    console.log(error)
    
    throw error;
  }
};



export const putRequest = async (apiUrl, body) => {
  console.log(apiUrl)
  try {
    const response = await fetch(apiUrl, {
      credentials: "include",
      method: "PUT",
      headers: {
        // Authorization: "Basic " + getBasic(),
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    });
    const result = await response.text();
    console.log(result)
    const responseJson = JSON.parse(result);
    return responseJson;
  } catch (error) {
    console.log(error)
    
    throw error;
  }
};

export const putRequestWithHeader = async (apiUrl,token, body) => {
  console.log(apiUrl, body)
  try {
    const response = await fetch(apiUrl, {
      credentials: "include",
      method: "PUT",
      headers: {
        Authorization: "Bearer" + token,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    });
    const result = await response.text();
    console.log(result)
    const responseJson = JSON.parse(result);
    return responseJson;
  } catch (error) {
    console.log(error)
    
    throw error;
  }
};

export const getRequest = async apiUrl => {
  try {
    const response = await fetch(apiUrl, {
      credentials: "include",
      method: "GET",
      headers: {
        // Authorization: "Basic " + getBasic(),
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    });
    const result = await response.text();
    const responseJson = JSON.parse(result);
    return responseJson;
  } catch (error) {
    throw error;
  }
};


export const getRequestWithHeader = async (apiUrl,token) => {
  console.log(apiUrl, token)
  try {
    const response = await fetch(apiUrl, {
      credentials: "include",
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    });
    const result = await response.text();
    const responseJson = JSON.parse(result);
    console.log('res', responseJson)
    return responseJson;
  } catch (error) {
    throw error;
  }
};

export const postImageRequest = async (apiUrl,token, field, imageUrl, body) => {
 console.log(apiUrl,token, field, imageUrl, body)
  try {
    const response = await fetch(apiUrl, {
      credentials: "include",
      method: "POST",
      headers: {
        Authorization: "Bearer" + token,
        "Content-Type": "multipart/form-data"
      },
      body: createFormData(field, imageUrl, body)
    });
    console.log('responseJson', response)
    const responseJson = await response.json();
   
    return responseJson;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const createFormData = (field, photo, body) => {
  const data = new FormData();

  data.append(field, {
    name: "image.jpg",
    type: "image/*",
    uri: photo
  });

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};
