import ApiUrl from '../Network/ApiUrl';
import { postRequest, putRequest, getRequest, getRequestWithHeader, postImageRequest, postRequestWithHeader, putRequestWithHeader } from '../Network/ApiRequest'; 
export const LoginService = async (userData) => {
    try {
        const res = await postRequest(ApiUrl.LOGIN, userData);
        return res;
    } catch(error) {
        throw error;
    }
}

export const ForgetPasswordService = async (userData) => {
    try {
        const res = await postRequest(ApiUrl.FORGOT_PASSWORD, userData);
        return res;
    } catch(error) {
        throw error;
    }
}

export const ConfirmOtpService = async (userData) => {
    try {
        const res = await postRequest(ApiUrl.CONFIRM_OTP, userData);
        return res;
    } catch(error) {
        throw error;
    }
}

export const SetNewPasswordService = async (userData) => {
    try {
        const res = await postRequest(ApiUrl.SET_NEW_PASSWORD, userData);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getUserProfile = async (token) => {
    try {
        const res = await getRequestWithHeader(ApiUrl.USER_PROFILE, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const updateUserProfile = async (token, field, imageUrl, body) => {
    try {
        const res = await postImageRequest(ApiUrl.UPDATE_PROFILE, token, field, imageUrl, body);
        return res;
    } catch(error) {
        throw error;
    }
}

export const changePassword = async (token, body) => {
    try {
        const res = await postRequestWithHeader(ApiUrl.CHANGE_PASSWORD, token, body);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getIncomingEvents = async (token, page, perPage) => {
    try {
        const res = await getRequestWithHeader(`${ApiUrl.EVENT_INCOMING}page=${page}&per_page=${perPage}`, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const myIncomingEvents = async (token, page, perPage) => {
    try {
        const res = await getRequestWithHeader(`${ApiUrl.MY_INCOMING_EVENT}page=${page}&per_page=${perPage}`, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const allEvents = async (token, page, perPage) => {
    try {
        const res = await getRequestWithHeader(`${ApiUrl.GET_ALL_EVENTS}page=${page}&per_page=${perPage}`, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const allEventsWithDates = async (token, page, perPage, selected_date) => {
    try {
        const res = await getRequestWithHeader(`${ApiUrl.GET_ALL_EVENTS}page=${page}&per_page=${perPage}&selected_date=${selected_date}`, token);
        return res;
    } catch(error) {
        throw error;
    }
}


export const allEventsWithUser = async (token, page, perPage, userId) => {
    try {
        const res = await getRequestWithHeader(`${ApiUrl.GET_ALL_EVENTS}page=${page}&per_page=${perPage}&connected_user_id=${userId}`, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getEventView = async (token, eventId) => {
    try {
        const res = await getRequestWithHeader(`${ApiUrl.EVENT_DETAILS}/${eventId}/view`, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const actionRequestEvents = async (token, eventId, status) => {
    try {
        const res = await putRequestWithHeader(`${ApiUrl.ACTION_EVENTS}${eventId}/accept-invite?approval_status=${status}`, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const sendConnection = async (token, body) => {
    try {
        const res = await postRequestWithHeader(ApiUrl.SEND_CONNECTION_REQUEST, token, body);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getConnectionList = async (token, page, perPage, status) => {
    try {
        const res = await getRequestWithHeader(`${ApiUrl.GET_CONNECTION_LIST}page=${page}&per_page=${perPage}&status=${status}`, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const connectionActions = async (token, body) => {
    try {
        const res = await postRequestWithHeader(ApiUrl.CONNECTION_ACTION, token, body);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getImageList = async (token) => {
    try {
        const res = await getRequestWithHeader(`${ApiUrl.GET_IMAGES}`, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const completeEvents = async (token, eventId, body) => {
    try {
        const res = await putRequestWithHeader(`${ApiUrl.COMPLETE_EVENTS}${eventId}/mark-complete`, token, body);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getCalenderDates = async (token, month, year) => {
    try {
        const res = await getRequestWithHeader(`${ApiUrl.CALENDER_EVENTS}?month=${month}&year=${year}`, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const addNewEvent = async (token, body) => {
    try {
        const res = await postRequestWithHeader(ApiUrl.ADD_NEW_EVENT, token, body);
        return res;
    } catch(error) {
        throw error;
    }
}

export const addImagesToEvents = async (token, eventId, body) => {
    try {
        const res = await putRequestWithHeader(`${ApiUrl.COMPLETE_EVENTS}${eventId}/set-image`, token, body);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getConnectedUserProfile = async (token, userId) => {
    try {
        const res = await getRequestWithHeader(ApiUrl.GET_CONNECTED_USER_PROFILE + userId, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const getNotification = async (token, page) => {
    try {
        const res = await getRequestWithHeader(ApiUrl.NOTIFICATION + 'page=' + page, token);
        return res;
    } catch(error) {
        throw error;
    }
}

export const readNotification = async (token, body) => {
    try {
        const res = await putRequestWithHeader(ApiUrl.READ_NOTIFICATION, token, body);
        return res;
    } catch(error) {
        throw error;
    }
}
