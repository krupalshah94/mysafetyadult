import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  ScrollView
} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Text, Button, Image } from "react-native-elements";
import LoginScreenStyle from "../../CustomStyle/index";
import { TextField } from "react-native-material-textfield";
import Icons from "../../Resource/index";
import colors from "../../Resource/Color";
import font from "../../Resource/fontFamily";
import string from "../../Resource/string";
import { emailValidate } from "../../Resource/util";
import { getLangValue } from "../../Resource/string/language";
import { RFPercentage } from 'react-native-responsive-fontsize';
import HeaderWithBack from "../../Component/HeaderWithBack";
import { connect } from "react-redux";
import {setNewPasswordSuccess, setNewPasswordRequest, setNewPasswordFail} from '../../Store/Actions/AuthAction';
import { SetNewPasswordService } from '../../Network/Service';
import CustomToast from "../../Component/CustomToast";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import NetInfo from "@react-native-community/netinfo";
class SetNewPassword extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
        loading: false,
        password: '',
        errPassword: '',
        confirmPassword: '',
        errConfirmPassword: '',
        isConnected: false,
    };
  }

  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getData();
  }

  componentWillUnmount() {
    this._subscription && this._subscription();
    this.setState({ password: '', confirmPassword: ''})
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isNewPasswordLoading !== this.props.isNewPasswordLoading) {
      this.setState({ loading: this.props.isNewPasswordLoading});
    }
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  doRediect(screen) {
    this.props.navigation.navigate(screen);
  }

  getData(){
    setTimeout(() => {
      AsyncStorage.getItem("notification")
      .then(data => {
        const myData = JSON.parse(data);
        if (myData != null) {
            if (myData.extra==="notification") {
              this.doRediect("Notification");
              AsyncStorage.removeItem("notification");
            } else {
              
            }
        } else {
        }
      })
      .done(); 
    }, 500);
  }
 
  doSetNewPassword =  async () => {
    const { password, confirmPassword } = this.state;
    if (password.trim() === "") {
        this.passRef.focus();
        this.setState({ errPassword: getLangValue(string.emptyPassword, 'en')});
        return;
      } 
      if (password.length <= 4) {
        this.passRef.focus();
        this.setState({ errPassword: getLangValue(string.invalidPassword, 'en')});
        return;
      }  
      if (confirmPassword.trim() === "") {
        this.confPassRef.focus();
        this.setState({ errConfirmPassword: getLangValue(string.emptyCPassword, 'en')});
        return;
      } 
      if (confirmPassword.length <= 4) {
        this.confPassRef.focus();
        this.setState({ errConfirmPassword: getLangValue(string.errCPassword, 'en')});
        return;
      } 
      if (password.trim() !== confirmPassword.trim()) {
        this.confPassRef.focus();
        this.setState({ errConfirmPassword: getLangValue(string.errCPasswordMatch, 'en')});
        return;
      } 
      this.setState({ errPassword: "", errConfirmPassword: ""});
      if (!this.state.isConnected) {
        this.refs.internetConnection.ShowToastFunction('No Internet Connected');
       return;
     }
      const userData = {
        email: this.props.route.params.email,
        otp: this.props.route.params.otp,
        password: this.state.password,
        confirm_password: this.state.confirmPassword
       }
       try {
         this.props.dispatch(setNewPasswordRequest());
         const res = await SetNewPasswordService(userData);
         const code = res.code;
         switch (code) {
           case 200:
             this.props.dispatch(setNewPasswordSuccess(res));
             this.refs.success.ShowToastFunction(res.message);
             setTimeout(() => {
               this.props.navigation.navigate('Login');
             }, 2000);
             break;
           case 400:
             this.props.dispatch(setNewPasswordFail(res));
             this.refs.defaultToastTop.ShowToastFunction(res.message);
             break;
           
           default:
             break;
         }
       } catch (error) {
         this.props.dispatch(setNewPasswordFail(error));
       }
  }
  render() {
    return (
      <SafeAreaView style={LoginScreenStyle.container}>
       {/* <HeaderWithBack notify={false} navigation={this.props.navigation}/> */}
        <ImageBackground
          source={Icons.bg_login}
          style={LoginScreenStyle.ImageBackground}
        >
          <ScrollView>
            <View style={LoginScreenStyle.container}>
            <View style={{alignItems: 'center', marginTop: "10%",
    marginBottom: "5%"}}>
              <Image
                source={Icons.badge_white}
                style={LoginScreenStyle.image_badge}
              />
              </View>
              <View style={LoginScreenStyle.card}>
                <Text style={LoginScreenStyle.text_forget_heading}>{getLangValue(string.setNewPassword, 'en')}</Text>
                <TextField
                  ref={ref => {
                    this.passRef = ref;
                  }}
                  underlineColorAndroid={colors.transparent}
                  value={this.state.password}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ password: text });
                  }}
                  returnKeyType="done"
                  label={getLangValue(string.password, 'en')}
                  error={this.state.errPassword}
                  fontSize={16}
                  placeholderTextColor={colors.inputTextColor}
                  textColor={colors.inputTextColor}
                  tintColor={colors.inputTextColor}
                  secureTextEntry={true}
                  containerStyle={LoginScreenStyle.containerStyleTextField}
                  inputContainerStyle={
                    LoginScreenStyle.inputContainerStyleTextField
                  }
                  labelTextStyle={{ fontFamily: font.Regular }}
                  titleTextStyle={{ fontFamily: font.Regular }}
                  affixTextStyle={{ fontFamily: font.Regular }}
                  onBlur={() => this.setState({ errPassword: "" })}
                />
                 <TextField
                  ref={ref => {
                    this.confPassRef = ref;
                  }}
                  underlineColorAndroid={colors.transparent}
                  value={this.state.password}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ confirmPassword: text });
                  }}
                  returnKeyType="done"
                  label={getLangValue(string.confirmPassword, 'en')}
                  error={this.state.errConfirmPassword}
                  fontSize={16}
                  placeholderTextColor={colors.inputTextColor}
                  textColor={colors.inputTextColor}
                  tintColor={colors.inputTextColor}
                  secureTextEntry={true}
                  containerStyle={LoginScreenStyle.containerStyleTextField}
                  inputContainerStyle={
                    LoginScreenStyle.inputContainerStyleTextField
                  }
                  labelTextStyle={{ fontFamily: font.Regular }}
                  titleTextStyle={{ fontFamily: font.Regular }}
                  affixTextStyle={{ fontFamily: font.Regular }}
                  onBlur={() => this.setState({ errConfirmPassword: "" })}
                />
                <Button
                  loading={this.state.loading}
                  title={getLangValue(string.submit, 'en')}
                  buttonStyle={LoginScreenStyle.loginButton}
                  containerStyle={LoginScreenStyle.loginButtonContainer}
                  onPress={() => this.doSetNewPassword()}
                  // titleStyle={{ fontFamily: font.Regular, fontSize: RFPercentage(2)}}
                  textStyle={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Bold",
                }}
                />
                <View>
              </View>
              </View>
             </View>
          </ScrollView>
        </ImageBackground>
        <CustomToast ref = "defaultToastTop" backgroundColor={colors.white} textColor={colors.red} position = "bottom"/>
        <CustomToast ref = "success" backgroundColor={colors.white} textColor={colors.greenText} position = "top"/>
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    isNewPasswordLoading: state.Auth.isNewPasswordLoading,
    setPasswordDetails: state.Auth.setPasswordDetails,
    setPasswordError: state.Auth.setPasswordError, 
  };
}

export default connect(mapStateToProps,null)(SetNewPassword);
