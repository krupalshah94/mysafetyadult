import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  ScrollView
} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Text, Button, Image } from "react-native-elements";
import LoginScreenStyle from "../../CustomStyle/index";
import { TextField } from "react-native-material-textfield";
import Icons from "../../Resource/index";
import colors from "../../Resource/Color";
import font from "../../Resource/fontFamily";
import string from "../../Resource/string";
import { emailValidate } from "../../Resource/util";
import { getLangValue } from "../../Resource/string/language";
import { RFPercentage } from 'react-native-responsive-fontsize';
import { forgetPasswordRequest, forgetPasswordSuccess, forgetPasswordFail } from '../../Store/Actions/AuthAction';
import {ForgetPasswordService} from '../../Network/Service';
import { connect } from "react-redux";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import CustomToast from "../../Component/CustomToast";
import NetInfo from "@react-native-community/netinfo";

class ForgetPasswordScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      email: "",
      errEmail: "",
      isConnected: false,
    };
  }
  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isForgetPasswordLoading !== this.props.isForgetPasswordLoading) {
      this.setState({ loading: this.props.isForgetPasswordLoading});
    }
    this.getData();
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  componentWillUnmount() {
    this._subscription && this._subscription();
    this.setState({ email: ''})
  }

  doRediect(screen) {
    this.props.navigation.navigate(screen);
  }

  getData() {
    setTimeout(() => {
      AsyncStorage.getItem("notification")
        .then(data => {
          const myData = JSON.parse(data);

          if (myData != null) {
            if (myData.extra === "notification") {
              this.doRediect("Notification");
              AsyncStorage.removeItem("notification");
            } else {

            }
          } else {
          }
        })
        .done();
    }, 500);
  }
  doHandleForgerPassword = async () =>  {
    const { email } = this.state;
    if (email === "") {
      this.emailRef.focus();
      this.setState({ errEmail: getLangValue(string.emptyEmail, 'en') });
      return;
    }
    if (!emailValidate(email)) {
      this.emailRef.focus();
      this.setState({ errEmail: getLangValue(string.invalidEmail, 'en') });
      return;
    }
    this.setState({ errEmail: "" });
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
   }
    const userData = {
      email: this.state.email
    };
    try { 
      this.props.dispatch(forgetPasswordRequest());
      const res =  await ForgetPasswordService(userData);
      const code = res.code;
      switch (code) {
        case 200:
            this.refs.success.ShowToastFunction(res.message);
            this.props.dispatch(forgetPasswordSuccess(res));
            setTimeout(() => {
              this.props.navigation.navigate('VerifyOtp', {email: this.state.email});
            }, 2000);
            break;
        case 400:
          this.props.dispatch(forgetPasswordFail(res));
          this.refs.defaultToastTop.ShowToastFunction(res.message);
        break;
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(forgetPasswordFail(error));
    }
  }
  render() {
    return (
      <SafeAreaView style={LoginScreenStyle.container}>
        <ImageBackground
          source={Icons.bg_login}
          style={LoginScreenStyle.ImageBackground}
        >
          <ScrollView>
            <View style={LoginScreenStyle.container}>
              <View style={{
                alignItems: 'center', marginTop: "10%",
                marginBottom: "5%"
              }}>
                <Image
                  source={Icons.badge_white}
                  style={LoginScreenStyle.image_badge}
                />
              </View>
              <View style={LoginScreenStyle.card}>
                <Text style={LoginScreenStyle.text_forget_heading}>{getLangValue(string.forgotPassword, 'en')}</Text>
                <TextField
                  ref={ref => {
                    this.emailRef = ref;
                  }}
                  underlineColorAndroid={colors.transparent}
                  value={this.state.email}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ email: text });
                  }}
                  onSubmitEditing={() => {
                    this.passRef.focus();
                  }}
                  returnKeyType="next"
                  label={getLangValue(string.email, 'en')}
                  keyboardType="email-address"
                  error={this.state.errEmail}
                  placeholderTextColor={colors.inputTextColor}
                  textColor={colors.inputTextColor}
                  tintColor={colors.inputTextColor}
                  fontSize={16}
                  containerStyle={LoginScreenStyle.containerStyleTextField}
                  inputContainerStyle={
                    LoginScreenStyle.inputContainerStyleTextField
                  }
                  autoCapitalize="none"
                  onBlur={() => this.setState({ errEmail: "" })}
                  labelTextStyle={{ fontFamily: font.Regular }}
                  titleTextStyle={{ fontFamily: font.Regular }}
                  affixTextStyle={{ fontFamily: font.Regular }}
                />
                <Button
                  loading={this.state.loading}
                  title={getLangValue(string.submit, 'en')}
                  buttonStyle={LoginScreenStyle.loginButton}
                  containerStyle={LoginScreenStyle.loginButtonContainer}
                  onPress={() => this.doHandleForgerPassword()}
                  // titleStyle={{ fontFamily: font.Regular, fontSize: RFPercentage(2)}}
                  textStyle={{
                    textAlign: 'center',
                    fontSize: 16,
                    fontFamily: "AktivGrotesk-Bold",
                  }}
                />
                <View>
                  <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }}>
                    <Text style={LoginScreenStyle.text_login_red}>
                      {getLangValue(string.login, 'en')}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </ScrollView>
        </ImageBackground>
        <CustomToast ref = "defaultToastTop" backgroundColor={colors.white} textColor={colors.red} position = "bottom"/>
        <CustomToast ref = "success" backgroundColor={colors.white} textColor={colors.greenText} position = "top"/>
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    isForgetPasswordLoading: state.Auth.isForgetPasswordLoading,
    forgetPasswordDetails: state.Auth.forgetPasswordDetails,
    forgetPasswordError: state.Auth.forgetPasswordError, 
  };
}

export default connect(mapStateToProps,null)(ForgetPasswordScreen);
