import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  ScrollView, Platform, Alert, TouchableNativeFeedbackBase
} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Text, Button, Image } from "react-native-elements";
import LoginScreenStyle from "../../CustomStyle/index";
import { TextField } from "react-native-material-textfield";
import Icons from "../../Resource/index";
import colors from "../../Resource/Color";
import font from "../../Resource/fontFamily";
import string from "../../Resource/string";
import { emailValidate } from "../../Resource/util";
import { getLangValue } from "../../Resource/string/language";
import CustomToast from '../../Component/CustomToast';
import { connect } from 'react-redux';
import { LoginService } from '../../Network/Service';
import { loginSuccess, loginFail, loginRequest, doSetNotificationData } from "../../Store/Actions/AuthAction";
import BarIndicatorLoader from '../../Component/BarIndicatorLoader';
import * as Constants from '../../Resource/Constants';
import firebase from "react-native-firebase";
import NetInfo from "@react-native-community/netinfo";

class LoginScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      email: "",
      errEmail: "",
      password: "",
      errPassword: "",
      isConnected: false,
    };
    this.fcmToken = '';
  }

  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.checkPermission();
    this.checkUserLogin();
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();

      this.getToken();
    } catch (error) {
    }
  }

  componentWillUnmount() {
    this._subscription && this._subscription();
    this.setState({ email: '', password: ''});
  }
  	
 
  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  doRedirect(screen) {
    this.props.navigation.navigate(screen);
  }

  async getToken() {
    this.fcmToken = await AsyncStorage.getItem(Constants.FIRE_BASE_TOKEN);
    if (!this.fcmToken) {
      this.fcmToken = await firebase.messaging().getToken();
      if (this.fcmToken) {
        await AsyncStorage.setItem(Constants.FIRE_BASE_TOKEN, this.fcmToken);
      }
    }
  }

  async checkUserLogin() {
    const res = await AsyncStorage.getItem(Constants.TOKEN);
    var isLogin = false;
    if (res) {
    isLogin = true;
    } else {
      isLogin = false;
    }
    if (isLogin === true) {
      this.props.navigation.navigate('Tabs', {screen:'Home'});
    }
  }


  componentDidUpdate(prevProps) {
    if (prevProps.isLoginLoading !== this.props.isLoginLoading) {
      this.setState({ loading: this.props.isLoginLoading });
    }
  }

  doLogin = async () => {
    const { email, password } = this.state;
    if (email === "") {
      this.emailRef.focus();
      this.setState({ errEmail: getLangValue(string.emptyEmail, 'en') });
      return;
    }
    if (!emailValidate(email)) {
      this.emailRef.focus();
      this.setState({ errEmail: getLangValue(string.invalidEmail, 'en') });
      return;
    }
    if (password === "") {
      this.passRef.focus();
      this.setState({ errPassword: getLangValue(string.emptyPassword, 'en') });
      return;
    }
    if (password.length <= 4) {
      this.passRef.focus();
      this.setState({ errPassword: getLangValue(string.invalidPassword, 'en') });
      return;
    }
    this.setState({ errEmail: "", errPassword: "" });
    if (!this.state.isConnected) {
       this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    const userData = {
      email: this.state.email,
      password: this.state.password,
      device_type: Platform.OS,
      device_token: this.fcmToken,
      firebase_token: this.fcmToken,
      user_type: 'User',
    }
      try {
        this.props.dispatch(loginRequest());
        const res = await LoginService(userData);
        const code = res.code;
        switch (code) {
          case 200:
            AsyncStorage.setItem(Constants.TOKEN, res.data.user.token);
            AsyncStorage.setItem(Constants.USER_DATA, JSON.stringify(res.data.user));
            this.props.dispatch(loginSuccess(res));
            setTimeout(() => {
              this.props.navigation.navigate('Tabs');
            }, 200);
            break;
          case 400:
            this.props.dispatch(loginFail(res));
            this.refs.defaultToastTop.ShowToastFunction(res.message);
            break;
          default:
            break;
        }
      } catch (error) {
        this.props.dispatch(loginFail(error));
      }
  }
  render() {
    return (
      <SafeAreaView style={LoginScreenStyle.container}>
        <ImageBackground
          source={Icons.bg_login}
          style={LoginScreenStyle.ImageBackground}
        >
          <ScrollView>
            <View style={LoginScreenStyle.container}>
              <View style={{ alignItems: 'center', marginTop: "10%" }}>
                <Image
                  source={Icons.badge_white}
                  style={LoginScreenStyle.image_badge}
                />
              </View>
              <View style={LoginScreenStyle.card}>
                <Text style={LoginScreenStyle.text_login}>{getLangValue(string.login, 'en')}</Text>
                <TextField
                  ref={ref => {
                    this.emailRef = ref;
                  }}
                  underlineColorAndroid={colors.transparent}
                  value={this.state.email}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ email: text });
                  }}
                  onSubmitEditing={() => {
                    this.passRef.focus();
                  }}
                  returnKeyType="next"
                  label={getLangValue(string.enterEmail, 'en')}
                  keyboardType="email-address"
                  error={this.state.errEmail}
                  placeholderTextColor={colors.inputTextColor}
                  textColor={colors.inputTextColor}
                  tintColor={colors.inputTextColor}
                  fontSize={16}
                  containerStyle={LoginScreenStyle.containerStyleTextField}
                  inputContainerStyle={
                    LoginScreenStyle.inputContainerStyleTextField
                  }
                  autoCapitalize="none"
                  onBlur={() => this.setState({ errEmail: "" })}
                  labelTextStyle={{ fontFamily: font.Regular }}
                  titleTextStyle={{ fontFamily: font.Regular }}
                  affixTextStyle={{ fontFamily: font.Regular }}
                />
                <TextField
                  ref={ref => {
                    this.passRef = ref;
                  }}
                  underlineColorAndroid={colors.transparent}
                  value={this.state.password}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ password: text });
                  }}
                  returnKeyType="done"
                  label={getLangValue(string.password, 'en')}
                  error={this.state.errPassword}
                  fontSize={16}
                  placeholderTextColor={colors.inputTextColor}
                  textColor={colors.inputTextColor}
                  tintColor={colors.inputTextColor}
                  secureTextEntry={true}
                  containerStyle={LoginScreenStyle.containerStyleTextField}
                  inputContainerStyle={
                    LoginScreenStyle.inputContainerStyleTextField
                  }
                  labelTextStyle={{ fontFamily: font.Regular }}
                  titleTextStyle={{ fontFamily: font.Regular }}
                  affixTextStyle={{ fontFamily: font.Regular }}
                  onBlur={() => this.setState({ errPassword: "" })}
                />
                <Button
                  loading={this.state.loading}
                  title="LOGIN"
                  buttonStyle={LoginScreenStyle.loginButton}
                  containerStyle={LoginScreenStyle.loginButtonContainer}
                  onPress={() => this.doLogin()}
                  // titleStyle={{ fontFamily: font.Regular, fontSize: RFPercentage(2)}}
                  textStyle={{
                    textAlign: 'center',
                    fontSize: 16,
                    fontFamily: "AktivGrotesk-Bold",
                  }}
                />
              </View>

              <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgetPassword')}>
                <Text style={LoginScreenStyle.text_forgot}>
                  {getLangValue(string.forgotPassword, 'en')}
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </ImageBackground>
        <CustomToast ref="defaultToastTop" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />

        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5} />}
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    isLoginLoading: state.Auth.isLoginLoading,
    user: state.Auth.user,
    loginError: state.Auth.loginError,
    notification: state.Auth.notification,
  };
}

export default connect(mapStateToProps, null)(LoginScreen);
