import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  ScrollView
} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Text, Button, Image } from "react-native-elements";
import LoginScreenStyle from "../../CustomStyle/index";
import { TextField } from "react-native-material-textfield";
import Icons from "../../Resource/index";
import colors from "../../Resource/Color";
import font from "../../Resource/fontFamily";
import string from "../../Resource/string";
import { getLangValue } from "../../Resource/string/language";
import { connect } from "react-redux";
import {confirmOtpRequest, confirmOtpSuccess, confirmOtpFail} from '../../Store/Actions/AuthAction';
import { ConfirmOtpService, ForgetPasswordService } from '../../Network/Service';
import CustomToast from "../../Component/CustomToast";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import NetInfo from "@react-native-community/netinfo";


class VerifyOtp extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      otp: "",
      errOtp: "",
      isConnected: false,
    };
  }

  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getData();
  }

  componentWillUnmount() {
    this._subscription && this._subscription();
    this.setState({ otp: ''});
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isConfirmOtpLoading !== this.props.isConfirmOtpLoading) {
      this.setState({ loading: this.props.isConfirmOtpLoading});
    }
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  doRediect(screen) {
    this.props.navigation.navigate(screen);
  }

  getData(){
    setTimeout(() => {
      AsyncStorage.getItem("notification")
      .then(data => {
        const myData = JSON.parse(data);
        
        if (myData != null) {
            if (myData.extra==="notification") {
              this.doRediect("Notification");
              AsyncStorage.removeItem("notification");
            } else {
              
            }
        } else {
        }
      })
      .done(); 
    }, 500);
  }

  doVerify = async () => {
    const { otp } = this.state;
    if (otp === "") {
      this.otpRef.focus();
      this.setState({ errOtp: getLangValue(string.emptyOtp, 'en')});
      return;
    } 
    this.setState({ errOtp: "" });
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
   }
    const userData = {
     email: this.props.route.params.email,
     otp: this.state.otp,
    }
    try {
      this.props.dispatch(confirmOtpRequest());
      const res = await ConfirmOtpService(userData);
      const code = res.code;
      switch (code) {
        case 200:
          this.props.dispatch(confirmOtpSuccess(res));
          this.refs.success.ShowToastFunction(res.message);
          setTimeout(() => {
            this.props.navigation.navigate('SetNewPassword', {email: res.data.email, otp: res.data.otp});
          }, 2000);
          break;
        case 400:
          this.props.dispatch(confirmOtpFail(res));
          this.refs.defaultToastTop.ShowToastFunction(res.message);
          break;
        
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(confirmOtpFail(error));
    }
  }

  handleResendOtp = async () => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
   }
    const userData = {
      email: this.props.route.params.email,
     }
     try {
       this.props.dispatch(confirmOtpRequest());
       const res = await ForgetPasswordService(userData);
       const code = res.code;
       switch (code) {
         case 200:
           this.props.dispatch(confirmOtpSuccess(res));
           this.refs.success.ShowToastFunction(res.message);
           break;
         case 400:
           this.props.dispatch(confirmOtpFail(res));
           this.refs.defaultToastTop.ShowToastFunction(res.message);
           break;
         
         default:
           break;
       }
     } catch (error) {
       this.props.dispatch(confirmOtpFail(error));
     }
  }
  render() {
    return (
      <SafeAreaView style={LoginScreenStyle.container}>
        <ImageBackground
          source={Icons.bg_login}
          style={LoginScreenStyle.ImageBackground}
        >
          <ScrollView>
            <View style={LoginScreenStyle.container}>
            <View style={{alignItems: 'center', marginTop: "10%",
    marginBottom: "5%"}}>
              <Image
                source={Icons.badge_white}
                style={LoginScreenStyle.image_badge}
              />
              </View>
              <View style={LoginScreenStyle.card}>
                <Text style={LoginScreenStyle.text_forget_heading}>{getLangValue(string.verifyOtp, 'en')}</Text>
                <TextField
                  ref={ref => {
                    this.otpRef = ref;
                  }}
                  underlineColorAndroid={colors.transparent}
                  value={this.state.otp}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ otp: text });
                  }}
                  returnKeyType="next"
                  label={getLangValue(string.enterOtp, 'en')}
                  keyboardType="number-pad"
                  error={this.state.errOtp}
                  placeholderTextColor={colors.inputTextColor}
                  textColor={colors.inputTextColor}
                  tintColor={colors.inputTextColor}
                  fontSize={16}
                  containerStyle={LoginScreenStyle.containerStyleTextField}
                  inputContainerStyle={
                    LoginScreenStyle.inputContainerStyleTextField
                  }
                  autoCapitalize="none"
                  onBlur={() => this.setState({ errOtp: "" })}
                  labelTextStyle={{ fontFamily: font.Regular }}
                  titleTextStyle={{ fontFamily: font.Regular }}
                  affixTextStyle={{ fontFamily: font.Regular }}
                />
                <View style={{justifyContent: 'flex-end', alignItems: 'flex-end', marginEnd: 20}}>
                <TouchableOpacity onPress={this.handleResendOtp}>
                <Text style={LoginScreenStyle.resend_text}>
                  {getLangValue(string.resendOtp, 'en')}
                </Text>
              </TouchableOpacity>
              </View>
                <Button
                  loading={this.state.loading}
                  title={getLangValue(string.verify, 'en')}
                  buttonStyle={LoginScreenStyle.loginButton}
                  containerStyle={LoginScreenStyle.loginButtonContainer}
                  onPress={() => this.doVerify()}
                  // titleStyle={{ fontFamily: font.Regular, fontSize: RFPercentage(2)}}
                  textStyle={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Bold",
                }}
                />
                <View>
                <TouchableOpacity onPress={() => {this.props.navigation.navigate('Login')}}>
                <Text style={LoginScreenStyle.text_login_red}>
                  {getLangValue(string.login, 'en')}
                </Text>
              </TouchableOpacity>
              </View>
              </View>
             </View>
          </ScrollView>
        </ImageBackground>
        <CustomToast ref = "defaultToastTop" backgroundColor={colors.white} textColor={colors.red} position = "bottom"/>
        <CustomToast ref = "success" backgroundColor={colors.white} textColor={colors.greenText} position = "top"/>
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    isConfirmOtpLoading: state.Auth.isConfirmOtpLoading,
    confirmOtpDetails: state.Auth.confirmOtpDetails,
    confirmOtpError: state.Auth.confirmOtpError, 
  };
}

export default connect(mapStateToProps,null)(VerifyOtp);
