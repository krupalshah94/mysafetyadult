import { StyleSheet } from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';
  import colors from '../../../Resource/Color/index';
import { RFPercentage } from 'react-native-responsive-fontsize';

export const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: "#DFE2F1" },
    row: {flexDirection: 'row',},
    margin_10: {margin: 10},
    m_bottom_3: {marginBottom: 3},
    connectionView: {
        marginTop: hp(0.5),
        marginBottom: hp(0.5),
        marginStart: wp(4),
        marginEnd: wp(4),
        backgroundColor: colors.white,
        borderRadius: 4,
        padding: 10,
      },
    userImage: {
      width: hp(10),
      height: hp(10) 
    },
    tickedImage: {
      width: 18, 
      height: 18, 
      position: 'absolute', 
      right: -2, 
      top: 4
    },
    nameText: {
      fontFamily: "AktivGrotesk-Regular", 
      fontSize: RFPercentage(2.5)
    },
    badgeText: {
      fontFamily: "AktivGrotesk-Regular", 
      fontSize: RFPercentage(2), 
      color: colors.textColorLight
    },
    timeText: {
      fontFamily: "AktivGrotesk-Regular", 
      fontSize: RFPercentage(2), 
      color: colors.textColorLight
    }
})

