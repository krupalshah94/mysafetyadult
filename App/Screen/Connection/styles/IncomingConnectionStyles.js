import { StyleSheet } from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';
  import colors from '../../../Resource/Color/index';
import { RFPercentage } from 'react-native-responsive-fontsize';

export const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: "#DFE2F1" },
    flex_1: {flex: 1},
    row: {flexDirection: 'row',},
    margin_10: {margin: 10},
    m_bottom_3: {marginBottom: 3},
    m_bottom_20: {marginBottom: 20},
    connectionView: {
        marginTop: hp(0.5),
        marginBottom: hp(0.5),
        marginStart: wp(4),
        marginEnd: wp(4),
        backgroundColor: colors.white,
        borderRadius: 4,
        padding: 10,
      },
    userImage: {
      width: hp(10),
      height: hp(10) 
    },
    tickedImage: {
      width: 18, 
      height: 18, 
      position: 'absolute', 
      right: -2, 
      top: 4
    },
    nameText: {
      fontFamily: "AktivGrotesk-Regular", 
      fontSize: RFPercentage(2.5)
    },
    badgeText: {
      fontFamily: "AktivGrotesk-Regular", 
      fontSize: RFPercentage(2), 
      color: colors.textColorLight
    },
    timeText: {
      fontFamily: "AktivGrotesk-Regular", 
      fontSize: RFPercentage(2), 
      color: colors.textColorLight
    },
    confirmBtnView: {
        backgroundColor: colors.bg_btn_blue, 
        borderRadius: 20, 
        borderWidth: 1, 
        borderColor: colors.white
    },
    confirmText: {
        textAlign: 'center',
        color: colors.white,
        fontSize: RFPercentage(2),
        paddingTop: 6,
        paddingBottom: 6,
        paddingStart: 10,
        paddingEnd: 10, 
    },
    deleteBtnView: {
        backgroundColor: '#ffffff', 
        borderRadius: 20, 
        borderWidth: 1, 
        borderColor: "#43A7E6"
    },
    deleteText: {
        textAlign: 'center',
        fontSize: RFPercentage(2),
        paddingTop: 6,
        paddingBottom: 6,
        paddingStart: 10,
        paddingEnd: 10,
    }
})