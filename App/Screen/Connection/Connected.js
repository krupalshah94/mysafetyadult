import React, { Component } from 'react'
import { View, SafeAreaView, FlatList, Text, BackHandler, ScrollView } from 'react-native'
import {styles} from './styles/connectionStyles';
import Icons from '../../Resource/index';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP } from 'react-native-responsive-screen';
import ConnectionItem from './SubComponent/ConnectionItem';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-community/async-storage';
import * as Constants from '../../Resource/Constants';
import BarIndicatorLoader from '../../Component/BarIndicatorLoader';
import CustomToast from '../../Component/CustomToast';
import colors from '../../Resource/Color';
import { connect } from 'react-redux';
import {getConnectionList} from '../../Network/Service';
import {connectedUserListRequest, connectedUserListSuccess, connectedUserListFailed,
  connectedUserSendRequest, connectedUserSendSuccess, connectedUserSendFailed
 } from '../../Store/Actions/ConnectionAction';
import { RFPercentage } from 'react-native-responsive-fontsize';
import NoInternetConnection from '../../Component/NoInternetConnection';
import ConnectionSendItem from './SubComponent/ConnectionSendItem';
class Connected extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: false,
            refreshing: false,
            refreshingSend: false,
            viewEmpty: true,
            viewEmptySend: true,
            viewEmptySend: true,
            last_page: 0,
            page: 1,
            per_page:10,
            last_pageSend: 0,
            pageSend: 1,
            per_pageSend:10,
            connections: [],
            connectionsSend: [],
            connectionData: [
                {id: 0, name: 'William', image: Icons.user_1, badge:'465554', time: '3'},
                {id: 1, name: 'Jacob', image: Icons.user_2, badge:'455551', time: '2'},
                {id: 2, name: 'William', image: Icons.user_1, badge:'465554', time: '3'},
                {id: 3, name: 'Jacob', image: Icons.user_2, badge:'455551', time: '2'},
                {id: 4, name: 'William', image: Icons.user_1, badge:'465554', time: '3'},
                {id: 5, name: 'Jacob', image: Icons.user_2, badge:'455551', time: '2'},
            ]
        };
    } 

    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      const unsubscribe = this.props.navigation.addListener('tabPress', e => {
        this.getToken();
      });
      this.getToken();
      this._subscription = NetInfo.addEventListener(
          this._handleConnectivityChange
        );
      
        return unsubscribe;
      }
    
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        this._subscription && this._subscription();
      }

      onBackPress = () => {
        this.props.navigation.goBack();
        return true;
      }
    
      _handleConnectivityChange = state => {
        this.setState({
          isConnected: state.isConnected
        });
      };

    componentDidUpdate(prevProps) {
        if (prevProps.isLoadingConnectedUserList !== this.props.isLoadingConnectedUserList) {
            this.setState({ isLoading: this.props.isLoadingConnectedUserList });
        }
        if (prevProps.connectedUserList !== this.props.connectedUserList) {
          if (this.props.connectedUserList.data.data.length === 0) {
            this.setState({ viewEmpty: true, refreshing: false, connections: []}) 
          } else {
            this.setState({ viewEmpty: false,
              connections: this.state.page === 1? this.props.connectedUserList.data.data : [...this.state.connections, ...this.props.connectedUserList.data.data],
            refreshing: false,
            last_page: this.props.connectedUserList.data.last_page
            })
          }
        }
        if (prevProps.actionConnection !== this.props.actionConnection) {
            const code = this.props.actionConnection.code;
            const status = this.props.actionConnection.status;
            if (code === 200 && status === "true") {
                this.getConnections();
            }
        }
        if (prevProps.isLoadingConnectedUserSend !== this.props.isLoadingConnectedUserSend) {
          this.setState({ isLoading: this.props.isLoadingConnectedUserSend });
        }
        if (prevProps.connectedUserSend !== this.props.connectedUserSend){
          if (this.props.connectedUserSend.data.data.length === 0) {
            this.setState({ viewEmptySend: true, refreshingSend: false, connectionsSend: []}) 
          } else {
            this.setState({ viewEmptySend: false,
              connectionsSend: this.state.pageSend === 1? this.props.connectedUserSend.data.data : [...this.state.connections, ...this.props.connectedUserList.data.data],
            refreshingSend: false,
            last_pageSend: this.props.connectedUserSend.data.last_page
            })
          }
        }
    }

    async getToken() {
        this.Token = await AsyncStorage.getItem(Constants.TOKEN);
        if (this.Token !== '') {
            this.getConnections();
            this.getConnectionsSendRequest();      
        }
      }

      handleLoadMore = () => {
        if (this.state.page != this.state.last_page) {
          this.setState(
            {
              page: this.state.page + 1
            },
            () => {
              this.Token=== "" ? null : this.getConnections();
            }
          );
        }
      };

      handleLoadMoreSend = () => {
        if (this.state.pageSend != this.state.last_pageSend) {
          this.setState(
            {
              pageSend: this.state.pageSend + 1
            },
            () => {
              this.Token=== "" ? null : this.getConnectionsSendRequest();
            }
          );
        }
      };
    
      handleRefresh = () => {
        this.setState(
          {
            page: 1,
            refreshing: true
          },
          () => {
            this.getConnections();
          }
        );
      };

      handleRefreshSend = () => {
        this.setState(
          {
            pageSend: 1,
            refreshingSend: true
          },
          () => {
            this.getConnectionsSendRequest();
          }
        );
      };

      renderEmpty = () => {
        return <View style={styles.container}>
            <View style={{
                width: wp(90), 
                height: hp(100),
                margin: 20,
                }}>
            <Text style={{
                textAlign: 'center',
                fontSize: RFPercentage(2),
                fontFamily: "AktivGrotesk-Regular", 
            }}>No Connection Found</Text>
            </View>
        </View>;
      }

      renderEmptySend = () => {
        return <View style={styles.container}>
            <View style={{
                width: wp(90), 
                height: hp(100),
                margin: 20,
                }}>
            <Text style={{
                textAlign: 'center',
                fontSize: RFPercentage(2),
                fontFamily: "AktivGrotesk-Regular", 
            }}>No Send Request Found</Text>
            </View>
        </View>;
      }

      getConnections = async() => {
        if (!this.state.isConnected) {
            this.refs.internetConnection.ShowToastFunction('No Internet Connected');
           return;
         }
         try{
            const status = 'connected';
            const { page, per_page } = this.state;
            this.props.dispatch(connectedUserListRequest());
            const res = await getConnectionList(this.Token, page, per_page, status);
            const code = res.code;
            switch (code) {
              case 200:
                this.props.dispatch(connectedUserListSuccess(res));  
                break;
              case 400: 
              this.props.dispatch(connectedUserListFailed(res));
              default:
                break;
            }
          } catch (error) {
            this.props.dispatch(connectedUserListFailed(error));
            this.setState({ refreshing: false})
          }
      }

      getConnectionsSendRequest = async () => {
        if (!this.state.isConnected) {
          this.refs.internetConnection.ShowToastFunction('No Internet Connected');
         return;
       }
       try{
          const status = 'send_request';
          const { page, per_page } = this.state;
          this.props.dispatch(connectedUserSendRequest());
          const res = await getConnectionList(this.Token, page, per_page, status);
          const code = res.code;
          switch (code) {
            case 200:
              this.props.dispatch(connectedUserSendSuccess(res));  
              break;
            case 400: 
            this.props.dispatch(connectedUserSendFailed(res));
            default:
              break;
          }
        } catch (error) {
          this.props.dispatch(connectedUserSendFailed(error));
          this.setState({ refreshing: false})
        }
      }

    render() {
        const { connectionData, connections, connectionsSend } = this.state;
        return (
            <SafeAreaView style={styles.container}>
            <ScrollView>
            <View style={{marginTop: 30}}>
            <Text 
            style={{
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: RFPercentage(2.2),
                  color: colors.black,
                  textTransform: 'uppercase',
                  marginStart: 20              
            }}>Connected Connection</Text>
            <FlatList
            contentContainerStyle={{ paddingBottom: hp(1) }}
            data={connections} 
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => {
                return  <ConnectionItem item={item} navigation={this.props.navigation} />
            }}
            stickySectionHeadersEnabled={false}
          // style={styles.listView}
            ListEmptyComponent={this.state.viewEmpty ? this.renderEmpty : null}
            onEndReached={this.handleLoadMore}
            onEndThreshold={50}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            />
            </View>

            <View style={{marginTop: 20,marginBottom: heightPercentageToDP(15)}}>
            <Text style={{
             fontFamily: "AktivGrotesk-Regular",
                  fontSize: RFPercentage(2.2),
                  color: colors.black,
                  textTransform: 'uppercase',
                  marginStart: 20 
            }}>Send Connection Request</Text>
            <FlatList
            contentContainerStyle={{ paddingBottom: hp(1) }}
            data={connectionsSend} 
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => {
                return  <ConnectionSendItem item={item} navigation={this.props.navigation} />
            }}
            stickySectionHeadersEnabled={false}
          // style={styles.listView}
            ListEmptyComponent={this.state.viewEmptySend ? this.renderEmptySend : null}
            onEndReached={this.handleLoadMoreSend}
            onEndThreshold={50}
            onRefresh={this.handleRefreshSend}
            refreshing={this.state.refreshingSend}
            />
            </View>
            </ScrollView>
            {this.state.isLoading && <BarIndicatorLoader color='black' size={16} count={5}/>}
            <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
            {!this.state.isConnected && <NoInternetConnection />}
            </SafeAreaView>
        )
    }
}

function mapStateToProps(state) {
    return {
      isLoadingConnectedUserList: state.Connection.isLoadingConnectedUserList,
      connectedUserList: state.Connection.connectedUserList,
      errorConnectedUserList: state.Connection.errorConnectedUserList,
      actionConnection: state.Connection.actionConnection,
      isLoadingConnectedUserSend: state.Connection.isLoadingConnectedUserSend,
      connectedUserSend: state.Connection.connectedUserSend,
      errorConnectedUserSend: state.Connection.errorConnectedUserSend
    };
  }
  export default connect(mapStateToProps,null)(Connected);
