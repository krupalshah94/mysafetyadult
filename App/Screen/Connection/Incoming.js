import React, { Component } from 'react'
import { View, SafeAreaView, FlatList, Text, BackHandler, TouchableOpacity } from 'react-native'
import {styles} from './styles/connectionStyles';
import Icons from '../../Resource/index';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import IncomingConnectionItem from './SubComponent/IncomingConnectionItem';
import AsyncStorage from '@react-native-community/async-storage';
import * as Constants from '../../Resource/Constants';
import {getConnectionList} from '../../Network/Service';
import {getConnectionListFailed, getConnectionListSuccess, getConnectionListRequest } from '../../Store/Actions/ConnectionAction';
import CustomToast from '../../Component/CustomToast';
import colors from '../../Resource/Color';
import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import { RFPercentage } from 'react-native-responsive-fontsize';
import BarIndicatorLoader from '../../Component/BarIndicatorLoader';
import NoInternetConnection from '../../Component/NoInternetConnection';

class Incoming extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page:1,
            per_page: 10,
            connectionData: [
                {id: 0, name: 'William', image: Icons.user_1, badge:'465554', time: 'Sun at 6:25 PM'},
                {id: 1, name: 'Jacob', image: Icons.user_2, badge:'455551', time: 'Sun at 6:25 PM'},
                {id: 2, name: 'William', image: Icons.user_1, badge:'465554', time: 'Sun at 6:25 PM'},
                {id: 3, name: 'Jacob', image: Icons.user_2, badge:'455551', time: 'Sun at 6:25 PM'},
                {id: 4, name: 'William', image: Icons.user_1, badge:'465554', time: 'Sun at 6:25 PM'},
                {id: 5, name: 'Jacob', image: Icons.user_2, badge:'455551', time: 'Sun at 6:25 PM'},
            ],
            isLoading: false,
            incomingConnection: [],
            refreshing: false,
            isConnected: false,
            viewEmpty: false,
            page:1,
            per_page:10,
            last_page:0,
        };
        this.userData = '';
        this.Token = '';
    } 
   
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        const unsubscribe = this.props.navigation.addListener('tabPress', e => {
          this.getToken();
        });
        this.getToken();
        this._subscription = NetInfo.addEventListener(
          this._handleConnectivityChange
        );
        console.log('props', this.props)
        return unsubscribe;
      }
    
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        this._subscription && this._subscription();
      }

      onBackPress = () => {
        this.props.navigation.goBack();
        return true;
      }
    
      _handleConnectivityChange = state => {
        this.setState({
          isConnected: state.isConnected
        });
      };

    componentDidUpdate(prevProps) {
      console.log('props', this.props)
        if (prevProps.isLoadingGetConnectionList !== this.props.isLoadingGetConnectionList) {
            this.setState({ isLoading: this.props.isLoadingGetConnectionList });
        }
        if (prevProps.connectionList !== this.props.connectionList) {
          if (this.props.connectionList.data.data.length === 0) {
            this.setState({ viewEmpty: true, refreshing: false, incomingConnection: [] }) 
          } else {
            this.setState({ viewEmpty: false,
            incomingConnection: this.state.page === 1? this.props.connectionList.data.data : [...this.state.incomingConnection, ...this.props.connectionList.data.data],
            refreshing: false,
            last_page: this.props.connectionList.data.last_page
            })
          }
        }
        if (prevProps.actionConnection !== this.props.actionConnection) {
            const code = this.props.actionConnection.code;
            const status = this.props.actionConnection.status;
            if (code === 200 && status === "true") {
                this.getIncomingConnection();
            }
        }
    }

    async getToken() {
        this.Token = await AsyncStorage.getItem(Constants.TOKEN);
        this.userData = await AsyncStorage.getItem(Constants.USER_DATA);
        this.userData = JSON.parse(this.userData);
        if (this.Token !== '') {
            this.getIncomingConnection();
        }
      }
    
      getIncomingConnection = async () => {
        if (!this.state.isConnected) {
            this.refs.internetConnection.ShowToastFunction('No Internet Connected');
           return;
         }
        try{
            const status = 'received_request';
            const { page, per_page } = this.state;
            this.props.dispatch(getConnectionListRequest());
            const res = await getConnectionList(this.Token, page, per_page, status);
            const code = res.code;
            switch (code) {
              case 200:
                this.props.dispatch(getConnectionListSuccess(res));  
                break;
              case 400: 
              this.props.dispatch(getConnectionListFailed(res));
              default:
                break;
            }
          } catch (error) {
            this.props.dispatch(getConnectionListFailed(error));
            this.setState({ refreshing: false})
          }
      }

      handleLoadMore = () => {
        if (this.state.page != this.state.last_page) {
          this.setState(
            {
              page: this.state.page + 1
            },
            () => {
              this.Token=== "" ? null : this.getIncomingConnection();
            }
          );
        }
      };
    
      handleRefresh = () => {
        this.setState(
          {
            page: 1,
            refreshing: true
          },
          () => {
            this.getIncomingConnection();
          }
        );
      };

      renderEmpty = () => {
        return <View style={styles.container}>
            <View style={{
                width: widthPercentageToDP(90), 
                height: heightPercentageToDP(100),
                margin: 20,
                }}>
           
            <Text 
            style={{
                textAlign: 'center',
                fontSize: RFPercentage(3),
                fontFamily: "AktivGrotesk-Regular", 
            }}>No Request Found</Text>
            </View>
        </View>;
      }

    render() {
        const { connectionData, incomingConnection } = this.state;
        return (
            <SafeAreaView style={styles.container}>
            <View style={{marginTop: 10, flex:1}}>
            <FlatList
            contentContainerStyle={{ paddingBottom: heightPercentageToDP(15) }}
            data={incomingConnection} 
            keyExtractor={item => item.id.toString()}
            renderItem={({ item }) => {
              if (item.connection_status !== "connected") {
                // if (item.user.id !== this.userData.id) {
                return  <IncomingConnectionItem item={item} navigation={this.props.navigation} />
                // }
              }
            }}
            stickySectionHeadersEnabled={false}
          // style={styles.listView}
            ListEmptyComponent={this.state.viewEmpty ? this.renderEmpty : null}
            onEndReached={this.handleLoadMore}
            onEndThreshold={50}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            />
            </View>
            {this.state.isLoading && <BarIndicatorLoader color='black' size={16} count={5}/>}
            <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
            {!this.state.isConnected && <NoInternetConnection />}
            </SafeAreaView>
        )
    }
}


function mapStateToProps(state) {
    return {
      isLoadingGetConnectionList: state.Connection.isLoadingGetConnectionList,
      connectionList: state.Connection.connectionList,
      errorConnectionList: state.Connection.errorConnectionList,
      actionConnection: state.Connection.actionConnection,
    };
  }
  export default connect(mapStateToProps,null)(Incoming);