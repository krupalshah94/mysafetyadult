import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, SafeAreaView, ImageStore, ActivityIndicator } from 'react-native'
import {styles} from '../styles/connectionStyles';
import Icons from '../../../Resource/index';
import { getLangValue } from '../../../Resource/string/language';
import string from '../../../Resource/string';
import moment from 'moment';
import colors from '../../../Resource/Color';
import { RFPercentage } from 'react-native-responsive-fontsize';
import { widthPercentageToDP } from 'react-native-responsive-screen';

export default class ConnectionSendItem extends Component {
    state = {
        loadingImg: true,
    }
    handleLoading = () => {
        this.setState({
          loadingImg: false,
        });
      }
    render() {
        const { navigation, item } = this.props;
        return (
            <View>
                <View  style={styles.connectionView}>
                    <View style={styles.row}>
                        <View style={styles.margin_10}>
                        <View>
                            <Image source={item.user.profile_pic !== ''? {uri: item.user.profile_pic}: Icons.inviteUser} style={styles.userImage}
                             onLoadEnd={this.handleLoading}
                            />
                        <ActivityIndicator
                            style={{
                            position: 'absolute',
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0,
                            backgroundColor: colors.transparent,
                            }}
                            animating={this.state.loadingImg}
                        />
                        </View>
                            <Image source={Icons.tick} style={styles.tickedImage} />
                        </View>
                        <View style={styles.margin_10}>
                            <View style={styles.m_bottom_3}>
                                <Text style={styles.nameText}>{item.user.name}</Text>
                            </View>
                            <View style={styles.m_bottom_3}>
                                <Text style={styles.badgeText}>{getLangValue(string.badgeNumber, 'en')}: {item.user.batch_id}</Text>
                            </View>
                            <View style={styles.m_bottom_3}>
                                <Text style={styles.timeText}>Since {moment(item.created_at).startOf('hour').fromNow()}</Text>
                            </View>
                            <View style={{backgroundColor: colors.awaitingEvent, borderRadius: 20, padding: 5, marginTop: 5, marginRight: 10, width: widthPercentageToDP(25)}}>
                    <Text style={{
                      textAlign: 'center',
                      color: colors.white,
                      fontSize: RFPercentage(2),
                      textTransform:'capitalize',
                    }}>Awaiting</Text>
                  </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
