import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, Alert, ActivityIndicator } from 'react-native'
import { styles } from '../styles/IncomingConnectionStyles';
import { getLangValue } from '../../../Resource/string/language';
import string from '../../../Resource/string';
import icons from '../../../Resource/index';
import NetInfo from "@react-native-community/netinfo";
import CustomToast from '../../../Component/CustomToast';
import colors from '../../../Resource/Color';
import BarIndicatorLoader from '../../../Component/BarIndicatorLoader';
import { connectionActions } from '../../../Network/Service';
import { actionConnectionRequest, actionConnectionSuccess, actionConnectionFailed } from '../../../Store/Actions/ConnectionAction';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import * as Constants from '../../../Resource/Constants';
import { StackActions } from 'react-navigation';

class IncomingConnectionItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: false,
            isLoading: false,
            loadingImg: true,
        };
        this.Token = '';
    }
    
    componentDidMount() {
        this._subscription = NetInfo.addEventListener(
            this._handleConnectivityChange
          );
          this.getToken();
    }

    componentWillUnmount() {
        this._subscription && this._subscription();
      }
    
    _handleConnectivityChange = state => {
    this.setState({
        isConnected: state.isConnected
    });
    };
    
    componentDidUpdate(prevProps) {
        if (prevProps.isLoadingActionConnection !== this.props.isLoadingActionConnection) {
            this.setState({ isLoading: this.props.isLoadingActionConnection});
        }
        if (prevProps.actionConnection !== this.props.actionConnection) {

        }
    }
    
    async getToken() {
        this.Token = await AsyncStorage.getItem(Constants.TOKEN);
      }
      

    handleLogout = () => {
    AsyncStorage.clear();
    this.doFinish('Login'); 
    }

    doFinish(screen) {
        // this.props.navigation.dispatch(StackActions.popToTop());
    }

    handleDeleteRequest = async() => {
        Alert.alert(
            'Delete Request',
            'Are you sure you want to delete request?',
            [
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'OK', onPress: async() => {
                if (!this.state.isConnected) {
                    this.refs.internetConnection.ShowToastFunction('No Internet Connected');
                   return;
                 }
                 try{
                    this.props.dispatch(actionConnectionRequest());
                    const data = {
                        connection_id: this.props.item.id,
                        action: 'decline',
                    };
                    const res = await connectionActions(this.Token, data);
                    const code = res.code;
                    switch (code) {
                      case 200:
                        this.props.dispatch(actionConnectionSuccess(res));  
                        break;
                      case 400: 
                      this.props.dispatch(actionConnectionFailed(res));
                      case 401:
                        this.handleLogout();
                      default:
                        break;
                    }
                  } catch (error) {
                    this.props.dispatch(actionConnectionFailed(error));
                  }
              }},
            ],
            { cancelable: false }
          )
    }

    handleLoading = () => {
        this.setState({
          loadingImg: false,
        });
    }
    
    render() {
        const { item, navigation } = this.props;
        return (
            <View>
                <View style={styles.connectionView}>
                    <View style={styles.row}>
                        <View style={styles.margin_10}>
                            <View>
                                <Image source={item.user.profile_pic !== ''? {uri:item.user.profile_pic}: icons.inviteUser} style={styles.userImage} 
                                     onLoadEnd={this.handleLoading}
                                />
                                <ActivityIndicator
                                style={{
                                position: 'absolute',
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0,
                                backgroundColor: colors.transparent,
                                }}
                                animating={this.state.loadingImg}
                            />
                            </View>
                        </View>
                        <View style={[styles.flex_1, {padding: 10}]}>
                            <View style={styles.m_bottom_3}>
                                <Text style={styles.nameText}>{item.user.name} <Text style={styles.badgeText}>
                                {getLangValue(string.sendYouRequest, 'en')}</Text></Text>
                            </View>
                            <View style={styles.m_bottom_3}>
                                <Text style={styles.badgeText}>{getLangValue(string.badgeNumber, 'en')}: {item.user.batch_id}</Text>
                            </View>
                            <View style={styles.m_bottom_20}>
                                <Text style={styles.timeText}>{item.created_humans}</Text>
                            </View>
                            <View style={styles.flex_1}>
                                <View style={styles.row}>
                                    <View style={[styles.flex_1,{marginRight: 10 }]}>
                                        <TouchableOpacity onPress={() => navigation.navigate('ConnectionRequested', {connection: item})}>
                                            <View style={styles.confirmBtnView}>
                                                <Text style={styles.confirmText}>{getLangValue(string.confirm, 'en')}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.flex_1}>
                                        <TouchableOpacity onPress={this.handleDeleteRequest}>
                                            <View style={styles.deleteBtnView}>
                                                <Text style={styles.deleteText}>{getLangValue(string.delete, 'en')}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
                {this.state.isLoading && <BarIndicatorLoader color='black' size={16} count={5}/>}
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        isLoadingActionConnection: state.Connection.isLoadingActionConnection,
        actionConnection: state.Connection.actionConnection,
        errorActionConnection: state.Connection.errorActionConnection
    };
  }
  export default connect(mapStateToProps,null)(IncomingConnectionItem);