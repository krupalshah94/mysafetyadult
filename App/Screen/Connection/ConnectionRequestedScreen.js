import React, { Component } from 'react'
import {
  ScrollView,
  SafeAreaView,
  Image,
  View,
  StyleSheet,
  Text,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import colors from '../../Resource/Color';
import { RFPercentage } from 'react-native-responsive-fontsize';
import HeaderWithBack from '../../Component/HeaderWithBack';
import images from "../../Resource/index";
import { getLangValue } from '../../Resource/string/language';
import string from '../../Resource/string';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { connect } from 'react-redux';
import NetInfo from "@react-native-community/netinfo";
import CustomToast from '../../Component/CustomToast';
import BarIndicatorLoader from '../../Component/BarIndicatorLoader';
import { connectionActions } from '../../Network/Service';
import { actionConnectionRequest, actionConnectionSuccess, actionConnectionFailed } from '../../Store/Actions/ConnectionAction';
import AsyncStorage from '@react-native-community/async-storage';
import * as Constants from '../../Resource/Constants';

class ConnectionRequestedScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      userName: "Williams",
      userBadge: "#44564",
      city: "Bakersfield",
      state: "California",
      profilePicture: "",
      loading: true
    };
  }

  componentDidMount() {
    this.setState({ user: this.props.route.params.connection });
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentWillUnmount() {
    this._subscription && this._subscription();
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.isLoadingActionConnection !== this.props.isLoadingActionConnection) {
      this.setState({ isLoading: this.props.isLoadingActionConnection });
    }
    if (prevProps.actionConnection !== this.props.actionConnection) {

    }
  }

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
  }

  handleAcceptRequest =  async () => {
    const { user } = this.state;
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
   }
   try{
      this.props.dispatch(actionConnectionRequest());
      const data = {
          connection_id: user.id,
          action: 'accept',
      };
      const res = await connectionActions(this.Token, data);
      const code = res.code;
      switch (code) {
        case 200:
          this.props.dispatch(actionConnectionSuccess(res));  
          this.props.navigation.navigate('Connections', {screen: 'Connected'});
          break;
        case 400: 
        this.props.dispatch(actionConnectionFailed(res));
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(actionConnectionFailed(error));
    }

  }

  handleLoading = () => {
    this.setState({
      loading: false,
    });
  }
  render() {
    const { user: {user} } = this.state;
    console.log('connected user data', user)
    return (
      <SafeAreaView style={customStyles.container}>
        <HeaderWithBack notify={false} name={getLangValue(string.connection, 'en')} navigation={this.props.navigation} />
        <ScrollView>
          <View style={customStyles.topView}>
            <View style={customStyles.cart}>
              <View style={customStyles.userView}>
                <View style={customStyles.imageView}>
                  <Image
                    source={user && user.profile_pic !== '' ? { uri: user.profile_pic } : images.inviteUser}
                    style={customStyles.UserImage}
                    onLoadEnd={this.handleLoading}
                  />
                      <ActivityIndicator
                        style={{
                          position: 'absolute',
                          left: 0,
                          right: 0,
                          top: 0,
                          bottom: 0,
                          backgroundColor: colors.transparent,
                        }}
                        animating={this.state.loading}
                      />
                </View>
              </View>
              <View style={customStyles.detailView}>
                <Text style={customStyles.userName}>{user && user.name}</Text>
                <Text style={customStyles.userBadge}>
                  {getLangValue(string.badgeNumber, 'en')}: {user && user.batch_id}
                </Text>
              </View>
              <View>
                <TouchableOpacity onPress={this.handleAcceptRequest}>
                  <View style={customStyles.confirmBtnView}>
                    <Text style={customStyles.confirmText}>{getLangValue(string.accept, 'en')}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {this.state.isLoading && <BarIndicatorLoader color='black' size={16} count={5} />}
      </SafeAreaView>
    )
  }
}


const customStyles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.bg_color },
  topView: { marginTop: 70, marginBottom: 30 },
  userView: { flex: 1, alignItems: "center" },
  cart: {
    marginStart: 20,
    marginEnd: 20,
    backgroundColor: colors.white,
    borderRadius: 5,
    position: "relative"
  },
  imageView: {
    bottom: 45,
    zIndex: 100,
    flexDirection: "row",
    alignItems: "center",
    position: "relative"
  },
  detailView: { alignItems: "center", marginTop: -30, marginBottom: 15 },
  UserImage: {
    height: 150,
    width: 150
  },
  tickImage: {
    top: 35,
    right: 35,
    zIndex: 100,
    height: 45,
    width: 45
  },
  line: {
    width: 1,
    backgroundColor: colors.textColorDark,
    marginEnd: 5,
    marginStart: 5
  },
  userName: {
    fontSize: RFPercentage(2),
    // fontFamily: font.Regular,
    color: colors.black
  },
  userBadge: {
    marginTop: 8,
    fontSize: RFPercentage(2),
    // fontFamily: font.Regular,
    color: colors.textColorDark
  },
  confirmBtnView: {
    backgroundColor: colors.bg_btn_blue,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: colors.white,
    width: widthPercentageToDP(25),
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    margin: 20
  },
  confirmText: {
    textAlign: 'center',
    color: colors.white,
    fontSize: RFPercentage(1.5),
    paddingTop: 6,
    paddingBottom: 6,
    paddingStart: 10,
    paddingEnd: 10,
  },
});

function mapStateToProps(state) {
  return {
    isLoadingActionConnection: state.Connection.isLoadingActionConnection,
    actionConnection: state.Connection.actionConnection,
    errorActionConnection: state.Connection.errorActionConnection
  };
}
export default connect(mapStateToProps, null)(ConnectionRequestedScreen);