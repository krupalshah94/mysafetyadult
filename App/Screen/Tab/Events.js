import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  SectionList,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
  BackHandler
} from "react-native";
import { Text, Image } from "react-native-elements";

import EventScreenStyle from "../../Resource/Style/EventScreenStyle";
import { Calendar } from "react-native-calendars";
import moment from "moment";
import Icons from "../../Resource/index";
import Colors from "../../Resource/Color/index";
import EventListComponent from "../../Component/EventListComponent";
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP } from 'react-native-responsive-screen';
import string from "../../Resource/string";
import {getLangValue} from '../../Resource/string/language';
import HeaderWithBack from "../../Component/HeaderWithBack";
import { RFPercentage } from "react-native-responsive-fontsize";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";
import * as Constants from '../../Resource/Constants';
import { connect } from "react-redux";
import { calenderDatesRequest, calenderDatesSuccess, calenderDatesFailed, getAllEventRequest, getAllEventSuccess, getAllEventFailed, refreshList } from "../../Store/Actions/EventAction";
import { getCalenderDates, allEventsWithDates } from "../../Network/Service";
import CustomToast from "../../Component/CustomToast";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import colors from "../../Resource/Color/index";
import EventsPageAllEvents from "../../Component/EventsPageAllEvents";
import NoInternetConnection from "../../Component/NoInternetConnection";

class Events extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      isConnected: false,
      sectionDotColor: ["#4181DF"],
      selectedDay: moment(new Date()).format("YYYY-MM-DD"),
      changedMonth: undefined,
      markedDates: [],
      events: [],
    };
    this.previousMonth = moment(new Date()).subtract(1, 'months').startOf('month').format('MMM YYYY');
    this.nextMonth = moment(new Date()).add(1, 'months').endOf('month').format('MMM YYYY');
    this.currentMonth = undefined;
    this.Token = '';
    this.month = moment(new Date()).format('M');
    this.year = moment(new Date()).format('YYYY');
  }
  
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentDidUpdate(prevProps) {

    if (prevProps.isLoadingAllEvent !== this.props.isLoadingAllEvent) {
      this.setState({ loading: this.props.isLoadingAllEvent });
    }
    if (prevProps.isLoadingActionEvent !== this.props.isLoadingActionEvent) {
      this.setState({ loading: this.props.isLoadingActionEvent});
    }
    
    if (prevProps.isLoadingAddNewEvent !== this.props.isLoadingAddNewEvent) {
      this.setState({ loading: this.props.isLoadingAddNewEvent})
    }

    if (prevProps.isLoadingCalenderDates !== this.props.isLoadingCalenderDates) {
      this.setState({ loading: this.props.isLoadingCalenderDates});
    }
    if (prevProps.calenderDates !== this.props.calenderDates) {
      this.setState({ markedDates: this.props.calenderDates.data});
    }
    if (prevProps.errorCalenderDates !== this.props.errorCalenderDates) {
      
    }
    if (prevProps.isListRefresh !== this.props.isListRefresh) {
      if(this.props.isListRefresh === true) {
        this.getEventList();
      }
    }
    if (prevProps.addNewEvent !== this.props.addNewEvent) {
      if (this.props.addNewEvent.code === 200 && this.props.addNewEvent.status === "true") {
        this.getEventList();
      }
    }
  } 

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    this._subscription && this._subscription();
  }

  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    if (this.Token !== '' && this.UserData !== '') {
      this.getCalenderEvents();
      this.getEventList();
    }
  }

  getCalenderEvents = async () => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    try {
      this.props.dispatch(calenderDatesRequest());
      const data = {
        month: parseInt(this.month),
        year: parseInt(this.year)
      }
      const res = await getCalenderDates(this.Token, data.month, data.year);
      const code = res.code;
      switch (code) {
        case 200:
        this.props.dispatch(calenderDatesSuccess(res));  
        break;
        case 400:
        this.props.dispatch(calenderDatesFailed(res));
        break;
        case 401:
          this.props.dispatch(calenderDatesFailed(res));
        break;
          
        default:
          break;
      }

    } catch(error) {
      this.props.dispatch(calenderDatesFailed(res));
    }
  }

  getEventList = async () => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    const { perPage, page } = this.state;
    try {
      this.props.dispatch(getAllEventRequest());
      const res = await allEventsWithDates(this.Token, page, perPage, this.state.selectedDay);
      const code = res.code;
      switch (code) {
        case 200:
          let eventsNew = [];
          if (res.data.events.length > 0) {
          eventsNew.push({title: this.state.selectedDay, data: res.data.events});
          }
          this.setState({ events: eventsNew, refreshing: false}, () => {
          });
          this.props.dispatch(getAllEventSuccess(res));
          this.props.dispatch(refreshList(false));
          break;
        case 400:
          this.setState({refreshing: false})
          this.props.dispatch(getAllEventFailed(res));
          this.refs.errorEvents.ShowToastFunction(res.message);
          this.props.dispatch(refreshList(false));
          case 401:
          this.setState({refreshing: false})
          this.props.dispatch(getAllEventFailed(res));
          this.handleLogout();
          this.refs.errorEvents.ShowToastFunction(res.message);
          this.props.dispatch(refreshList(false));
          default:
          break;
      }
    } catch (error) {
      this.props.dispatch(refreshList(false));
      this.props.dispatch(getAllEventFailed(error));
      this.setState({ refreshing: false, loading: false })
    }
  }
  
  renderItemSeprator() {
    return (
      <View
        style={{
          height: 1,
          backgroundColor: "#CCCCCC",
          marginStart: 10,
          marginEnd: 10
        }}
      />
    );
  }
  renderSectionHeader(section) {
    return (
      <View
        style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}
      >
        <View
          style={{
            flex: 1,
            height: 1,
            backgroundColor: "#BFBFBF",
            marginStart: 10,
            marginEnd: 5
          }}
        />
        <Text
          style={{
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 15,
            color: "#2640DF"
          }}
        >
          {section.title}
        </Text>
        <View
          style={{
            flex: 1,
            height: 1,
            backgroundColor: "#BFBFBF",
            marginStart: 5,
            marginEnd: 10
          }}
        />
      </View>
    );
  }
  renderSectionItem(item, index, section) {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          marginTop: 10,
          marginBottom: 10,
          marginStart: 5,
          marginEnd: 10
        }}
      >
        <View
          style={{
            width: 15,
            height: 15,
            backgroundColor: this.state.sectionDotColor[
              Math.floor(Math.random() * this.state.sectionDotColor.length)
            ],
            borderRadius: 7.5,
            margin: 5
          }}
        />
        <View style={{ flex: 1 }}>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 13,
              color: Colors.black
            }}
          >
            {item.event_title}
          </Text>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 12,
              color: "#888888"
            }}
          >
            {item.location}
          </Text>
        </View>

        <View>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 12,
              color: "#666666"
            }}
          >
            {item.time}
          </Text>
        </View>
      </View>
    );
  }
  renderConnectionItem(item, index) {
    return (
      <View
        style={{
          backgroundColor: Colors.white,
          flexDirection: "row",
          borderRadius: 4,
          marginEnd: 10,
          alignItems: "center",
          height: 80
        }}
      >
        <View style={{ flexDirection: "row", margin: 5, marginStart: 10 }}>
          <Image
            source={{ uri: item.image }}
            style={{ width: 60, height: 60, borderRadius: 30 }}
          />
          <Image
            source={Icons.tick}
            style={{
              height: 15,
              width: 15,
              position: "absolute",

              top: 0,
              end: -4
            }}
          />
        </View>
        <View style={{ flex: 1, marginStart: 10, marginEnd: 10 }}>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 14,
              color: Colors.black
            }}
          >
            {item.name}
          </Text>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 12,
              color: "#666666"
            }}
          >
            Badge Number : {item.badgeNumber}
          </Text>
        </View>
      </View>
    );
  }
  _renderArrow = direction => {
    if (direction === "left") {
      return (
        <Text
          style={{
            fontFamily: "AktivGrotesk-Regular",
            fontSize: RFPercentage(2),
            color: "#ADADAD"
          }}
        >
          {this.previousMonth}
        </Text>
      );
    } else {
      return (
        <Text
          style={{
            fontFamily: "AktivGrotesk-Regular",
            fontSize: RFPercentage(2),
            color: "#ADADAD"
          }}
        >
          {this.nextMonth}
        </Text>
      );
    }
  };

  handleAddEvent = () => {
    this.props.navigation.navigate("AddEventScreen");
  }

  getMarkedDates = () => {
    const { markedDates } = this.state;
    let marked = [...this.state.markedDates];
    let newData = {};
    marked.map((data, index) => {
     Object.assign(newData, data);
    })
    Object.assign(newData, {[this.state.selectedDay]: { selected: true }})
    return newData;
  }

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.getEventList();
      }
    );
  };

  handleCurrentPage = (page) => {
  }

  render() {
    return (
      <SafeAreaView style={EventScreenStyle.container}>
        <HeaderWithBack
          navigation={this.props.navigation}
          name={getLangValue(string.events,'en')}
          notify={false}
          isBackNeeded={false}
        />
        <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh} />} contentContainerStyle={{ paddingBottom: heightPercentageToDP(15) }} style={{marginBottom:5,marginTop:5}}>
        
          <View style={EventScreenStyle.container}>
            <Calendar
              // Initially visible month. Default = Date()
              // current={new Date()}
              markedDates={this.state.markedDates.length > 0 ? this.getMarkedDates() :  { [this.state.selectedDay]: { selected: true } }}
              // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
              // Handler which gets executed on day press. Default = undefined
              onDayPress={day => {
                this.currentMonth = new Date(day.dateString);
                this.previousMonth = moment(this.currentMonth).subtract(2, 'months').startOf('month').format('MMM YYYY');
                this.nextMonth = moment(this.currentMonth).add(2, 'months').endOf('month').format('MMM YYYY');
                this.setState({ selectedDay: day.dateString }, () => {
                  this.getEventList();
                });
              }}
              onVisibleMonthsChange={(month) => {
                this.currentMonth = new Date(month[0].dateString);
                this.month = month[0].month;
                this.year = month[0].year;
                this.getCalenderEvents();
              }}
              // Handler which gets executed on day long press. Default = undefined
              // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
              monthFormat={"MMM yyyy"}
              // Handler which gets executed when visible month changes in calendar. Default = undefined
              onMonthChange={(month) => {
              }}
              // Hide month navigation arrows. Default = false
              hideArrows={false}
              // Replace default arrows with custom ones (direction can be 'left' or 'right')

              // Do not show days of other months in month page. Default = false
              hideExtraDays={false}
              // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
              // day from another month that is visible in calendar page. Default = false
              disableMonthChange={true}
              // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
              firstDay={0}
              // Hide day names. Default = false
              hideDayNames={false}
              // Show week numbers to the left. Default = false
              showWeekNumbers={false}
              // Handler which gets executed when press arrow icon left. It receive a callback can go back month
               onPressArrowLeft={substractMonth => {substractMonth()
                this.previousMonth = moment(this.currentMonth).subtract(2, 'months').startOf('month').format('MMM YYYY');
                this.nextMonth = moment(this.currentMonth).endOf('month').format('MMM YYYY');
               }}
              // // Handler which gets executed when press arrow icon left. It receive a callback can go next month
              onPressArrowRight={addMonth => {addMonth()
                this.nextMonth = moment(this.currentMonth).add(2, 'months').endOf('month').format('MMM YYYY');
                this.previousMonth = moment(this.currentMonth).startOf('month').format('MMM YYYY');
              }}
              renderArrow={this._renderArrow}
              theme={{
                textDisabledColor: "#D1D1D1",
                dayTextColor: "#222222",
                textDayFontFamily: "AktivGrotesk-Regular",
                textMonthFontFamily: "AktivGrotesk-Regular",
                textDayHeaderFontFamily: "AktivGrotesk-Regular",
                selectedDayBackgroundColor: colors.greenText,
                monthTextColor: "#2640DF",
                todayTextColor: "#2640DF",
                selectedDayTextColor: "#FFFFFF",
                textDayFontSize: RFPercentage(2.5),
                textDayHeaderFontSize: RFPercentage(1.9),
                textMonthFontSize: RFPercentage(2.5),
              }}
            />
            <View style={{ marginTop: 10, flex:1 }}>
           <EventsPageAllEvents
              navigation={this.props.navigation}
              upcomingEvents={this.state.events}
              sectionDotColor={this.state.sectionDotColor}
              selectedDay={this.state.selectedDay}
              refreshing={this.state.refreshing}
              currentPage={this.handleCurrentPage}
            />
              {/* <EventListComponent
                navigation={this.props.navigation}
                upcomingEvents={this.state.upcomingEvents}
                sectionDotColor={this.state.sectionDotColor}
              /> */}
            </View>
         </View>
        </ScrollView>
        <View style={{
                position: "absolute", 
                bottom: hp(15),
                end: 10,
                width: 60,
                height: 60,
                elevation: 2,}}>
        <TouchableOpacity onPress={this.handleAddEvent}>
        <View>
          <Image
            source={Icons.plus_icon}
            style={{
              width: 50,
              height: 50,
            }}
          />
          </View>
        </TouchableOpacity>
        </View>
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        <CustomToast ref="errorEvents" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
        {!this.state.isConnected && <NoInternetConnection />}
           
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoadingAllEvent: state.Event.isLoadingAllEvent,
    allEventList: state.Event.allEventList,
    errorAllEvents: state.Event.errorAllEvents,
    isLoadingCalenderDates: state.Event.isLoadingCalenderDates,
    calenderDates: state.Event.calenderDates,
    errorCalenderDates: state.Event.errorCalenderDates,
    isListRefresh: state.Event.isListRefresh,
    isLoadingActionEvent: state.Event.isLoadingActionEvent,
    isLoadingAddNewEvent: state.Event.isLoadingAddNewEvent,
    addNewEvent: state.Event.addNewEvent
  };
}
export default connect(mapStateToProps, null)(Events);