import React, { PureComponent } from "react";
import { View, SafeAreaView, TouchableOpacity, ScrollView, BackHandler, Clipboard, ActivityIndicator, Linking } from "react-native";
import {  Text, Button, Image, Icon } from "react-native-elements";
import Colors from "../../Resource/Color/index";
import Icons from "../../Resource/index";
import SettingScreenStyle from "../../Resource/Style/SettingScreenStyle";
import string from "../../Resource/string";
import {getLangValue} from '../../Resource/string/language'; 
import { heightPercentageToDP } from "react-native-responsive-screen";
import HeaderWithBack from "../../Component/HeaderWithBack";
import AsyncStorage from "@react-native-community/async-storage";
import { StackActions } from '@react-navigation/native';
import { getUserProfile } from '../../Network/Service';
import { connect } from "react-redux";
import { UserProfile, UserProfileRequest, UserProfileFailed } from '../../Store/Actions/UserAction';
import * as Constants from '../../Resource/Constants';
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import NetInfo from "@react-native-community/netinfo";
import CustomToast from "../../Component/CustomToast";
import colors from "../../Resource/Color/index";
import NoInternetConnection from "../../Component/NoInternetConnection";
import { RFPercentage } from "react-native-responsive-fontsize";
import { showMessage } from "react-native-flash-message";

class SettingScreen extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      userName: "",
      email: "",
      userImage: '',
      isConnected: false,
      batch_id: '',
      loadingImg: true,
    };
    this.Token = '';
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    this._subscription && this._subscription();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isUserDataLoading !== this.props.isUserDataLoading) {
      this.setState({ loading: this.props.isUserDataLoading});
    }
    if (prevProps.updatedUser !== this.props.updatedUser) {
      this.getUserProfile();
    }
  }

  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    if (this.Token !== '') {
       this.getUserProfile();
    }
  }

  getUserProfile = async () => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
   }
    try {
      this.props.dispatch(UserProfileRequest());
      const res = await getUserProfile(this.Token);
      if (res) {
        const code = res.code;
        console.log('res', res);

        switch (code) {
          case 200:
              this.setState({ 
                userName: res.data.name, 
                email: res.data.email, 
                userImage: res.data.profile_pic,
                batch_id: res.data.batch_id,
              });
              this.props.dispatch(UserProfile(res));
            break;
          case 400:
            this.props.dispatch(UserProfileFailed(res));
            break;
          case 401:
            this.handleLogout();
          default:
            break;
        }
      }
    } catch(error) {
      this.props.dispatch(UserProfileFailed(error));
      throw error;
    }
  }

  doRedirect(screen) {
    this.props.navigation.navigate(screen);
  }
  doRedirectLink(title) {
    switch (title) {
      case 'Partners':
        Linking.openURL('http://dev.mysafetynet.info/partners')
        break;
      case 'Help and Feedback':
        Linking.openURL('http://dev.mysafetynet.info/helpAndFeedback')
        break;
      case 'About Us':
        Linking.openURL('http://dev.mysafetynet.info/about')
        break;
      case 'Contact Us':
        Linking.openURL('http://dev.mysafetynet.info/contact')
        break;
      case 'FAQs':
        Linking.openURL('http://dev.mysafetynet.info/faqs')
        break;
      case 'Privacy Policy':
        Linking.openURL('http://dev.mysafetynet.info/terms-conditions')
        break;
  
      default:
        break;
    }
  }
  
  handleLogout = () => {
    AsyncStorage.clear();
   this.doFinish('Login'); 
  }

  doFinish(screen) {
    this.props.navigation.navigate(screen);
    // this.props.navigation.dispatch(StackActions.popToTop());
  }

  handleCopyBadge = async () => {
    showMessage('copy to clipboard');
    Clipboard.setString(this.state.batch_id);
    const link = await Clipboard.getString();
  }
  handleLoading = () => {
    this.setState({
      loadingImg: false,
    });
}

  render() {
    return (
      <View style={SettingScreenStyle.container}>
        <HeaderWithBack
          navigation={this.props.navigation}
          name={getLangValue(string.settings,'en')}
          notify={false}
          isBackNeeded={false}
        />
        <ScrollView contentContainerStyle={{ paddingBottom: heightPercentageToDP(15) }} style={SettingScreenStyle.container}>
          <View style={SettingScreenStyle.container}>
            <View style={SettingScreenStyle.card}>
              <View
                style={{
                  flexDirection: "row",
                  backgroundColor: "#3344D6",
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4
                }}
              >
                <View style={{ alignItems: "center", marginTop: 15, marginStart: 20, marginBottom: 20}}>
                  <Image
                    source={this.state.userImage !== ''? {uri:this.state.userImage} : Icons.user}
                    style={{
                      width: 100,
                      height: 100,
                    }}
                    onLoadEnd={this.handleLoading}
                  />
                  <ActivityIndicator
                        style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        backgroundColor: colors.transparent,
                        }}
                        animating={this.state.loadingImg}
                    />
                </View>
                <View style={{ flex: 1, justifyContent: "center", marginTop: 15, marginStart: 20, marginBottom: 20 }}>
                  <Text
                    style={{
                      color: Colors.white,
                      fontFamily: "AktivGrotesk-Bold",
                      fontSize: RFPercentage(2.4),
                      textTransform:'capitalize',
                      marginBottom: 8,
                    }}
                  >
                    {this.state.userName}
                  </Text>
                  <Text
                    style={{
                      color: Colors.white,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: RFPercentage(2),
                      marginBottom: 2,
                    }}
                  >
                    {this.state.email}
                  </Text>
          {this.state.batch_id !== '' &&       <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text
                    style={{
                      color: Colors.white,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: RFPercentage(2),
                      marginRight: 10,
                      marginBottom: 2,
                      textAlign: 'center'
                    }}
                  >
                   Badge No: {this.state.batch_id}
                  </Text>
                  <TouchableOpacity onPress={this.handleCopyBadge}>
                  <View style={{flexDirection: 'row'}}>
                  <Image source={Icons.copy} style={{width: 20, height: 20, marginRight: 5}} />
                  </View>
                  </TouchableOpacity>
                 </View>}
                  <View style={{
                    backgroundColor: Colors.white,
                    borderRadius: 30,
                    width: 100,
                  }}>
                  <TouchableOpacity  onPress={() => this.doRedirect("EditProfile")}>
                    <Text style={{
                      color: "#3444D6",
                      fontSize: RFPercentage(1.8),
                      textAlign: 'center',
                      padding: 3
                    }}>Edit Profile</Text>
                  </TouchableOpacity>
                  </View>
                  {/* <Button
                    title="Edit Profile"
                    buttonStyle={{
                      backgroundColor: Colors.white,
                      borderRadius: 30
                    }}
                    containerStyle={{ width: 130, marginTop: 10 }}
                    titleStyle={{
                      color: "#3444D6",
                      fontSize: RFPercentage(1.8),
                    }}
                    
                    onPress={() => this.doRedirect("EditProfile")}
                  /> */}
                </View>
              </View>
              <TouchableOpacity
                onPress={() => this.doRedirect("IncomingEvents")}
              >
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                   Incoming Events 
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.doRedirect("ChangePassword")}
              >
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    {getLangValue(string.changePassword,'en')}
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("Partners")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    {getLangValue(string.partners,'en')}
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("Help and Feedback")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    {getLangValue(string.helpAndFeedback,'en')}
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("About Us")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    {getLangValue(string.aboutUs,'en')}
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("Contact Us")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    {getLangValue(string.contactUs,'en')}
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("FAQs")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    {getLangValue(string.FAQs,'en')}
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("Privacy Policy")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    marginBottom: 20,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    {getLangValue(string.privacyPolicy,'en')}
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
            </View>
            <View style={SettingScreenStyle.view_button}>
              <Button
                loading={this.state.loading}
                title="LOG OUT"
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20
                }}
                // titleStyle={{ fontFamily: "AktivGrotesk-Bold", fontSize: 16 }}
                textStyle={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Bold",
                }}
                onPress={() => this.handleLogout()}
              />
            </View>
          </View>
        </ScrollView>
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {!this.state.isConnected && <NoInternetConnection />}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.User.user,
    isUserDataLoading: state.User.isUserDataLoading,
    userError: state.User.userError,
    updatedUser: state.User.updatedUser,
  };
}

export default connect(mapStateToProps,null)(SettingScreen);
