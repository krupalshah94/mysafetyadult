import React, { Component } from "react";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Incoming from '../Connection/Incoming';
import Connected from '../Connection/Connected';
import { SafeAreaView, BackHandler } from "react-native";
import { RFPercentage } from "react-native-responsive-fontsize";
import { getLangValue } from "../../Resource/string/language";
import string from "../../Resource/string";
import HeaderWithBack from "../../Component/HeaderWithBack";
const Tab = createMaterialTopTabNavigator();
export default class Connections extends Component {
  
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }
  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  handleTabPress = () => {
   console.log('tab') 
  }
  render() {
    return (
      <SafeAreaView style={{flex:1}}>
       <HeaderWithBack
          navigation={this.props.navigation}
          name={getLangValue(string.connection,'en')}
          notify={false}
          isBackNeeded={false}
        />
      <Tab.Navigator
       tabBarOptions={{
        labelStyle: { fontFamily: "AktivGrotesk-Regular", 
                      fontSize: RFPercentage(2.5)},
      }}
      >
      <Tab.Screen name="Connected" component={Connected} />
      <Tab.Screen name="Incoming" component={Incoming} />
    </Tab.Navigator>
    </SafeAreaView>
    );
  }
}
