import React, { PureComponent } from "react";
import { View, SafeAreaView, TouchableOpacity, StyleSheet, BackHandler, Animated, Dimensions } from "react-native";
import { Text } from "react-native-elements";
import Colors from "../../Resource/Color/index";
import ConnectionListComponent from "../../Component/ConnectionListComponent";
import EventListComponent from "../../Component/EventListComponent";
import {getLangValue} from '../../Resource/string/language';  
import string from "../../Resource/string";
import HeaderWithBack from "../../Component/HeaderWithBack";
import { RFPercentage } from "react-native-responsive-fontsize";
import AsyncStorage from "@react-native-community/async-storage";
import * as Constants from '../../Resource/Constants';
import { connect } from "react-redux";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import NetInfo from "@react-native-community/netinfo";
import CustomToast from "../../Component/CustomToast";
import colors from "../../Resource/Color/index";
import NoInternetConnection from '../../Component/NoInternetConnection';
import { doSetNotificationData } from "../../Store/Actions/AuthAction";
import {CommonActions, StackActions} from '@react-navigation/native';

let {width, height} = Dimensions.get('window');
class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      myConnections: [],
      sectionDotColor: ["#4181DF"],
      pageNo: 1,
      perPage: 10,
      isConnected: false,
      backClickCount: 0,
      viewEmpty: false,
      viewEmptyConnection: false,
    };
    this.Token = '';
    this.springValue = new Animated.Value(100);
  }

  componentDidMount() {

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
    if(this.props.notification !== undefined) {
      console.log('props notification==>', this.props.notification);
      if (this.props.notification !== undefined) {
        const { notification } = this.props;
        let notificationData = JSON.parse(notification.extraPayLoad);
        console.log('parse',notificationData)
        switch (notificationData.target_screen) {
          case "user_profile":
            switch (notificationData.type) {
              case 'connection_request_received':
                console.log('calledd')
                this.props.navigation.navigate('Connections',{screen:'Incoming', refresh: true});
                break;
              case 'connection_request_action':
                this.props.navigation.navigate('Connections',{screen:'Connected'});
                break;
              default:
                break;
            }
            break;
          case "event_detail":
            switch (notificationData.type) {
              case 'event_invite_received':
                this.props.navigation.navigate('IncomingEvents')
                break;
              case 'event_invite_action':
                this.props.navigation.navigate('CompleteEventList')
                break;
              case 'before_meeting_15':
                const before_meeting_15 = {
                  event_id: notificationData.event_id
                }
                 this.props.navigation.navigate('EventDetail', {EventDetail: before_meeting_15})
                break;
              case 'after_meeting':
                const after_meeting = {
                  event_id: notificationData.event_id
                }
                this.props.navigation.navigate('EventDetail', {EventDetail: after_meeting})
                break;
              case 'event_not_complete':
                const event_not_complete = {
                  event_id: notificationData.event_id
                }
                this.props.navigation.navigate('EventDetail', {EventDetail: event_not_complete})
                break;
              case 'event_complete':
                const event_complete = {
                  event_id: notificationData.event_id
                }
                this.props.navigation.navigate('EventDetail', {EventDetail: event_complete})
                break;
              default:
                break;
            }

          default:
            break;
        }
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.getList !== this.props.getList) { 
      if (this.props.getList.data.incoming_events.length === 0) {
        this.setState({ viewEmpty: false}) 
     } else {
      this.setState({ viewEmpty: true}) 
     }  
    }
    if (prevProps.isLoadingGetList !== this.props.isLoadingGetList) {
      this.setState({ loading: this.props.isLoadingGetList});
    }
    if (prevProps.connectedUserList !== this.props.connectedUserList) {
      if (this.props.connectedUserList.data.data.length === 0) {
        this.setState({ viewEmptyConnection: false}) 
      } else {
        this.setState({ viewEmptyConnection: true });
      }
    }
    if(prevProps.notification !== this.props.notification) {
      if(this.props.notification !== undefined) {
        console.log('props notification==>', this.props.notification);
        if (this.props.notification !== undefined) {
          const { notification } = this.props;
          let notificationData = JSON.parse(notification.extraPayLoad);
          console.log('parse',notificationData)
          switch (notificationData.target_screen) {
            case "user_profile":
              switch (notificationData.type) {
                case 'connection_request_received':
                  console.log('calledd')
                  this.props.navigation.navigate('Connections',{screen:'Incoming', refresh: true});
                  break;
                case 'connection_request_action':
                  this.props.navigation.navigate('Connections',{screen:'Connected'});
                  break;
                default:
                  break;
              }
              break;
            case "event_detail":
              switch (notificationData.type) {
                case 'event_invite_received':
                  this.props.navigation.navigate('IncomingEvents')
                  break;
                case 'event_invite_action':
                  this.props.navigation.navigate('CompleteEventList')
                  break;
                case 'before_meeting_15':
                  const before_meeting_15 = {
                    event_id: notificationData.event_id
                  }
                   this.props.navigation.navigate('EventDetail', {EventDetail: before_meeting_15})
                  break;
                case 'after_meeting':
                  const after_meeting = {
                    event_id: notificationData.event_id
                  }
                  this.props.navigation.navigate('EventDetail', {EventDetail: after_meeting})
                  break;
                case 'event_not_complete':
                  const event_not_complete = {
                    event_id: notificationData.event_id
                  }
                  this.props.navigation.navigate('EventDetail', {EventDetail: event_not_complete})
                  break;
                case 'event_complete':
                  const event_complete = {
                    event_id: notificationData.event_id
                  }
                  this.props.navigation.navigate('EventDetail', {EventDetail: event_complete})
                  break;
                case 'after_meeting_24':
                  this.props.navigation.navigate('Notification');
                  break;
                default:
                  break;
              }
  
            default:
              break;
          }
        }
      }
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
    this._subscription && this._subscription();
  }

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  handleBackPress = () => {
    this.state.backClickCount == 1 ? BackHandler.exitApp() : this._spring();
    return true;
  }

  _spring() {
    this.setState({backClickCount: 1}, () => {
        Animated.sequence([
            Animated.spring(
                this.springValue,
                {
                    toValue: -.15 * height,
                    friction: 5,
                    duration: 300,
                    useNativeDriver: true,
                }
            ),
            Animated.timing(
                this.springValue,
                {
                    toValue: 100,
                    duration: 300,
                    useNativeDriver: true,
                }
            ),

        ]).start(() => {
            this.setState({backClickCount: 0});
        });
    });

}

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <HeaderWithBack
          navigation={this.props.navigation}
          name={getLangValue(string.home,'en')}
          notify={false}
          isBackNeeded={false}
        />
        <View style={styles.container}>
          <View style={styles.connectionView}>
            <Text style={styles.textView}>{getLangValue(string.myConnection,'en')}</Text>
            {this.state.viewEmptyConnection === true &&
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Connections')}>
              <Text style={styles.viewAll}>{getLangValue(string.viewAll,'en')}</Text>
            </TouchableOpacity>
            }
          </View>

          <ConnectionListComponent
            navigation={this.props.navigation}
            Connections={this.state.myConnections}
            refreshing={this.state.refreshing}
          />

          <View style={styles.connectionView}>
            <Text style={styles.textView}>{getLangValue(string.upcomingEvents,'en')}</Text>
         {this.state.viewEmpty === true &&
            <TouchableOpacity onPress={() => this.props.navigation.navigate('CompleteEventList')}>
              <Text style={styles.viewAll}>{getLangValue(string.viewAll,'en')}</Text>
            </TouchableOpacity>
         }
          </View>
          
          <EventListComponent
            navigation={this.props.navigation}
            upcomingEvents={this.state.upcomingEvents}
            sectionDotColor={this.state.sectionDotColor}
          />
        </View>
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {!this.state.isConnected && <NoInternetConnection />}
        <Animated.View style={[styles.animatedView, {transform: [{translateY: this.springValue}]}]}>
            <Text style={styles.exitTitleText}>press back again to exit the app</Text>

            <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => BackHandler.exitApp()}
            >
                <Text style={styles.exitText}>Exit</Text>
            </TouchableOpacity>

        </Animated.View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#DFE2F1" },
  connectionView: {
    flexDirection: "row",
    marginStart: "5%",
    marginEnd: "5%",
    marginTop: 20,
    marginBottom: 10
  },
  textView: {
    flex: 1,
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2.2),
    color: Colors.black
  },
  viewAll: {
    flex: 1,
    fontFamily: "AktivGrotesk-Regular",
    fontSize: RFPercentage(2.2),
    textAlign: "right",
    color: "#2640DF"
  },
  animatedView: {
    width,
    backgroundColor: "#0a5386",
    elevation: 2,
    position: "absolute",
    bottom: 0,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
},
exitTitleText: {
    textAlign: "center",
    color: "#ffffff",
    marginRight: 10,
},
exitText: {
    color: "#e5933a",
    paddingHorizontal: 10,
    paddingVertical: 3
}
});

function mapStateToProps(state) {
  return {
    isLoadingGetList: state.Event.isLoadingGetList,
    getList: state.Event.getList,
    getListError: state.Event.getListError,
    connectedUserList: state.Connection.connectedUserList,
    notification: state.Auth.notification,
  };
}

export default connect(mapStateToProps,null)(Home);