import React, { PureComponent } from "react";
import { View, SafeAreaView, BackHandler, Clipboard, Alert } from "react-native";
import { Text, Button } from "react-native-elements";
import Colors from "../../Resource/Color/index";
import BadgeScreenStyle from "../../Resource/Style/BadgeScreenStyle";
import { TextField } from "react-native-material-textfield";
import {getLangValue} from '../../Resource/string/language';  
import string from "../../Resource/string";
import HeaderWithBack from "../../Component/HeaderWithBack";
import {sendConnection} from '../../Network/Service';
import {sendConnectionRequest, sendConnectionSuccess, sendConnectionFailed} from '../../Store/Actions/ConnectionAction';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";
import * as Constants from '../../Resource/Constants';
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import colors from "../../Resource/Color/index";
import { connect } from "react-redux";
import CustomToast from "../../Component/CustomToast";
import NoInternetConnection from "../../Component/NoInternetConnection";
import { widthPercentageToDP } from "react-native-responsive-screen";
import { RFPercentage } from "react-native-responsive-fontsize";

class Badge extends PureComponent {
 
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isError: false,
      badgeNumber: "",
      errBadgeNumber: "",
      isConnected: false,
      canPaste: true,
    };
    this.Token = '';
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    this._subscription && this._subscription();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoadingSendConnection !== this.props.isLoadingSendConnection) {
      this.setState({ loading: this.props.isLoadingSendConnection})
    }
    if (prevProps.sendConnectionData !== this.props.sendConnectionData) {
      this.refs.connectionSuccess.ShowToastFunction(this.props.sendConnectionData.message);
      this.props.navigation.navigate('Connections');
    }
    if (prevProps.errorSendConnection !== this.props.errorSendConnection) {
      if (this.props.errorSendConnection.status === "false") {
        this.setState({isError: true})
        this.refs.connectionError.ShowToastFunction(this.props.errorSendConnection.message);
      }
   }
  }

  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }
  
  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
  }

  handleBadgeVerify = async () => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
   }
   if (this.state.isError === true) {
    this.setState({isError: false});
    return;
  } 
   if (this.state.badgeNumber.trim() === '') {
      this.setState({errBadgeNumber: 'Please enter badge number'});
      return;
    }
    
    try {
      this.props.dispatch(sendConnectionRequest());
      const res = await sendConnection(this.Token,{batch_id: this.state.badgeNumber});
      const code = res.code;
      switch (code) {
        case 200:
          this.props.dispatch(sendConnectionSuccess(res));
        break;
        case 400:
          this.props.dispatch(sendConnectionFailed(res));
        break;
        case 401:
          this.props.dispatch(sendConnectionFailed(res));
        break;
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(sendConnectionFailed(error));
    }
  }

  handleFocus = async() => {
    const copiedContent = await Clipboard.getString();
    if (copiedContent === '') return;
   
    const isPasted = copiedContent;
    if (isPasted){ 
      if(this.state.canPaste === true) {
        this.setState({canPaste: false}, () => {
          Alert.alert(
            "Paste",
            "Do you want to Paste from Clipboard",
            [
              {
                text: "Cancel",
                onPress: () => {
                  this.setState({canPaste: true})
                },
                style: "cancel"
              },
              { text: "Paste", onPress: () =>  {
                this.handleOnPaste(isPasted)
              } }
            ],
            { cancelable: false }
          );
        })
      }
    }
  }

  handleOnPaste = (content) => {
    this.badgeNumberRef.setValue(content)
    this.setState({badgeNumber: content, canPaste: true})
  }

  handleOnChangeText = async (content) => {
    this.setState({badgeNumber: content})
    if (content === '') return;
    const copiedContent = await Clipboard.getString();

    if (copiedContent === '') return;
    const isPasted = content.includes(copiedContent);
    if (isPasted) this.handleOnPaste(content);
  }

  render() {
    return (
      <SafeAreaView style={BadgeScreenStyle.container}>
       <HeaderWithBack navigation={this.props.navigation}  name={getLangValue(string.badge,'en')} notify={false} isBackNeeded={true}/> 
        <View style={BadgeScreenStyle.container}>
          <View style={BadgeScreenStyle.card} keyboardShouldPersistTaps={'handled'}>
            <TextField
              ref={ref => {
                this.badgeNumberRef = ref;
              }}
              value={this.state.badgeNumber}
              autoCorrect={false}
              // enablesReturnKeyAutomatically={true}
              onChangeText={this.handleOnChangeText}
              selectTextOnFocus={true}
              onFocus={this.handleFocus}
              // returnKeyType="next"
              label="BADGE NUMBER"
              error={this.state.errBadgeNumber}
              placeholderTextColor="#222222"
              textColor="#222222"
              underlineColorAndroid={Colors.transparent}
              tintColor="#222222"
              fontSize={25}
              containerStyle={{
                marginStart: 10,
                marginEnd: 10,
                display: this.state.isError ? "none" : "flex"
              }}
              
              inputContainerStyle={{
                borderBottomColor: "#C3C3C3",
                borderBottomWidth: 1
              }}
              onBlur={() => this.setState({ errBadgeNumber: "" })}
              labelTextStyle={{ fontFamily: "AktivGrotesk-Regular"  }}
              titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              affixTextStyle={{ fontFamily: "AktivGrotesk-Regular"}}
            />
    
            <View style={{ display: this.state.isError ? "flex" : "none" }}>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 30,
                  fontFamily: "AktivGrotesk-Regular",
                  color: "#E82D2E",
                  marginBottom: 20
                }}
              >
                ERROR
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 20,
                  fontFamily: "AktivGrotesk-Regular",
                  color: "#222222",
                  marginBottom: 20
                }}
              >
                Badge is not verified..
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Regular",
                  color: "#222222",
                  marginStart: 10,
                  marginEnd: 10,
                  marginBottom: 20
                }}
              >
                Please enter valid Badge number or ask to repeat badge number
              </Text>
            </View>
          </View>
          <View style={BadgeScreenStyle.view_button}>
            <Button
              loading={this.state.loading}
              title={this.state.isError ? "VERIFY AGAIN" : "VERIFY"}
              buttonStyle={{
                backgroundColor: "#3444D6",
                borderRadius: 20
              }}
              // titleStyle={{ fontFamily: "AktivGrotesk-Bold", fontSize: 16 }}
              textStyle={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Bold",
                }}
                onPress={this.handleBadgeVerify}
            />
          </View>
        </View>
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        <CustomToast ref="connectionSuccess" backgroundColor={colors.white} textColor={colors.greenText} position="bottom" />
        <CustomToast ref="connectionError" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {!this.state.isConnected && <NoInternetConnection />}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoadingSendConnection: state.Connection.isLoadingSendConnection,
    sendConnectionData: state.Connection.sendConnectionData,
    errorSendConnection: state.Connection.errorSendConnection
  };
}

export default connect(mapStateToProps,null)(Badge);