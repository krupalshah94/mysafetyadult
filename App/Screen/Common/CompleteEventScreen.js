import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Colors from "../../Resource/Color/index";
import {
  Text,
  Image,
  Button,
  Input
} from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import SelectImageModal from '../../Component/Modal/SelectImageModal';
import HeaderWithBack from "../../Component/HeaderWithBack";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";
import * as Constants from '../../Resource/Constants';
import CustomToast from "../../Component/CustomToast";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import colors from "../../Resource/Color/index";
import { connect } from "react-redux";
import { completeEventRequest, completeEventSuccess, completeEventFailed, eventCompleted } from "../../Store/Actions/EventAction";
import { completeEvents } from "../../Network/Service";

class completeEventscreen extends PureComponent {
 
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      isConnected: false,
      eventTitle: "Untitled Event",
      errEventTitle: "",
      eventStartDate: "Event Start Date",
      eventEndDate: "Event End Date",
      eventStartTime: "Event Start Time",
      eventEndTime: "Event End Time",
      user_id: '',
      user_name: '',
      created_by_name: '',
      eventId: '',
      eventLocation: '',
      eventNotes: '',
      invitedUsers: [
        {
          name: "Smith Clarrt",
          image:
            "http://ap.church/wp-content/uploads/2018/08/Brian-Jones-Profile.jpg"
        }
      ],
      showSelectModal: false,
    };
    this.Token = '';
    this.UserData = {};
  }

  componentDidMount() {
    const { route: { params: {eventStartDate, 
      eventEndDate, eventStartTime, 
      eventEndTime, user_id, 
      user_name, created_by_name,
      eventId, eventTitle, eventLocation, eventNotes
    }}} = this.props;
    this.setState({
      eventTitle: eventTitle,
      eventStartDate: eventStartDate,
      eventEndDate: eventEndDate,
      eventStartTime: eventStartTime,
      eventEndTime: eventEndTime,
      user_id: user_id,
      user_name: user_name,
      created_by_name: created_by_name,
      eventId: eventId,
      eventLocation: eventLocation,
      eventNotes: eventNotes
    })
    this.eventTitleRef.setValue(eventTitle);
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoadingCompleteEvent !== this.props.isLoadingCompleteEvent) {
      this.setState({ loading: this.props.isLoadingCompleteEvent });
    }
    if (prevProps.completeEvent !== this.props.completeEvent) {
      if (this.props.completeEvent.status === "true"){
        this.refs.completeEvent.ShowToastFunction(this.props.completeEvent.message);
        this.props.dispatch(eventCompleted(true));
        setTimeout(() => {
          this.props.navigation.navigate('Home', {completedEvent: true});
        }, 1000);
      } else {
      this.refs.completeEventError.ShowToastFunction(this.props.completeEvent.message);
      this.props.navigation.navigate('Home');
    }
  }
  if (prevProps.errorCompleteEvents !== this.props.errorCompleteEvents) {
    if (this.props.errorCompleteEvents.code === 400) {
      this.refs.completeEventError.ShowToastFunction(this.props.errorCompleteEvents.message);
    }
  }
}

  componentWillUnmount() {
    this._subscription && this._subscription();
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    this.UserData = await AsyncStorage.getItem(Constants.USER_DATA);
    this.UserData = JSON.parse(this.UserData);
  }

  doCompleteEvent(visible) {
  this.setState({showSelectModal: visible})
  }


  renderInvite(item, index) {
    return (
      <View
        style={{
          marginStart: 10,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image
          source={{ uri: item.image }}
          style={{ width: 100, height: 100, borderRadius: 4 }}
        />

        <Text
          style={{
            marginTop: 5,
            color: "#666666",
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 13,
            textAlign: "center"
          }}
        >
          {item.name}
        </Text>
      </View>
    );
  }

  handleSubmit = async (selectedImage) =>  {
    if (selectedImage === undefined) {
      this.refs.completeEventError.ShowToastFunction('Please select one safety image');
      return;
    }
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    try {
      this.props.dispatch(completeEventRequest());
      const data = {
        user_id: this.UserData.id,
        image_id: selectedImage,
      }
      const res = await completeEvents(this.Token,this.state.eventId, data);
      const code = res.code;
      switch (code) {
        case 200:
          this.props.dispatch(completeEventSuccess(res));
        break;
        case 400:
          this.props.dispatch(completeEventFailed(res));
        break;
        case 401:
          this.props.dispatch(completeEventFailed(res));
        break;
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(completeEventFailed(error));
    }
   
    // this.props.navigation.navigate('Home');
  }

  render() {
    const { showSelectModal } = this.state;
    return (
        <View style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
          <HeaderWithBack notify={false} name="EVENT COMPLETE" navigation={this.props.navigation}/>
          <View style={{ backgroundColor: "#DFE2F1", flex:1  }}>
            <View
              style={{
                backgroundColor: Colors.white,
                borderRadius: 4,
                elevation: 2,
                margin: "5%",
              }}
            >
              <TextField
                ref={ref => {
                  this.eventTitleRef = ref;
                }}
                value={this.state.eventTitle}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventTitle: text });
                }}
                editable={false}
                returnKeyType="next"
                label="EVENT TITLE"
                error={this.state.errEventTitle}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={25}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10
                }}
                inputContainerStyle={{
                  borderBottomColor: Colors.transparent,
                  borderBottomWidth: 0
                }}
                onBlur={() => this.setState({ errEventTitle: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginBottom: 10
                }}
              >
                {this.state.eventTitle} with <Text style={{ color: "#3444D6" }}> {this.state.user_name}</Text>
              </Text>
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginBottom: 10
                }}
              >
                EVENT TIME
              </Text>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <TouchableOpacity
                  disabled={true}
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black
                    }}
                  >
                    {this.state.eventStartDate}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={true}
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black,
                      textAlign: "right"
                    }}
                  >
                    {this.state.eventStartTime}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                  disabled={true}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black
                    }}
                  >
                    {this.state.eventEndDate}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={true}
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black,
                      textAlign: "right"
                    }}
                  >
                    {this.state.eventEndTime}
                  </Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  height: 1,
                  backgroundColor: "#CCCCCC",
                  marginStart: 10,
                  marginEnd: 10,
                  marginTop: 20,
                  marginBottom: 20
                }}
              />
              <Button
                loading={this.state.loading}
                title="VIEW MORE"
                onPress={()=>this.props.navigation.goBack(null)}
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20,
                  alignItems: "center"
                }}
                // titleStyle={{
                //   fontFamily: "AktivGrotesk-Bold",
                //   fontSize: 16
                // }}
                textStyle={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Bold",
                }}
                containerStyle={{
                  width: 180,
                  alignSelf: "center",
                  marginBottom: 20
                }}
              />
            </View>
            <View
              style={{
                justifyContent: 'flex-end',
                flex:1,
                margin: 10,
              }}
            >
              <Button
                loading={this.state.loading}
                title="COMPLETE YOUR EVENT"
                onPress={()=>this.doCompleteEvent(true)}
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20,
                }}
                textStyle={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Bold",
                }}
              />
            </View>
          </View>
          <SelectImageModal showModal={showSelectModal} handleSubmit={this.handleSubmit} closeModal={() => this.doCompleteEvent(false)} />
          <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
          <CustomToast ref="completeEvent" backgroundColor={colors.white} textColor={colors.greenText} position="bottom" />
          <CustomToast ref="completeEventError" backgroundColor={colors.white} textColor={colors.red} position="bottom" />

          {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
        </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoadingEventDetails: state.Event.isLoadingEventDetails,
    eventDetails: state.Event.eventDetails,
    errorEventDetails: state.Event.errorEventDetails,
    isLoadingCompleteEvent: state.Event.isLoadingCompleteEvent,
    completeEvent: state.Event.completeEvent,
    errorCompleteEvents: state.Event.errorCompleteEvents,
  };
}
export default connect(mapStateToProps, null)(completeEventscreen);