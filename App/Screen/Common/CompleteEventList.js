import React, { Component } from "react";
import { View } from "react-native";
import CompletedEventListComponent from "../../Component/CompletedEventListComponent";
import colors from "../../Resource/Color";
import HeaderWithBack from "../../Component/HeaderWithBack";
import string from "../../Resource/string";
import { getLangValue  } from '../../Resource/string/language';

export default class CompleteEventList extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.bg_color }}>
        <HeaderWithBack notify={false} name={getLangValue(string.completedEvent,'en')} navigation={this.props.navigation}/>
        <CompletedEventListComponent
          navigation={this.props.navigation}
        />
      </View>
    );
  }
}
