import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  FlatList,
  BackHandler
} from "react-native";
import Colors from "../../Resource/Color/index";
import { Text, Image, Button, Input } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import moment from "moment";
import HeaderWithBack from "../../Component/HeaderWithBack";
import Icons from '../../Resource/index';
import { RFPercentage } from "react-native-responsive-fontsize";
import colors from "../../Resource/Color/index";
import { getLangValue } from "../../Resource/string/language";
import string from "../../Resource/string";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";
import * as Constants from '../../Resource/Constants';
import {getEventDetailsRequest, getEventDetailsSuccess, getEventDetailsFailed} from '../../Store/Actions/EventAction';
import { getEventView } from '../../Network/Service';
import { connect } from "react-redux";
import CustomToast from "../../Component/CustomToast";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import { StackActions } from "react-navigation";
class EventDetailScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      isConnected: false,
      eventId: undefined,
      user_id: undefined,
      user_name: "",
      user_batch: '',
      user_profile_image: '',
      user_location: '',
      user_connection_time: '',
      created_by_name: "",
      status: '',
      mark_complete: false,
      eventTitle: "New Event",
      errEventTitle: "",
      eventLocation: 'North capitol st nw',
      eventNotes: 'Notes are listed here',
      eventStartDate: moment(new Date()).format("ddd D MMM, YYYY"),
      eventEndDate: moment(new Date()).format("ddd D MMM, YYYY"),
      eventStartTime: moment(new Date()).format("hh:mm A"),
      eventEndTime: moment(new Date()).format("hh:mm A"),
      event_created_date: '',
      invitedUsers: [
        {
          name: "Smith Clarrt",
          image:
            "http://ap.church/wp-content/uploads/2018/08/Brian-Jones-Profile.jpg",
          badge:'465554',
          time: '3'
      }
      ]
    };
    this.Token = '';
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress)
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.setState({ eventId: this.props.route.params.EventDetail.event_id,
      mark_complete: this.props.route.params.EventDetail.mark_complete})
    this.getToken();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoadingEventDetails !== this.props.isLoadingEventDetails) {
      this.setState({loading: this.props.isLoadingEventDetails});
    }
    if(this.props.route.params.EventDetail !== prevProps.route.params.EventDetail) {
      if (this.props.route.params.EventDetail) {
        console.log('Event', this.props.route.params.EventDetail)
        this.setState({ eventId: this.props.route.params.EventDetail.event_id,
        }, () => {
          // this.getEventDetails();
        })
      }
    }
    if (prevProps.eventDetails !== this.props.eventDetails) {
    const { eventDetails} = this.props;
    this.eventTitleRef.setValue(eventDetails.data.title),
    this.eventLocationRef.setValue(eventDetails.data.address)
    this.setState({
      eventTitle: eventDetails.data.title,
      eventLocation: eventDetails.data.address,
      eventNotes: eventDetails.data.description,
      eventStartDate:  moment(eventDetails.data.start_date).format("ddd D MMM, YYYY"),
      eventEndDate: moment(eventDetails.data.end_date).format("ddd D MMM, YYYY"),
      eventStartTime: eventDetails.data.start_time,
      eventEndTime: eventDetails.data.end_time,
      user_id: eventDetails.data.user_id,
      user_name: eventDetails.data.show_user.name,
      created_by_name: eventDetails.data.created_by_name,
      status: eventDetails.data.status,
      user_batch: eventDetails.data.show_user.batch_id,
      user_profile_image: eventDetails.data.show_user.profile_pic,
      user_location: eventDetails.data.show_user.city,
      user_connection_time: eventDetails.data.user_connection_time,
      mark_complete: eventDetails.data.mark_complete,
      event_created_date: eventDetails.data.user_connection_time,
    })

    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
    this._subscription && this._subscription();
  }

  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    if (this.Token !== '') {
      this.getEventDetails();
    }
  }

  getEventDetails = async () => {
    const { eventId } = this.state;
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    if (eventId !== undefined) {
      try {
        this.setState({ loading: true })
          this.props.dispatch(getEventDetailsRequest());
          const res = await getEventView(this.Token, eventId);
          const code = res.code;
          switch (code) {
            case 200:
              this.setState({ loading: false })
              this.props.dispatch(getEventDetailsSuccess(res));
              break;
            case 400:
              this.setState({ loading: false })
              this.props.dispatch(getEventDetailsFailed(res));
              this.refs.errorEvents.ShowToastFunction(res.message);
              break;
            case 401:
              this.setState({ loading: false })
              this.props.dispatch(getEventDetailsFailed(res));
              this.refs.errorEvents.ShowToastFunction(res.message);
              // this.handleLogout();
              break;
            default:
              break;
          }
      } catch (error) {
        this.setState({ loading: false })
        this.props.dispatch(getEventDetailsFailed(error));
      }
    }
  }

  handleLogout = () => {
    AsyncStorage.clear();
    this.doFinish('Login');
  }

  doFinish(screen) {
    // this.props.navigation.dispatch(StackActions.popToTop());
  }


  doCompleteEvent() {
    const { navigate } = this.props.navigation;
    navigate("CompleteEvent", {
      eventStartDate:  this.state.eventStartDate,
      eventEndDate: this.state.eventEndDate,
      eventStartTime: this.state.eventStartTime,
      eventEndTime: this.state.eventEndTime,
      user_id: this.state.user_id,
      user_name: this.state.user_name,
      created_by_name: this.state.created_by_name,
      eventId: this.state.eventId,
      eventTitle: this.state.eventTitle,
      eventLocation: this.state.eventLocation,
      eventNotes: this.state.eventNotes,
      mark_complete: this.state.mark_complete,
    });
  }

  renderInvite(item, index) {
    return (
      <View
        style={{
          marginStart: 10,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
      <View style={{flexDirection: 'row'}}>
        <Image
          source={{ uri: item.image }}
          style={{ width: 100, height: 100, borderRadius: 4 }}
        />
        <View style={{marginStart: 10, marginEnd: 10}}>
        <Text
          style={{
            color: colors.inputTextColor,
            fontFamily: "AktivGrotesk-Regular",
            fontSize: RFPercentage(2.5),
            paddingBottom: 5,

          }}
        >
          {item.name}
        </Text>
        <Text
          style={{
            color: colors.textColorLight,
            fontFamily: "AktivGrotesk-Regular",
            fontSize: RFPercentage(2),
            paddingBottom: 5,
          }}
        >
          Badge Number: #{item.badge}
        </Text>
        <Text
          style={{
            color: colors.textColorLight,
            fontFamily: "AktivGrotesk-Regular",
            fontSize: RFPercentage(2),
            paddingBottom: 5,
          }}
        >
          Since {item.time} days ago
        </Text>
        </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
      <HeaderWithBack notify={false} name="EVENT DETAILS" navigation={this.props.navigation}/>
        <ScrollView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
          <View style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
            <View
              style={{
                backgroundColor: Colors.white,
                borderRadius: 4,
                elevation: 2,
                margin: "5%"
              }}
            >
              <TextField
                ref={ref => {
                  this.eventTitleRef = ref;
                }}
                value={this.state.eventTitle}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventTitle: text });
                }}
                editable={false}
                returnKeyType="next"
                label="EVENT TITLE"
                error={this.state.errEventTitle}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={RFPercentage(2.5)}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10
                }}
                inputContainerStyle={{
                  borderBottomColor: Colors.transparent,
                  borderBottomWidth: 0
                }}
                onBlur={() => this.setState({ errEventTitle: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
          
                <View style={{ flexDirection: "row", marginBottom: 10, marginTop: 20 }}>
                <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1, flexDirection: 'row' }}
                 disabled={true}
                >
                <Image style={{width: 18, height: 18}} source={Icons.calender}/>
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: RFPercentage(2),
                      color: Colors.black,
                      textAlign: 'center',
                      padding: 4,
                      marginStart: 5
                    }}
                  >
                    {this.state.eventStartDate}
                  </Text>
                </TouchableOpacity>
                 <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1, flexDirection: 'row' }}
                  disabled={true}
                >
                 <Image style={{width: 18, height: 18}} source={Icons.calender}/>
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: RFPercentage(2),
                      color: Colors.black,
                      textAlign: "right",
                      padding: 4,
                      marginStart:5
                    }}
                  >
                    {this.state.eventEndDate}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
              <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 , flexDirection: 'row'}}
                  disabled={true}
                >
                  <Image style={{width: 18, height: 18}} source={Icons.clock}/>
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: RFPercentage(2),
                      color: Colors.black,
                      padding: 4,
                      marginStart: 5,
                    }}
                  >
                    {this.state.eventStartTime}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1, flexDirection: 'row' }}
                  disabled={true}
                >
                   <Image style={{width: 18, height: 18}} source={Icons.clock}/>
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: RFPercentage(2),
                      color: Colors.black,
                      textAlign: "right",
                      padding: 4,
                      marginStart: 5
                    }}
                  >
                    {this.state.eventEndTime}
                  </Text>
                </TouchableOpacity>
              </View>
              <TextField
                ref={ref => {
                  this.eventLocationRef = ref;
                }}
                value={this.state.eventLocation}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventLocation: text });
                }}
                editable={false}
                returnKeyType="next"
                label="EVENT LOCATION"
                error={this.state.errEventLocation}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={RFPercentage(2.5)}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10
                }}
                inputContainerStyle={{
                  borderBottomColor: "#DFDFDF",
                  borderBottomWidth: 1
                }}
                onBlur={() => this.setState({ errEventLocation: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
              {/* <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginTop: 30,
                  marginBottom: 10
                }}
              >
                INVITES
              </Text> */}
              <View style={{marginTop: 20}}>
                  <View
            style={{
              marginStart: 10,
              // justifyContent: "center",
              // alignItems: "center"
            }}
          >
          <View style={{flexDirection: 'row'}}>
            <Image
              source={this.state.user_profile_image !== ''? {uri:this.state.user_profile_image} : Icons.inviteUser}
              style={{ width: 100, height: 100, borderRadius: 4 }}
            />
            <View style={{marginStart: 10, marginEnd: 10}}>
            <Text
              style={{
                color: colors.inputTextColor,
                fontFamily: "AktivGrotesk-Regular",
                fontSize: RFPercentage(2.5),
                paddingBottom: 5,

              }}
            >
              {this.state.user_name}
            </Text>
            <Text
              style={{
                color: colors.textColorLight,
                fontFamily: "AktivGrotesk-Regular",
                fontSize: RFPercentage(2),
                paddingBottom: 5,
              }}
            >
              Badge Number: {this.state.user_batch}
            </Text>
            <Text
              style={{
                color: colors.textColorLight,
                fontFamily: "AktivGrotesk-Regular",
                fontSize: RFPercentage(2),
                paddingBottom: 5,
              }}
            >
              Since {moment(this.state.event_created_date).fromNow()}
            </Text>
            <Text
              style={{
                color: colors.textColorLight,
                fontFamily: "AktivGrotesk-Regular",
                fontSize: RFPercentage(2),
                paddingBottom: 5,
              }}
            >
              Created By: {this.state.created_by_name}
            </Text>
            </View>
            </View>
      </View>
                {/* <FlatList
                  horizontal={true}
                  data={this.state.invitedUsers}
                  renderItem={({ item, index }) =>
                    this.renderInvite(item, index)
                  }
                  keyExtractor={(item, index) => index.toString()}
                  refreshing={this.state.refreshing}
                  style={{ marginEnd: 10, marginBottom: 20 }}
                /> */}
              </View>
       {/* <TextField
                ref={ref => {
                  this.eventNotesRef = ref;
                }}
                value={this.state.eventNotes}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventNotes: text });
                }}
                returnKeyType="next"
                label="NOTES"
                error={this.state.errEventNotes}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={13}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10,
                  marginBottom: 20
                }}
                inputContainerStyle={{
                  borderBottomColor: "#DFDFDF",
                  borderBottomWidth: 1
                }}
                onBlur={() => this.setState({ errEventNotes: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              /> */}
              <View style={{
                  marginStart: 10,
                  marginEnd: 10,
                  marginBottom: 20,
                  marginTop: 20
                }}>
                  <Text style={{ marginBottom: 10, color: colors.textColorLight, fontSize: RFPercentage(2) }}>{getLangValue(string.notes, 'en')}</Text>
                  <View
                    style={{
                      borderRadius: 2,
                      borderColor: colors.itemSeparator,
                      borderWidth: 1,
                      minHeight: 80,
                    }}
                  >
                  <Text style={{fontFamily: "AktivGrotesk-Regular",
                        fontSize: RFPercentage(2.5),
                        color: Colors.inputTextColor, padding: 5}}>{this.state.eventNotes}</Text>
                    {/* <Input
                      ref={ref => {
                        this.eventNotesRef = ref;
                      }}
                      returnKeyType="done"
                      value={this.state.eventNotes}
                      onChangeText={text => this.setState({ eventNotes: text })}
                      inputStyle={{
                        fontFamily: "AktivGrotesk-Regular",
                        fontSize: RFPercentage(2.5),
                        color: Colors.inputTextColor
                      }}
                      placeholderTextColor={Colors.inputTextColor}
                      multiline={true}
                      numberOfLines={10}
                      underlineColorAndroid={colors.transparent}
                      inputContainerStyle={{ padding: 0, borderBottomWidth: 0}}
                      containerStyle={{color: Colors.inputTextColor}}
                      disabled={true}
                    /> */}
                  </View>
                    {this.state.mark_complete && 
                    <View style={{marginTop: 10}}>
                      <Text style={{color: colors.black, fontSize: RFPercentage(2)}}>* You have marked your event completed.</Text>
                    </View>}
                </View>
            </View>
            {this.state.status === 'scheduled' && this.state.mark_complete === false &&
            <View
              style={{
                margin: 15
              }}
            >
              <Button
                loading={this.state.loading}
                onPress={() => this.doCompleteEvent()}
                title="COMPLETE YOUR EVENT"
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20,
                  alignItems: "center"
                }}
                textStyle={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Bold",
                }}
              />
            </View>
            }
          </View>
            
        </ScrollView>
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        <CustomToast ref="errorEvents" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoadingEventDetails: state.Event.isLoadingEventDetails,
    eventDetails: state.Event.eventDetails,
    errorEventDetails: state.Event.errorEventDetails
  };
}
export default connect(mapStateToProps, null)(EventDetailScreen);