import React, { Component } from "react";
import { View, BackHandler, StyleSheet, ActivityIndicator, FlatList, TouchableOpacity, Text } from "react-native";
import NotificationListComponent from "../../Component/NotificationListComponent";
import colors from "../../Resource/Color";
import HeaderWithBack from "../../Component/HeaderWithBack";
import string from "../../Resource/string";
import { getLangValue  } from '../../Resource/string/language';
import { getNotificationRequest, getNotificationSuccess, getNotificationFail} from '../../Store/Actions/AuthAction';
import { getNotification } from '../../Network/Service';
import AsyncStorage from "@react-native-community/async-storage";
import * as Constants from '../../Resource/Constants';
import NetInfo from "@react-native-community/netinfo";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import CustomToast from "../../Component/CustomToast";
import { connect } from "react-redux";
import { RFPercentage } from "react-native-responsive-fontsize";
import { heightPercentageToDP } from "react-native-responsive-screen";
import { readNotification } from '../../Network/Service';
class NotificationList extends Component {
  static navigationOptions = {
    title: "Notification"
  };
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      loading: false,
      page:1,
      lastPage:1,
      viewEmpty: false,
      refreshing: false,
      sectionDotColor: ["#4181DF"],
      Notification: []
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    this._subscription && this._subscription();
  }

  componentDidUpdate(prevProps) {
    if(prevProps.isNotificationListLoading !== this.props.isNotificationListLoading) {
      this.setState({ loading: this.props.isNotificationListLoading });
    }
    if(prevProps.notificationListDetails !== this.props.notificationListDetails) {
      console.log('notification', this.props.notificationListDetails);
      let notificationData = [];
      this.props.notificationListDetails.data.data.length > 0 && this.props.notificationListDetails.data.data.map((item) => {
        if(item.read_flag === 0) {
          notificationData.push(item);
        }
      })
      this.setState({Notification: [...this.state.Notification,...notificationData], last_page: this.props.notificationListDetails.data.last_page}, () => {
        if (this.state.Notification.length === 0) {
          this.setState({ viewEmpty: true})
        } else {
          this.setState({ viewEmpty: false})
        }
      })
    }
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    if (this.Token !== '') {
      this.getNotificationListing();
    }
  }

  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  getNotificationListing = async () => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    try {
      this.props.dispatch(getNotificationRequest());
      const res = await getNotification(this.Token, this.state.page)
      const code = res.code;
      switch (code) {
        case 200:
          this.props.dispatch(getNotificationSuccess(res));
          break;
        case 400:
          this.props.dispatch(getNotificationFail(res));
          break;
        case 401:
          this.props.dispatch(getNotificationFail(res));
          break;
            
        default:
          break;
      }
    } catch(error) {
      this.props.dispatch(getNotificationFail(res));
    }
  }

  goNotification = async (item) => {
     const eventId = JSON.parse(item.data)
    const userData = {
      notification_id: item.id
    };
     try {
      const res = await readNotification(this.Token, userData);
      const code = res.code;
      switch (code) {
        case 200:
          this.setState({Notification: [], page: 1}, () => {
            this.getNotificationListing();
          });
  
          break;
        case 400:
        
        break;
        case 401:
          
        break;
            
        default:
          break;
      }
    } catch(error) {

    }
      switch (item.type) {
        case "event_invite_action":
          const Data = {
            event_id: item.data_ob.event_id
          }
          this.props.navigation.navigate('EventDetail', {EventDetail: Data})
        break;
        case "event_invite_received":
          const dataReceived = {
            eventId: eventId.event_id
          }
          this.props.navigation.navigate('IncomingEvents')
        break;
        case "connection_request_action":
          this.props.navigation.navigate('Connections',{screen:'Connected'});
        break;
        case "connection_request_received":
          this.props.navigation.navigate('Connections',{screen:'Incoming'});
        break;
        case 'before_meeting_15':
          const before_meeting_15 = {
            event_id: item.data_ob.event_id
          }
           this.props.navigation.navigate('EventDetail', {EventDetail: before_meeting_15})
          break;
        case 'after_meeting':
          const after_meeting = {
            event_id: item.data_ob.event_id
          }
          this.props.navigation.navigate('EventDetail', {EventDetail: after_meeting})
          break;
        case 'after_meeting_24':
          const after_meeting_24 = {
            after_meeting_24: item.data_ob.event_id
          }
          this.props.navigation.navigate('EventDetail', {EventDetail: after_meeting_24});
          break;
        default:
        break;
      }
  }

  _renderItem(item, index) {
    if (item.read_flag === 0) {
    return (
      <View>
        <TouchableOpacity
          style={style.notificationView}
          onPress={() => this.goNotification(item)}
        >
          <View
            style={[
              style.dotView,
              {
                backgroundColor: this.state.sectionDotColor[
                  Math.floor(Math.random() * this.state.sectionDotColor.length)
                ]
              }
            ]}
          />
          <View style={{ flex: 1 }}>
            <Text style={style.notificationText}>{item.message}</Text>
            {item.event.date &&
            <View>
            <Text style={style.notificationTimeText}>{item.event.date}</Text>
            <Text style={style.notificationTimeText}>{item.event.time}</Text>
            </View>
            }
          </View>
        </TouchableOpacity>
        <View style={style.underline} />
      </View>
    );
    } else {
      return null
    }
  }
  renderOnRefresh() {
    this.setState({page: 1, refreshing: false, Notification: []}, () => {
       this.getNotificationListing();
    });
  }

  handleLoadMore() {
    if (this.state.page < this.state.lastPage) {
      this.setState(
        (prevState, nextProps) => ({
          page: prevState.page + 1,
        }),
        () => {
           this.getNotificationListing();
        },
      );
    }
  }

  renderEmpty = () => {
    return <View style={{justifyContent: 'center', alignItems: 'center'}}>
    <Text style={style.noNotificationText}>No Notifications!</Text>
    </View>
  }


  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.bg_color }}>
        <HeaderWithBack notify={false} name={getLangValue(string.notification,'en')} navigation={this.props.navigation}/>
        <View elevation={5} style={style.container}>
        <ActivityIndicator
          size="large"
          style={{
            display: this.state.refreshing ? "flex" : "none",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
        />
        <FlatList
          onRefresh={() => this.renderOnRefresh()}
          data={this.state.Notification.reduce(
                (x, y) =>
                  x.findIndex(e => e.id == y.id) < 0
                    ? [...x, y]
                    : x,
                [],
              )}
          contentContainerStyle={{ paddingBottom: heightPercentageToDP(15) }}
          renderItem={({ item, index }) => 
            this._renderItem(item, index)
          }
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.state.refreshing}
          style={{
            display: this.state.refreshing ? "none" : "flex"
          }}
          onEndReached={() => this.handleLoadMore()}
          onEndReachedThreshold={15}
          ListEmptyComponent={
            this.state.viewEmpty ? this.renderEmpty : null
          }
        />
       {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
      </View>
        
          <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isNotificationListLoading: state.Auth.isNotificationListLoading,
    notificationListDetails: state.Auth.notificationListDetails,
    notificationListError: state.Auth.notificationListError,
  };
}

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
    borderRadius: 5,
    margin:"5%"
  },
  underline: {
    width: "90%",
    height: 0.8,
    backgroundColor: colors.bg_color,
    alignSelf: "center",
    marginStart: 15,
    marginTop: 10,
    marginBottom: 10
  },
  notificationView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    marginStart: 5,
    marginEnd: 10,
  },
  dotView: { width: 15, height: 15, borderRadius: 7.5, marginLeft: 10, marginRight: 10, marginTop: 5},
  notificationText: {
    fontSize: RFPercentage(2.2),
    fontFamily: "AktivGrotesk-Regular", 
    color: colors.black,
  },
  notificationLocationText: {
    fontFamily: "AktivGrotesk-Regular", 
    fontSize: RFPercentage(2.2),
    color: colors.textColorDark,
  },
  notificationTimeText: {
    paddingTop: 5,
    fontFamily: "AktivGrotesk-Regular", 
    fontSize: RFPercentage(1.8),
    color: colors.textColorDark,
  },
  noNotificationText: {
    fontSize: RFPercentage(3),
    fontFamily: "AktivGrotesk-Regular", 
    color: colors.black,
    marginTop: 20
  },
  name: {
    fontSize: RFPercentage(2.2),
    fontFamily: "AktivGrotesk-Regular", 
    color: colors.blue,
    paddingBottom: 10,
  }
});


export default connect(mapStateToProps,null)(NotificationList);