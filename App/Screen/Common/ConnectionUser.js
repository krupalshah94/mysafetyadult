import React, { Component } from "react";
import {
  SafeAreaView,
  Image,
  View,
  StyleSheet,
  Text,
  BackHandler,
  ActivityIndicator
} from "react-native";
import colors from "../../Resource/Color";
import images from "../../Resource/index";
import HeaderWithBack from "../../Component/HeaderWithBack";
import { getLangValue } from "../../Resource/string/language";
import string from "../../Resource/string";
import { RFPercentage } from "react-native-responsive-fontsize";
import { connect } from "react-redux";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import UserEventListComponent from "../../Component/UserEventListComponent";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";
import * as Constants from '../../Resource/Constants';
import { getConnectedUserProfile } from '../../Network/Service';
import { getConnectedUserProfileRequest, getConnectedUserProfileSuccess, getConnectedUserProfileFailed} from '../../Store/Actions/ConnectionAction';
import CustomToast from "../../Component/CustomToast";
class ConnectionUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isConnected: false,
      userId: undefined,
      userData: {},
      sectionDotColor: ["#4181DF"],
      loadingImg: true,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this.setState({ userId: this.props.route.params.userData});
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoadingUserEvent !== this.props.isLoadingUserEvent) {
      this.setState({ loading: this.props.isLoadingUserEvent});
    }
    if (prevProps.isLoadingConnectedUserProfile !== this.props.isLoadingConnectedUserProfile) {
      this.setState({ loading: this.props.isLoadingConnectedUserProfile});
    }
    if (prevProps.connectedUserProfile !== this.props.connectedUserProfile) {
      this.setState({ userData : this.props.connectedUserProfile.data});
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    this._subscription && this._subscription();
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    if (this.Token !== '') {
      this.getConnectedUser();
    }
  }

  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  getConnectedUser = async () => {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
      return;
    }
    if (this.state.userId !== undefined) {
      try {
        this.props.dispatch(getConnectedUserProfileRequest());
        const res = await getConnectedUserProfile(this.Token, this.state.userId)
        const code = res.code;
        switch (code) {
          case 200:
            this.props.dispatch(getConnectedUserProfileSuccess(res));
            break;
          case 400:
            this.props.dispatch(getConnectedUserProfileFailed(res));
            break;
          case 401:
            this.props.dispatch(getConnectedUserProfileFailed(res));
            break;
              
          default:
            break;
        }
      } catch(error) {
        this.props.dispatch(getConnectedUserProfileFailed(res));
      }
    }
  }

  handleLoading = () => {
    this.setState({
      loadingImg: false,
    });
  }

  render() {
    const { userData } = this.state;
    return (
      <SafeAreaView style={customStyles.container}>
      <HeaderWithBack notify={false} name="CONNECTION" navigation={this.props.navigation}/>
        {/* <ScrollView> */}
          <View style={customStyles.topView}>
            <View style={customStyles.cart}>
              <View style={customStyles.userView}>
                <View style={customStyles.imageView}>
                  <Image
                    source={
                      userData && userData.profile_pic === ""
                        ? images.inviteUser
                        : { uri: userData && userData.profile_pic }
                    }
                    style={customStyles.UserImage}
                    onLoadEnd={this.handleLoading}
                  />
                  <Image source={images.tick} style={customStyles.tickImage} />
                </View>
                {this.state.loadingImg &&
                  <ActivityIndicator
                    style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    backgroundColor: colors.transparent,
                    }}
                    animating={this.state.loadingImg}
                />
                  }
              </View>
              <View style={customStyles.detailView}>
                <Text style={customStyles.userName}>{ userData && userData.name}</Text>
                <Text style={customStyles.userBadge}>
                  {getLangValue(string.badgeNumber, 'en')}: { userData && userData.batch_id}
                </Text>
                <View style={customStyles.location}>
                  <Text style={[customStyles.address, { textAlign: "right" }]}>
                    {userData&& userData.city}
                  </Text>
                  <View style={customStyles.line} />
                  <Text style={[customStyles.address, { textAlign: "left" }]}>
                    {userData && userData.state}
                  </Text>
                </View>
              </View>
            </View>
            <Text style={customStyles.SCHEDULED}>SCHEDULED EVENTS</Text>
            <UserEventListComponent
            userId={userData && userData.id}
            navigation={this.props.navigation}
            // upcomingEvents={this.state.upcomingEvents}
            sectionDotColor={this.state.sectionDotColor}
          />
          </View>
        {/* </ScrollView> */}
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
      </SafeAreaView>
    );
  }
}
const customStyles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.bg_color },
  topView: { marginTop: 90, marginBottom: 0, flex:1 },
  userView: {alignItems: "center" },
  cart: {
    marginStart: 20,
    marginEnd: 20,
    backgroundColor: colors.white,
    borderRadius: 5,
    position: "relative",
    // flex:1
  },
  imageView: {
    bottom: 75,
    zIndex: 100,
    marginStart: 55,
    flexDirection: "row",
    alignItems: "center",
    position: "relative",
   
  },
  detailView: { alignItems: "center", marginTop: -50, marginBottom: 20 },
  UserImage: {
    height: 150,
    width: 150
  },
  tickImage: {
    top: 35,
    right: 35,
    zIndex: 100,
    height: 45,
    width: 45
  },

  location: { flexDirection: "row", width: "100%", marginTop: 5 },
  line: {
    width: 1,
    backgroundColor: colors.textColorDark,
    marginEnd: 5,
    marginStart: 5
  },
  SCHEDULED: { margin: 20, fontWeight: '400', fontSize: RFPercentage(2.5) },
  userName: {
    fontSize: 18,
    // fontFamily: font.Regular,
    color: colors.black
  },
  userBadge: {
    marginTop: 8,
    fontSize: 16,
    // fontFamily: font.Regular,
    color: colors.textColorDark
  },
  address: {
    flex: 1,
    fontSize: 14,
    // fontFamily: font.Regular,
    color: colors.textColorDark
  }
});

function mapStateToProps(state) {
  return {
    isLoadingUserEvent: state.Event.isLoadingUserEvent,
    userEvent: state.Event.userEvent,
    errorUserEvent: state.Event.errorUserEvent,
    isLoadingConnectedUserProfile: state.Connection.isLoadingConnectedUserProfile,
    connectedUserProfile: state.Connection.connectedUserProfile,
    errorConnectedUserProfile: state.Connection.errorConnectedUserProfile,
  };
}

export default connect(mapStateToProps,null)(ConnectionUser);