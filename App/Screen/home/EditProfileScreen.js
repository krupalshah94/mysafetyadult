import React, { PureComponent } from "react";
import {
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
  BackHandler
} from "react-native";
import SignUpScreenStyle from "../../Resource/Style/SignUpScreenStyle";
import {
  Header,
  Text,
  Button,
  Image
} from "react-native-elements";
import Colors from "../../Resource/Color/index";
import Icons from "../../Resource/index";
import { TextField } from "react-native-material-textfield";
import ImagePicker from "react-native-image-crop-picker";
import { Dropdown } from "react-native-material-dropdown";
import {emailValidate, showErrorMessage} from '../../Resource/util';
import HeaderWithBack from '../../Component/HeaderWithBack';
import { connect } from "react-redux";
import PhoneNumberInput from "../../Component/PhoneNumber/PhoneNumberInput";
import string from "../../Resource/string";
import colors from "../../Resource/Color/index";
import { getLangValue } from "../../Resource/string/language";
import { RFPercentage } from "react-native-responsive-fontsize";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { updateUserProfile} from '../../Network/Service';
import AsyncStorage from "@react-native-community/async-storage";
import * as Constants from '../../Resource/Constants';
import CustomToast from "../../Component/CustomToast";
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import { UpdateUserProfile, UpdateUserProfileFailed, UpdateUserProfileRequest } from '../../Store/Actions/UserAction';
import NoInternetConnection from "../../Component/NoInternetConnection";
import NetInfo from "@react-native-community/netinfo";

class EditProfileScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isConnected: false,
      profilePicture: "",
      fName: "",
      lName: "",
      countryCode: "",
      phoneNumber: "",
      email: "",
      city: "",
      stateName: "",
      userId: '',
      gender: '',
      dob: '',
      errFName: "",
      errLName: "",
      errPhone: "",
      errEmail: "",
      errCity: "",
      errStateName: "",
      errCountryCode: "",
      countryCodes: []
    };
    this.Token = '';
  }
  componentDidMount() {
    const {user} = this.props;
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
    if (user) {
      if (user.data) {
        this.setState({
          fName: user.data.first_name,
          lName: user.data.last_name,
          email: user.data.email,
          countryCode: user.data.country_code,
          phoneNumber: user.data.phone,
          city: user.data.city,
          stateName: user.data.state,
          profilePicture: user.data.profile_pic,
          userId: user.data.id,
          gender: user.data.gender,
          dob: user.data.dob
        });
      }
    }
    // this.getCountryCodes();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isUpdateUserLoading !== this.props.isUpdateUserLoading) {
      this.setState({ loading: this.props.isUpdateUserLoading});
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    this._subscription && this._subscription();
  }

  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
  }

  onTextChange(text) {
    var cleaned = ("" + text).replace(/\D/g, "");
    var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      var intlCode = match[1] ? "" : "",
        number = [
          intlCode,
          "",
          match[2],
          " - ",
          match[3],
          " - ",
          match[4]
        ].join("");

      this.setState({ phoneNumber: number });

      return;
    }
     this.setState({ phoneNumber: number });
  }
  pickSingle(cropit, circular = false, mediaType) {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: "MediumQuality",
      includeExif: true
    })
      .then(image => {
        this.setState({ profilePicture: image.path });
      })
      .catch(e => {
      });
  }
  async getCountryCodes() {
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
   }
    try {
      let response = await fetch("http://country.io/phone.json");
      let responseJson = await response.json();
      const tempData = [];
      Object.keys(responseJson).reduce((result, currentKey) => {
        const code = {
          value: responseJson[currentKey]
        };
        tempData.push(code);
      }, []);
      const uniqueNames = Array.from(new Set(tempData));

      this.setState({ countryCodes: uniqueNames });
      return responseJson;
    } catch (error) {
      console.error(error);
    }
  }
  doRegistration = async() => {
    const {
      profilePicture,
      fName,
      lName,
      phoneNumber,
      email,
      city,
      stateName,
      countryCode,
      userId,
      gender,
      dob,

    } = this.state;

    if (profilePicture === "") {
      alert("Please select Profile Picture");
      return;
    } else if (fName === "") {
      this.fNameRef.focus();
      this.setState({ errFName: "Please enter First Name" });
      return;
    } else if (lName === "") {
      this.lNameRef.focus();
      this.setState({ errLName: "Please enter Last Name" });
      return;
    } else if (countryCode === undefined || countryCode === '') {
      this.setState({ errCountryCode: 'Please select country code'})
      return;
    } else if (phoneNumber === "") {
      this.phoneRef.focus();
      this.setState({ errPhone: "Please enter Phone Number" });
      return;
    } else if (email === "") {
      this.emailRef.focus();
      this.setState({ errEmail: "Please enter Email" });
      return;
    } else if (!emailValidate(email)) {
      this.emailRef.focus();
      this.setState({ errEmail: "Please enter valid Email" });
      return;
    } else if (city === "") {
      this.cityRef.focus();
      this.setState({ errCity: "Please enter City" });
      return;
    } else if (stateName === "") {
      this.stateRef.focus();
      this.setState({ errStateName: "Please enter State" });
      return;
    } else {
      this.setState({ errStateName: '', errCity: '', errEmail: '',
      errPhone: '', errLName: '', errFName: '', errCountryCode: '' });
    }
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
   }
  
    const userData = {
      user_id: userId,
      first_name: fName,
      last_name: lName,
      phone: phoneNumber,
      country_code: countryCode,
      city: city,
      state: stateName,
      gender: gender,
      dob: dob,
    }
    try {
      this.props.dispatch(UpdateUserProfileRequest());
      const res = await updateUserProfile(this.Token,'image',profilePicture,userData);
      const code = res.code;
      switch (code) {
        case 200:
          showErrorMessage(res.message)
           // this.refs.success.ShowToastFunction(res.message);
            this.props.dispatch(UpdateUserProfile(res));
            break;
        case 400:
          this.props.dispatch(UpdateUserProfileFailed(res));
          this.refs.defaultToastTop.ShowToastFunction(res.message);
        break;
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(UpdateUserProfileFailed(error));
    }
  }

  render() {
    return (
      <SafeAreaView style={SignUpScreenStyle.container}>
        <HeaderWithBack notify={false} name="EDIT" navigation={this.props.navigation}/>
        <ScrollView style={SignUpScreenStyle.container}>
          <KeyboardAwareScrollView style={SignUpScreenStyle.container}>
            <View
              style={{
                flex: 1,
                width: "100%",
                marginTop: "30%"
              }}
            >
              <View style={{ width: "100%", flex:1 }}>
                <View style={SignUpScreenStyle.card}>
                  <TextField
                    ref={ref => {
                      this.fNameRef = ref;
                    }}
                    defaultValue={this.state.fName}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ fName: text });
                    }}
                    onSubmitEditing={() => {
                      this.lNameRef.focus();
                    }}
                    returnKeyType="next"
                    label="First Name"
                    error={this.state.errFName}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={RFPercentage(2.5)}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    containerStyle={{
                      marginStart: 10,
                      marginEnd: 10,
                      paddingTop: "28%"
                    }}
                    onBlur={() => this.setState({ errFName: "" })}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                  <TextField
                    ref={ref => {
                      this.lNameRef = ref;
                    }}
                    defaultValue={this.state.lName}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ lName: text });
                    }}
                    returnKeyType="next"
                    label="Last Name"
                    error={this.state.errLName}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={RFPercentage(2.5)}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    onBlur={() => this.setState({ errLName: "" })}
                    containerStyle={{ marginStart: 10, marginEnd: 10 }}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginStart: 10,
                      marginEnd: 10,
                      marginTop: 30,
                      flex: 1,
                    }}
                  >

                <PhoneNumberInput
                  ref={input => (this.phoneNumberInput = input)}
                  label={getLangValue(string.phoneNumber, 'en')}
                  labelColor= {colors.textColorLight}
                  value={this.state.phoneNumber}
                  errCountryCode={this.state.errCountryCode}
                  code={this.state.countryCode}
                  onComplete={(
                    country_code,
                    phoneNumber,
                    errPhoneNumber,
                    phoneInput,
                  ) => {
                    this.phoneInput = phoneInput;
                    this.setState({
                      countryCode: country_code,
                      phone: phoneNumber,
                      errPhoneNumber: errPhoneNumber,
                    });
                  }}
                />
                </View>
                 
                  <TextField
                    ref={ref => {
                      this.emailRef = ref;
                    }}
                    defaultValue={this.state.email}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ email: text });
                    }}
                    onSubmitEditing={() => {
                      this.cityRef.focus();
                    }}
                    editable={false}
                    returnKeyType="next"
                    label="Email"
                    keyboardType="email-address"
                    error={this.state.errEmail}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={RFPercentage(2.5)}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    autoCapitalize="none"
                    onBlur={() => this.setState({ errEmail: "" })}
                    containerStyle={{ marginStart: 10, marginEnd: 10 }}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                  <TextField
                    ref={ref => {
                      this.cityRef = ref;
                    }}
                    defaultValue={this.state.city}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ city: text });
                    }}
                    onSubmitEditing={() => {
                      this.stateRef.focus();
                    }}
                    returnKeyType="next"
                    label="City"
                    error={this.state.errCity}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={RFPercentage(2.5)}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    autoCapitalize="none"
                    onBlur={() => this.setState({ errCity: "" })}
                    containerStyle={{ marginStart: 10, marginEnd: 10 }}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                  <TextField
                    ref={ref => {
                      this.stateRef = ref;
                    }}
                    defaultValue={this.state.stateName}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ stateName: text });
                    }}
                    returnKeyType="next"
                    label="State"
                    error={this.state.errStateName}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={RFPercentage(2.5)}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    onBlur={() => this.setState({ errStateName: "" })}
                    containerStyle={{ marginStart: 10, marginEnd: 10,marginBottom:20 }}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                </View>

                <Button
                  loading={this.state.loading}
                  title="UPDATE"
                  buttonStyle={{ backgroundColor: "#3444D6", borderRadius: 20 }}
                  containerStyle={{
                    margin: 20
                  }}
                  onPress={() => this.doRegistration()}
                  // titleStyle={{ fontFamily: "AktivGrotesk-Bold",fontSize:16 }}
                  textStyle={{
                  textAlign: 'center',
                  fontSize: RFPercentage(2.5),
                  fontFamily: "AktivGrotesk-Bold",
                }}
                />
              </View>
              <View style={{ position: "absolute", top: -65, alignSelf: 'center', flex:1}}>
                <Image
                  source={
                    this.state.profilePicture == ""
                      ? Icons.user
                      : { uri: this.state.profilePicture }
                  }
                  style={{
                    height: 150,
                    width: 150,
                    alignSelf: "center",
                  }}
                  resizeMethod="auto"
                  resizeMode="contain"
                  PlaceholderContent={<ActivityIndicator />}
                />
                <TouchableOpacity
                  style={{ alignSelf: "center", marginTop: 10 }}
                  onPress={() => this.pickSingle(true)}
                >
                  <Text
                    style={{
                      color: "#6570DE",
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: RFPercentage(2.5),
                      textAlign: 'center'
                    }}
                  >
                    Edit
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </ScrollView>
        <CustomToast ref = "defaultToastTop" backgroundColor={colors.white} textColor={colors.red} position = "bottom"/>
        <CustomToast ref = "success" backgroundColor={colors.white} textColor={colors.greenText} position = "top"/>
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
        {/* {this.state.isConnected && <NoInternetConnection />} */}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.User.user,
    isUserDataLoading: state.User.isUserDataLoading,
    userError: state.User.userError,
    isUpdateUserLoading: state.User.isUpdateUserLoading,
    updatedUser: state.User.updatedUser,
    updateUserError: state.User.updateUserError
  };
}

export default connect(mapStateToProps,null)(EditProfileScreen);
