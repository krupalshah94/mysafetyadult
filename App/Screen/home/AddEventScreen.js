import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator,
  BackHandler,
  TextInput
} from "react-native";
import { Header, Text, Image, Button, Input } from "react-native-elements";
import moment from "moment";
import DateTimePicker from "react-native-modal-datetime-picker";
import { TextField } from "react-native-material-textfield";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
// resource
import Colors from "../../Resource/Color/index";
import Icons from "../../Resource/index";
import string from "../../Resource/string";
import { getLangValue } from '../../Resource/string/language';
// components
import AddInviteModal from '../../Component/Modal/AddInviteModal';
import HeaderWithBack from "../../Component/HeaderWithBack";
import SafetyImageSelectionModal from '../../Component/Modal/SafetyImageSelectionModal';
import WarningImageSelectionModal from "../../Component/Modal/WarningImageSelectionModal";
import { RFPercentage } from "react-native-responsive-fontsize";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import colors from "../../Resource/Color/index";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-community/async-storage';
import * as Constants from '../../Resource/Constants';
import CustomToast from "../../Component/CustomToast";
import { connect } from "react-redux";
import { addNewEventRequest, addNewEventSuccess, addNewEventFailed, eventCompleted } from '../../Store/Actions/EventAction';
import { addNewEvent, addImagesToEvents } from '../../Network/Service';
import BarIndicatorLoader from "../../Component/BarIndicatorLoader";
import { showErrorMessage } from "../../Resource/util";

class AddEventScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      loading: false,
      refreshing: false,
      isDateTimePickerVisible: false,
      eventId: undefined,
      eventTitle: '',
      errEventTitle: "",
      eventLocation: '',
      errEventLocation: '',
      // eventStartDate: moment(new Date()).format("ddd D MMM, YYYY"),
      eventStartDate: 'Event Start Date',
      errEventStartDate: '',
      eventEndDate: "Event End Date",
      errEventEndDate: '',
      // eventStartTime: moment(new Date()).format("hh:mm A"),
      eventStartTime: 'Event Start Time',
      errEventStartTime: '',
      eventEndTime: "Event End Time",
      errEventEndTime: '',
      showInviteModal: false,
      showSafetyImageModal: false,
      showWarningImageModal: false,
      selectedConnection: undefined,
      isNewConnectionSelected: false,
      eventNotes: '',
      selectedImageId: '',
      errSelectedConnection: '',
      selectedWarningImage: '',
      startDate: '',
      endDate: '',
      startTime: '',
      endTime: '',
      startTimeDateFormate: '',
      endTimeDateFormate: '',

    };
    this.Token = '';
    this.userData = '';
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
    this.getToken();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    this._subscription && this._subscription();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoadingAddNewEvent !== this.props.isLoadingAddNewEvent) {
      this.setState({ loading: this.props.isLoadingAddNewEvent})
    }
    if (prevProps.addNewEvent !== this.props.addNewEvent) {
      if (this.props.addNewEvent.code === 200 && this.props.addNewEvent.status === "true") {
        this.props.navigation.navigate('Home');
        this.props.dispatch(eventCompleted(true));
      }
    }
  }
  
  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected
    });
  };

  async getToken() {
    this.Token = await AsyncStorage.getItem(Constants.TOKEN);
    this.userData = await AsyncStorage.getItem(Constants.USER_DATA);
    this.userData = JSON.parse(this.userData);
  }

  _showDateTimePicker = index => {
    this.setState({ index: index, isDateTimePickerVisible: true });
  };

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    switch (this.state.index) {
      case 1:
        this.setState({
          eventStartDate: moment(date).format("ddd D MMM, YYYY"),
          errEventStartDate: '',
          startDate: moment(date).format('YYYY-MM-DD'),
          startTimeDateFormate: date,
        });
        break;
      case 2:
        this.setState({
          eventStartTime: moment(date).format("hh:mm A"),
          startTimeDateFormate: date,
          errEventStartTime: '',
          startTime: moment(date).format("HH:mm"),
        });
        break;
      case 3:
        if ((Date.parse(date) < Date.parse(this.state.startTimeDateFormate))) {
         this.setState({errEventEndDate: 'End Date should be greater than Start Date....!'})
        } else {
          this.setState({errEventEndDate: ''})
        }
        this.setState({
          eventEndDate: moment(date).format("ddd D MMM, YYYY"),
          endDate: moment(date).format('YYYY-MM-DD'),
          endTimeDateFormate: date,
        });
        break;
      case 4:
        this.setState({
          eventEndTime: moment(date).format("hh:mm A"),
          endTime: moment(date).format("HH:mm"),
          errEventEndTime: ''
        });
        break;
      default:
        break;
    }
    this._hideDateTimePicker();
  };
  renderInvite(item, index) {
    return (
      <View
        style={{
          marginStart: 10,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image
          source={{ uri: item.image }}
          style={{ width: wp(25), height: wp(25), borderRadius: 4 }}
          PlaceholderContent={<ActivityIndicator />}
        />

        <Text
          style={{
            marginTop: 5,
            color: "#666666",
            fontFamily: "AktivGrotesk-Regular",
            fontSize: RFPercentage(2),
            textAlign: "center"
          }}
        >
          {item.name}
        </Text>
      </View>
    );
  }
  renderAddInvite() {
    return (
      <View
        style={{
          borderRadius: 4,
          width: wp(25),
          height: wp(25),
          backgroundColor: "#D6D6D6",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <TouchableOpacity
          onPress={() => this.handleInviteModal(true)}
        >
          <Image source={Icons.inviteUser} style={{ width: wp(25), height: wp(20) }} resizeMode="contain" />
          <View style={{ position: "absolute", bottom: 0, marginBottom: 10, start: 5 }}>
            <Text
              style={{
                color: "#666666",
                fontFamily: "AktivGrotesk-Regular",
                fontSize: RFPercentage(2),
                textAlign: "center"
              }}
            >
              + Add Invite
          </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  // invite modal
  handleInviteModal(visible) {
    this.setState({ showInviteModal: visible });
  }

  handleSafetyImageModal = (visible) => {
    if (this.state.eventTitle === '') {
      this.setState({ errEventTitle: getLangValue(string.enterEventTitle, 'en') })
      return;
    }
    if (this.state.eventStartDate === 'Event Start Date') {
      this.setState({ errEventStartDate: getLangValue(string.enterEventStartDate, 'en')})
      return;
    }
    if (this.state.eventEndDate === 'Event End Date') {
      this.setState({ errEventEndDate: getLangValue(string.enterEventEndDate, 'en')})
      return;
    }
    if (this.state.eventStartTime === 'Event Start Time') {
      this.setState({ errEventStartTime: getLangValue(string.enterEventStartTime, 'en')})
      return;
    }
    if (this.state.eventEndTime === 'Event End Time') {
      this.setState({ errEventEndTime: getLangValue(string.enterEventEndTime, 'en')})
      return;
    }
    if (this.state.eventLocation === '') {
      this.setState({ errEventLocation: getLangValue(string.enterEventLocation, 'en') })
      return;
    }
    if (this.state.selectedConnection === undefined) {
      this.setState({ errSelectedConnection: getLangValue(string.enterInviteUser, 'en') })
      return;
    }
    if (this.state.eventNotes === '') {
      this.setState({ errEventNotes: getLangValue(string.enterEventNotes, 'en') })
      return;
    } 
    if (this.state.errEventEndDate !== '') {
      return;
    }
    if (this.state.errEventEndTime !== '') {
      return;
    }  
    this.setState({ errEventNotes: '', errEventTitle: '', errEventLocation: '', errEventEndDate: '', errEventStartDate: '', errEventStartTime: '', errEventEndTime: '', errSelectedConnection: '' });
    this.setState({ showSafetyImageModal: visible });
  }

  handleWarningImageModal = (visible) => {
    this.setState({ showWarningImageModal: visible });
  }
  handleNext(id) {
    this.setState({ selectedImageId: id, errSelectImageId: ''});
    this.handleSafetyImageModal(false);
    this.handleWarningImageModal(true);
  }

  handleSubmit = async (id) => {
    this.setState({ selectedWarningImage: id, loading: true});
    const {eventTitle, eventNotes, startDate, startTime, endDate, endTime, eventLocation, selectedConnection } = this.state;
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
    }
    const userData = {
      title:eventTitle,
      description:eventNotes,
      start_time:startTime,
      end_time:endTime,
      start_date:startDate,
      end_date:endDate,
      address:eventLocation,
      city:'',
      state:'',
      user_id:this.userData.id,
      invite_user_id:selectedConnection.id
    }
    try {
      const res = await addNewEvent(this.Token, userData);
      const code = res.code;
      switch (code) {
        case 200:
          if (res.code === 200 && res.status === "true") {
            showErrorMessage(res.message)
            this.setState({ eventId: res.data.event_id});
            this.handleImageSelection();
          }
        break;
        case 400:
          showErrorMessage(res.message)
          this.setState({ loading: false});
          this.refs.errorEvent.ShowToastFunction(res.message);
        break;
        case 401:
          this.setState({ loading: false});
        showErrorMessage(res.message)
          this.refs.errorEvent.ShowToastFunction(res.message);
        break;
        default:
          break;
      }
    } catch (error) {
      this.setState({ loading: false});
      // this.props.dispatch(addNewEventFailed(error));
    }
    // this.props.navigation.navigate('Home');
  }

  handleImageSelection = async () => {
    const { selectedImageId, selectedWarningImage, eventId } = this.state;
    if (!this.state.isConnected) {
      this.refs.internetConnection.ShowToastFunction('No Internet Connected');
     return;
    }
    const userData = {
      user_id: this.userData.id,
      safe_image_id: selectedImageId,
      unsafe_image_id: selectedWarningImage,
    }
    try {
      this.props.dispatch(addNewEventRequest());
      const res = await addImagesToEvents(this.Token, eventId, userData);
      const code = res.code;
      switch (code) {
        case 200:
          showErrorMessage(res.message)
          this.props.dispatch(addNewEventSuccess(res));
          this.props.navigation.navigate('Home');
          this.props.dispatch(eventCompleted(true));

          break;
        case 400:
          this.props.dispatch(addNewEventFailed(res));
        this.refs.errorEvent.ShowToastFunction(res.message);
        break;
        case 401:
          this.props.dispatch(addNewEventFailed(res));
        this.refs.errorEvent.ShowToastFunction(res.message);
        break;
        default:
          break;
      }
    } catch (error) {
      this.props.dispatch(addNewEventFailed(error));
    }
  }

  handleInvite = (selected) => {
    this.setState({ selectedConnection: selected, isNewConnectionSelected: true , errSelectedConnection: ''});
  }

  render() {
    const { showInviteModal, showSafetyImageModal, showWarningImageModal } = this.state;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
        <HeaderWithBack notify={false} name={getLangValue(string.addEvent, 'en')} navigation={this.props.navigation} />
        <KeyboardAwareScrollView enableOnAndroid={true} style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
          <>
            <View style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
              <View
                style={{
                  backgroundColor: Colors.white,
                  borderRadius: 4,
                  elevation: 2,
                  margin: "5%"
                }}
              >
                <TextField
                  ref={ref => {
                    this.eventTitleRef = ref;
                  }}
                  defaultValue={this.state.eventTitle}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ eventTitle: text });
                  }}
                  onSubmitEditing={() => {
                      this.eventLocationRef.focus();
                    }}
             
                  returnKeyType="next"
                  label={getLangValue(string.eventTitle, 'en')}
                  error={this.state.errEventTitle}
                  placeholderTextColor="#222222"
                  textColor="#222222"
                  underlineColorAndroid={Colors.transparent}
                  tintColor="#222222"
                  fontSize={RFPercentage(3)}
                  containerStyle={{
                    marginStart: 10,
                    marginEnd: 10
                  }}
                  inputContainerStyle={{
                    borderBottomColor: Colors.transparent,
                    borderBottomWidth: 0
                  }}
                  onBlur={() => this.setState({ errEventTitle: "" })}
                  labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                />
                {/* <Text style={{ color: colors.red, fontSize: RFPercentage(2), fontFamily: "AktivGrotesk-Regular", marginTop: 5 }}>{this.state.errEventTitle}</Text> */}
                <DateTimePicker
                  isVisible={this.state.isDateTimePickerVisible}
                  onConfirm={this._handleDatePicked}
                  onCancel={this._hideDateTimePicker}
                  mode={
                    this.state.index == 1
                      ? "date"
                      : this.state.index == 3
                        ? "date"
                        : "time"
                  }
                />
                <View style={{ flexDirection: "row", marginTop: 10 }}>
                  <TouchableOpacity
                    style={{ marginStart: 10, marginEnd: 10, flex: 1, flexDirection: 'row' }}
                    onPress={() => this._showDateTimePicker(1)}
                  >
                    <Image style={{ width: 18, height: 18 }} source={Icons.calender} />
                    <Text
                      style={{
                        fontFamily: "AktivGrotesk-Regular",
                        fontSize: RFPercentage(2),
                        color: Colors.black,
                        textAlign: 'center',
                        padding: 4,
                        marginStart: 5
                      }}
                    >
                      {this.state.eventStartDate}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ marginStart: 10, marginEnd: 10, flex: 1, flexDirection: 'row' }}
                    onPress={() => this._showDateTimePicker(3)}
                  >
                    <Image style={{ width: 18, height: 18 }} source={Icons.calender} />
                    <Text
                      style={{
                        fontFamily: "AktivGrotesk-Regular",
                        fontSize: RFPercentage(2),
                        color: Colors.black,
                        textAlign: "right",
                        padding: 4,
                        marginStart: 5
                      }}
                    >
                      {this.state.eventEndDate}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <TouchableOpacity
                    style={{ marginStart: 10, marginEnd: 10, flex: 1, flexDirection: 'row' }}
                    onPress={() => this._showDateTimePicker(2)}
                  >
                    <Image style={{ width: 18, height: 18 }} source={Icons.clock} />
                    <Text
                      style={{
                        fontFamily: "AktivGrotesk-Regular",
                        fontSize: RFPercentage(2),
                        color: Colors.black,
                        padding: 4,
                        marginStart: 5,
                      }}
                    >
                      {this.state.eventStartTime}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ marginStart: 10, marginEnd: 10, flex: 1, flexDirection: 'row' }}
                    onPress={() => this._showDateTimePicker(4)}
                  >
                    <Image style={{ width: 18, height: 18 }} source={Icons.clock} />
                    <Text
                      style={{
                        fontFamily: "AktivGrotesk-Regular",
                        fontSize: RFPercentage(2),
                        color: Colors.black,
                        textAlign: "right",
                        padding: 4,
                        marginStart: 5
                      }}
                    >
                      {this.state.eventEndTime}
                    </Text>
                  </TouchableOpacity>
                </View>
                <Text style={{ color: colors.red, fontSize: RFPercentage(2), fontFamily: "AktivGrotesk-Regular", marginTop: 5, marginStart: 10 }}>
                {this.state.errEventStartDate !== ''? this.state.errEventStartDate : this.state.errEventEndDate !== '' ? this.state.errEventEndDate : this.state.errEventStartTime !== ''? this.state.errEventStartTime : this.state.errEventEndTime !== ''? 
                this.state.errEventEndTime : ''} 
                </Text>
             
                <TextField
                  ref={ref => {
                    this.eventLocationRef = ref;
                  }}
                  value={this.state.eventLocation}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ eventLocation: text });
                  }}
                  returnKeyType="next"
                  label={getLangValue(string.eventLocation, 'en')}
                  error={this.state.errEventLocation}
                  placeholderTextColor="#222222"
                  textColor="#222222"
                  underlineColorAndroid={Colors.transparent}
                  tintColor="#222222"
                  fontSize={RFPercentage(2)}
                  containerStyle={{
                    marginStart: 10,
                    marginEnd: 10
                  }}
                  inputContainerStyle={{
                    borderBottomColor: "#DFDFDF",
                    borderBottomWidth: 1
                  }}
                  onBlur={() => this.setState({ errEventLocation: "" })}
                  labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                />
                {this.state.isNewConnectionSelected === true ?
                  <View
                    style={{
                      marginStart: 10, marginBottom: 0, marginTop: 10
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => this.handleInviteModal(true)}
                    >
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={{ uri: this.state.selectedConnection.image }}
                        style={{ width: wp(25), height: wp(25), borderRadius: 4 }}
                        PlaceholderContent={<ActivityIndicator />}
                      />
                    <View style={{marginStart: 10, marginEnd: 10}}>
                      <Text
                        style={{
                          marginTop: 5,
                          color: colors.inputTextColor,
                          fontFamily: "AktivGrotesk-Regular",
                          fontSize: RFPercentage(2.5),
                        }}
                      >
                        {this.state.selectedConnection.name}
                      </Text>
                      <Text
                        style={{
                          marginTop: 5,
                          color: colors.textColorLight,
                          fontFamily: "AktivGrotesk-Regular",
                          fontSize: RFPercentage(2),
                        }}
                      >
                        Badge Number: {this.state.selectedConnection.badge}
                      </Text>
                      <Text
                        style={{
                          marginTop: 5,
                          color: colors.textColorLight,
                          fontFamily: "AktivGrotesk-Regular",
                          fontSize: RFPercentage(2),
                        }}
                      >
                        Since {this.state.selectedConnection.time}
                      </Text>
                      </View>
                    </View>
                    </TouchableOpacity>
                  </View> :
                  <View style={{ marginStart: 10, marginBottom: 0, marginTop: 10 }}>
                    <View
                      style={{
                        borderRadius: 4,
                        width: wp(25),
                        height: wp(25),
                        backgroundColor: "#D6D6D6",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.handleInviteModal(true)}
                      >
                        <Image source={Icons.inviteUser} style={{ width: wp(25), height: wp(20) }} resizeMode="contain" />
                        <View style={{ position: "absolute", bottom: 0, start: 5 }}>
                          <Text
                            style={{
                              color: "#666666",
                              fontFamily: "AktivGrotesk-Regular",
                              fontSize: RFPercentage(2),
                              textAlign: "center"
                            }}
                          >
                            + Add Invite
                      </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                }
                <Text style={{ color: colors.red, fontSize: RFPercentage(2), fontFamily: "AktivGrotesk-Regular", marginTop: 5 }}>{this.state.errSelectedConnection}</Text>
                <TextField
                  ref={ref => {
                    this.eventNotesRef = ref;
                  }}
                  defaultValue={this.state.eventNotes}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ eventNotes: text });
                  }}
                  onSubmitEditing={() => {
                      this.eventLocationRef.focus();
                    }}
             
                  returnKeyType="next"
                  label={getLangValue(string.notes, 'en')}
                  error={this.state.errEventNotes}
                  placeholderTextColor="#222222"
                  textColor="#222222"
                  underlineColorAndroid={Colors.transparent}
                  tintColor="#222222"
                  fontSize={RFPercentage(3)}
                  containerStyle={{
                    marginStart: 10,
                    marginEnd: 10,
                  }}
                  inputContainerStyle={{
                    borderBottomColor: Colors.transparent,
                    borderBottomWidth: 0,
                  }}
                  numberOfLines={10}
                  multiline={true}
                  onBlur={() => this.setState({ errEventNotes: "" })}
                  labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                />
            {/* <View style={{
                  marginStart: 10,
                  marginEnd: 10,
                  marginBottom: 20,
                  marginTop: 20
                }}>
                  <Text style={{ marginBottom: 10, color: colors.textColorLight, fontSize: RFPercentage(2) }}>{getLangValue(string.notes, 'en')}</Text>
                  <View
                    style={{
                      borderRadius: 2,
                      borderColor: colors.itemSeparator,
                      borderWidth: 1,
                      height: 80,
                    }}
                  >
                    <Input
                      ref={ref => {
                        this.eventNotesRef = ref;
                      }}
                      returnKeyType="done"
                      value={this.state.eventNotes}
                      onChangeText={text => this.setState({ eventNotes: text })}
                      inputStyle={{
                        fontFamily: "AktivGrotesk-Regular",
                        fontSize: RFPercentage(2),
                        padding: 0
                      }}
                      multiline={true}
                      numberOfLines={10}
                      underlineColorAndroid={Colors.transparent}
                      inputContainerStyle={{ padding: 0, borderBottomWidth: 0 }}
                    />
                  </View>
                  <Text style={{ color: colors.red, fontSize: RFPercentage(2), fontFamily: "AktivGrotesk-Regular", marginTop: 5 }}>{this.state.errEventNotes}</Text>
                </View> */}
              </View>

              <View
                style={{
                  margin: 15
                }}
              >
                <Button
                  loading={this.state.loading}
                  title={getLangValue(string.save, 'en')}
                  buttonStyle={{
                    backgroundColor: "#3444D6",
                    borderRadius: 20,
                    alignItems: "center"
                  }}
                  textStyle={{
                    textAlign: 'center',
                    fontSize: RFPercentage(2.5),
                    fontFamily: "AktivGrotesk-Bold",
                  }}
                  onPress={() => this.handleSafetyImageModal(true)}
                />
              </View>
            </View>
          </>
        </KeyboardAwareScrollView>
        <AddInviteModal handleInvite={this.handleInvite} showModal={showInviteModal} closeModal={() => this.handleInviteModal(false)} />
        <SafetyImageSelectionModal showModal={showSafetyImageModal} handleNext={(id) => this.handleNext(id)} closeModal={() => this.handleSafetyImageModal(false)} />
        <WarningImageSelectionModal selectedImageId={this.state.selectedImageId} showModal={showWarningImageModal} handleSubmit={(id) => this.handleSubmit(id)} closeModal={() => this.handleWarningImageModal(false)} />
        <CustomToast ref="internetConnection" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        <CustomToast ref="errorEvent" backgroundColor={colors.white} textColor={colors.red} position="bottom" />
        {this.state.loading && <BarIndicatorLoader color='black' size={16} count={5}/>}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoadingAddNewEvent: state.Event.isLoadingAddNewEvent,
    addNewEvent: state.Event.addNewEvent,
    errorAddNewEvent: state.Event.errorAddNewEvent
  };
}
export default connect(mapStateToProps, null)(AddEventScreen);