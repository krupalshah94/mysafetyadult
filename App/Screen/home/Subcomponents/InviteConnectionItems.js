import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, SafeAreaView, ActivityIndicator } from 'react-native'
import {styles} from './connectionStyle';
import Icons from '../../../Resource/index';
import { getLangValue } from '../../../Resource/string/language';
import string from '../../../Resource/string';
import colors from '../../../Resource/Color';

export default class InviteConnectionItems extends Component {
    state = {
        loadingImg: true
    }
    handleLoading = () => {
        this.setState({
          loadingImg: false,
        });
      }
    render() {
        const { navigation, item, handleConnection, index } = this.props;
        return (
            <View>
            <TouchableOpacity onPress={() => handleConnection(item.id,index, !item.selected)}> 
                <View  style={styles.connectionView}>
                    <View style={styles.row}>
                        <View style={[styles.margin_10, {flex:1}]}>
                        <View>
                            <Image source={{uri:item.image}} style={styles.userImage}
                                onLoadEnd={this.handleLoading}
                            />
                            <ActivityIndicator
                            style={{
                            position: 'absolute',
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0,
                            backgroundColor: colors.transparent,
                            }}
                            animating={this.state.loadingImg}
                        />
                        </View>
                            <Image source={Icons.tick} style={styles.tickedImage} />
                        </View>
                        <View style={[styles.margin_10, {flex:3, marginStart: 40}]}>
                            <View style={styles.m_bottom_3}>
                                <Text style={styles.nameText}>{item.name}</Text>
                            </View>
                            <View style={styles.m_bottom_3}>
                                <Text style={styles.badgeText}>{getLangValue(string.badgeNumber, 'en')}: {item.badge}</Text>
                            </View>
                            {/* <View style={styles.m_bottom_3}>
                                <Text style={styles.timeText}>Since {item.time}</Text>
                            </View> */}
                        </View>
                        <View style={[styles.margin_10, {flex:1}]}>
                        <View style={{alignSelf: 'center'}}>
                            <Image style={{width: 20, height: 20, opacity: 0.5}} source={item.selected? Icons.circleChecked: Icons.circle} />
                        </View>
                        </View>
                    </View>
                </View>
                </TouchableOpacity>
            </View>
        )
    }
}
