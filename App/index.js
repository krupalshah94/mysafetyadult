import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Home from "./Screen/Tab/Home";
import Connections from "./Screen/Tab/Connections";
import Events from "./Screen/Tab/Events";
import Badge from "./Screen/Tab/Badge";
import Setting from "./Screen/Tab/Setting";
import TabBarCustom from "./Component/TabBarCustom";

//login module
import Login from "./Screen/login/LoginScreen";
import ForgetPasswordScreen from './Screen/login/ForgetPassword';
import ChangePasswordScreen from './Screen/login/ChangePassword';
import VerifyOtp from './Screen/login/VerifyOtp';
import SetNewPassword from './Screen/login/SetNewPassword';
//common module
import Notification from "./Screen/Common/NotificationList";
import NotificationDetail from "./Screen/Common/NotificationDetail";
import ConnectionUser from "./Screen/Common/ConnectionUser";
import EventDetail from "./Screen/Common/EventDetailScreen";
import CompleteEvent from "./Screen/Common/CompleteEventScreen";
import ShowLink from "./Screen/Common/ShowLinkScreen";
import CompleteEventList from './Screen/Common/CompleteEventList';
//connection
import Connected from './Screen/Connection/Connected';
import Incoming from './Screen/Connection/Incoming';
import ConnectionRequestedScreen from './Screen/Connection/ConnectionRequestedScreen';

//event
import AddEventScreen from './Screen/home/AddEventScreen';
import IncomingEvents from './Screen/Common/IncomingEvents';

//settings
import EditProfileScreen from './Screen/home/EditProfileScreen';
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
export const BottomTab = () => (
  <Tab.Navigator tabBar={props => <TabBarCustom {...props} />}>
  <Tab.Screen name="Home" component={Home} />
  <Tab.Screen name="Connections" component={Connections} />
  <Tab.Screen name="Events" component={Events} />
  <Tab.Screen name="Badge" component={Badge} />
  <Tab.Screen name="Setting" component={Setting} />
</Tab.Navigator>
)
export default function App() {
  return (
    <NavigationContainer>
      {/* Rest of your app code */}
      <Stack.Navigator initialRouteName="Login" headerMode="none" screenOptions={{gestureEnabled: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Tabs" component={BottomTab} />
      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="NotificationDetail" component={NotificationDetail} />
      <Stack.Screen name="EventDetail" component={EventDetail} />
      <Stack.Screen name="CompleteEvent" component={CompleteEvent} />
      <Stack.Screen name="ShowLink" component={ShowLink} />
      <Stack.Screen name="ForgetPassword" component={ForgetPasswordScreen} />
      <Stack.Screen name="ChangePassword" component={ChangePasswordScreen} />
      <Stack.Screen name="Connected" component={Connected} />
      <Stack.Screen name="Incoming" component={Incoming} />
      <Stack.Screen name="ConnectionUser" component={ConnectionUser} /> 
      <Stack.Screen name="ConnectionRequested" component={ConnectionRequestedScreen} />
      <Stack.Screen name="AddEventScreen" component={AddEventScreen} />
      <Stack.Screen name="CompleteEventList" component={CompleteEventList} />
      <Stack.Screen name="EditProfile" component={EditProfileScreen} />
      <Stack.Screen name="VerifyOtp" component={VerifyOtp} />
      <Stack.Screen name="SetNewPassword" component={SetNewPassword} />   
      <Stack.Screen name="IncomingEvents" component={IncomingEvents} />   
    </Stack.Navigator>
    </NavigationContainer>
  );
}